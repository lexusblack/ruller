<?php

use Illuminate\Support\Facades\Route;
//use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);



Route::group([
    'middleware' => 'auth',
],
    function ($router) {
        Route::get('/dashboard', \App\Http\Livewire\Dashboard::class)->name('dashboard');
        Route::get('/', \App\Http\Livewire\Dashboard::class)->name('home');
        Route::get('/profile', \App\Http\Livewire\Profile::class)->name('profile');
        Route::get('/page', \App\Http\Livewire\Page::class)->name('page');
        Route::get('/town', \App\Http\Livewire\Towns::class)->name('town');
        Route::get('/firm', \App\Http\Livewire\Firm::class)->name('firm');
        Route::get('/job', \App\Http\Livewire\Job::class)->name('job');
        Route::get('/hostel', \App\Http\Livewire\Hostel::class)->name('hostel');
        Route::get('/profession', \App\Http\Livewire\Profession::class)->name('profession');
        Route::get('/worker', \App\Http\Livewire\Worker::class)->name('worker');
        Route::get('/staff', \App\Http\Livewire\Staff::class)->name('staff');
        Route::get('/service', \App\Http\Livewire\Service::class)->name('service');
        Route::get('/settlement', \App\Http\Livewire\Settlement::class)->name('settlement');
        Route::get('/status', \App\Http\Livewire\Status::class)->name('status');
        Route::get('/penalty', \App\Http\Livewire\Penalty::class)->name('penalty');
        Route::get('/fine', \App\Http\Livewire\Fine::class)->name('fine');
        Route::get('/booker', \App\Http\Livewire\Booker::class)->name('booker');
        Route::get('/money-workers', \App\Http\Livewire\MoneyWorkers::class)->name('money-workers');
        Route::get('/salary-workers', \App\Http\Livewire\SalaryWorkers::class)->name('salary-workers');
        Route::get('/salary', \App\Http\Livewire\Salary::class)->name('salary');
        Route::get('/employment', \App\Http\Livewire\Employment::class)->name('employment');
        Route::get('/bonus', \App\Http\Livewire\Bonus::class)->name('bonus');
        Route::get('/sponsor', \App\Http\Livewire\Sponsor::class)->name('sponsor');
        Route::get('/message-page', \App\Http\Livewire\MessagePage::class)->name('message-page');
        Route::get('/document', \App\Http\Livewire\Document::class)->name('document');
        Route::get('/settings', \App\Http\Livewire\Settings::class)->name('settings');
    });

Route::group([
    'middleware' => 'guest',
],
    function () {
        Route::get('/register', \App\Http\Livewire\Register::class)->name('register');
        Route::get('/login', \App\Http\Livewire\Login::class)->name('login');

//        Route::get('/login', \App\Http\Livewire\Login::class);
    });

