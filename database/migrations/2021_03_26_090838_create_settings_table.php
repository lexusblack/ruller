<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('nip');
            $table->string('town_id');
            $table->string('street');
            $table->integer('phone');
            $table->integer('postal_code');
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->integer('conto')->nullable();
            $table->integer('conto23')->nullable();
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
