<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDestinationToUserTableAnother extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('street')->nullable()->after('phone');
            $table->integer('role_id')->default(5)->after('street')->nullable();
            $table->string('profession')->nullable()->after('role_id');
            $table->string('town')->nullable()->after('role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('street');
            $table->dropColumn('role_id');
            $table->dropColumn('profession');
            $table->dropColumn('town');
        });
    }
}
