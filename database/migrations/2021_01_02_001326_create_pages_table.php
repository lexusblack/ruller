<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('page_title');
            $table->string('menu_title')->nullable();
            $table->integer('parent')->default(0);
            $table->string('alias');
            $table->string('uri');
            $table->string('template');
            $table->tinyInteger('published')->default(1);
            $table->dateTime('published_at', $precision = 0)->useCurrent();
            $table->dateTime('unpublished_at', $precision = 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
