<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDestinationToFirmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('firms', function (Blueprint $table) {
            $table->string('contact_name')->after('adress');
            $table->string('phone')->after('contact_name');
            $table->date('start_work')->nullable(true);
            $table->date('stop_work')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('firms', function (Blueprint $table) {
            $table->dropColumn('contact_name');
            $table->dropColumn('phone');
            $table->dropColumn('start_work');
            $table->dropColumn('stop_work');
        });
    }
}
