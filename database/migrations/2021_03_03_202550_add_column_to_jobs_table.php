<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->string('duties')->nullable(true);
            $table->string('time_work')->nullable(true);
            $table->string('living')->nullable(true);
            $table->string('transport')->nullable(true);
            $table->string('demand')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('duties');
            $table->dropColumn('time_work');
            $table->dropColumn('living');
            $table->dropColumn('transport');
            $table->dropColumn('demand');
        });
    }
}
