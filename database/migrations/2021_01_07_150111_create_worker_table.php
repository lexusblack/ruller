<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('second_name');
            $table->string('passport');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('national');
            $table->string('sex');
            $table->string('pesel')->nullable();
            $table->date('birthday');
            $table->date('start_passport');
            $table->date('stop_passport');
            $table->date('date_start_working');
            $table->date('date_stop_working')->nullable();
            $table->date('date_start_visa')->nullable();
            $table->date('date_stop_visa')->nullable();
//            $table->integer('town_id');
//            $table->integer('hostel_id')->nullable();
//            $table->string('street')->nullable();
//            $table->integer('profession_id')->nullable();
//            $table->integer('firm_id')->nullable();
//            $table->integer('job_id')->nullable();
            $table->text('images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
