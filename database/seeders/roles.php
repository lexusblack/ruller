<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'admin','alias' => 'Администратор'],
            ['name' => 'buh','alias' => 'Бухгалтер'],
            ['name' => 'owner','alias' => 'Владелец'],
            ['name' => 'kadr','alias' => 'Кадровик'],
            ['name' => 'rekrut','alias' => 'Рекрутер'],
            ['name' => 'call','alias' => 'Call center']
        ]);
    }
}
