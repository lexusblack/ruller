<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages= [
            [
                "page_title" => "Town",
                "menu_title" => "Города",
                "parent" => 0,
                "alias" => "town",
                "uri" => "/town",
                "template" => "livewire.town",
                "icon" => "fas fa-gopuram",
                "sort" => 13,
                "published" => true,
                "published_at" => "2021-01-23T21:19:44.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Staff",
                "menu_title" => "Сотрудники",
                "parent" => 0,
                "alias" => "staff",
                "uri" => "/staff",
                "template" => "livewire.staff",
                "icon" => "fas fa-user-tie",
                "sort" => 2,
                "published" => true,
                "published_at" => "2021-03-22T13:46:32.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Page",
                "menu_title" => "Страницы",
                "parent" => 0,
                "alias" => "page",
                "uri" => "/page",
                "template" => "livewire.page",
                "icon" => "fas fa-file-alt",
                "sort" => 100,
                "published" => true,
                "published_at" => "2021-01-08T03:17:40.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Dashboard",
                "menu_title" => "Dashboard",
                "parent" => 0,
                "alias" => "dashboard",
                "uri" => "/dashboard",
                "template" => "livewire.dashboard",
                "icon" => "fas fa-tachometer-alt",
                "sort" => 1,
                "published" => true,
                "published_at" => "2021-02-25T20:16:03.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Firm",
                "menu_title" => "Фирмы",
                "parent" => 0,
                "alias" => "firm",
                "uri" => "/firm",
                "template" => "livewire.firm",
                "icon" => "fas fa-building",
                "sort" => 11,
                "published" => true,
                "published_at" => "2021-01-22T11:43:27.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Worker",
                "menu_title" => "Работники",
                "parent" => 0,
                "alias" => "worker",
                "uri" => "/worker",
                "template" => "livewire.worker",
                "icon" => "fas fa-users",
                "sort" => 3,
                "published" => true,
                "published_at" => "2021-03-17T22:47:17.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Booker",
                "menu_title" => "Отчеты по хостелам",
                "parent" => 0,
                "alias" => "booker",
                "uri" => "/booker",
                "template" => "livewire.booker",
                "icon" => "fas fa-coins",
                "sort" => 99,
                "published" => true,
                "published_at" => "2021-03-26T08:10:18.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Service",
                "menu_title" => "Услуги",
                "parent" => 0,
                "alias" => "service",
                "uri" => "/service",
                "template" => "livewire.service",
                "icon" => "fas fa-hands-helping",
                "sort" => 9,
                "published" => true,
                "published_at" => "2021-02-08T14:32:24.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Hostel",
                "menu_title" => "Жилье",
                "parent" => 0,
                "alias" => "hostel",
                "uri" => "/hostel",
                "template" => "livewire.hostel",
                "icon" => "fas fa-house-user",
                "sort" => 98,
                "published" => true,
                "published_at" => "2021-01-14T16:35:12.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Job",
                "menu_title" => "Работа",
                "parent" => 0,
                "alias" => "job",
                "uri" => "/job",
                "template" => "livewire.job",
                "icon" => "fas fa-briefcase",
                "sort" => 4,
                "published" => true,
                "published_at" => "2021-01-09T13:33:33.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Profession",
                "menu_title" => "Профессии",
                "parent" => 0,
                "alias" => "profession",
                "uri" => "/profession",
                "template" => "livewire.profession",
                "icon" => "fas fa-user-md",
                "sort" => 6,
                "published" => true,
                "published_at" => "2021-01-25T14:58:13.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Settlement",
                "menu_title" => "Заселение",
                "parent" => 0,
                "alias" => "settlement",
                "uri" => "/settlement",
                "template" => "livewire.settlement",
                "icon" => "fas fa-hotel",
                "sort" => 97,
                "published" => false,
                "published_at" => "2021-01-18T00:06:38.000000Z",
                "unpublished_at" => "2021-03-22T13:46:28.000000Z"
            ],
            [
                "page_title" => "Status",
                "menu_title" => "Состояние услуги",
                "parent" => 0,
                "alias" => "status",
                "uri" => "/status",
                "template" => "livewire.statuses",
                "icon" => "fas fa-tasks",
                "sort" => 10,
                "published" => true,
                "published_at" => "2021-01-23T21:18:55.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Penalty",
                "menu_title" => "Штрафы",
                "parent" => 0,
                "alias" => "penalty",
                "uri" => "/penalty",
                "template" => "livewire.penalty",
                "icon" => "fas fa-frown",
                "sort" => 11,
                "published" => false,
                "published_at" => "2021-01-27T13:43:10.000000Z",
                "unpublished_at" => "2021-03-17T12:25:00.000000Z"
            ],
            [
                "page_title" => "Fine",
                "menu_title" => "Оштрафовать",
                "parent" => 0,
                "alias" => "fine",
                "uri" => "/fine",
                "template" => "livewire.fine",
                "icon" => "fas fa-file-signature",
                "sort" => 12,
                "published" => true,
                "published_at" => "2021-01-23T21:19:23.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Salary",
                "menu_title" => "Начисление зарплаты",
                "parent" => 0,
                "alias" => "salary",
                "uri" => "/salary",
                "template" => "livewire.salary",
                "icon" => "fas fa-credit-card",
                "sort" => 7,
                "published" => true,
                "published_at" => "2021-01-27T13:43:00.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Employment",
                "menu_title" => "Трудоустройство",
                "parent" => 0,
                "alias" => "employment",
                "uri" => "/employment",
                "template" => "livewire.employment",
                "icon" => "fas fa-network-wired",
                "sort" => 5,
                "published" => true,
                "published_at" => "2021-01-25T18:11:29.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Bonus",
                "menu_title" => "Бонусы",
                "parent" => 0,
                "alias" => "bonus",
                "uri" => "/bonus",
                "template" => "livewire.bonus",
                "icon" => "fas fa-file-invoice-dollar",
                "sort" => 8,
                "published" => true,
                "published_at" => "2021-01-27T13:39:33.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Sponsor",
                "menu_title" => "Поощрение",
                "parent" => 0,
                "alias" => "sponsor",
                "uri" => "/sponsor",
                "template" => "livewire.sponsor",
                "icon" => "fas fa-hand-holding-usd",
                "sort" => 8,
                "published" => true,
                "published_at" => "2021-01-27T13:45:59.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Message",
                "menu_title" => "Сообщения",
                "parent" => 0,
                "alias" => "message-page",
                "uri" => "/message-page",
                "template" => "livewire.messagepage",
                "icon" => "fas fa-sms",
                "sort" => 98,
                "published" => true,
                "published_at" => "2021-03-14T14:09:28.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Document",
                "menu_title" => "Шаблоны документов",
                "parent" => 0,
                "alias" => "document",
                "uri" => "/document",
                "template" => "livewire.documents",
                "icon" => "far fa-file-word",
                "sort" => 96,
                "published" => true,
                "published_at" => "2021-03-18T13:53:01.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Money workers",
                "menu_title" => "Отчеты по выплатам",
                "parent" => 0,
                "alias" => "money-workers",
                "uri" => "/money-workers",
                "template" => "livewire.money-workers",
                "icon" => "fas fa-coins",
                "sort" => 99,
                "published" => true,
                "published_at" => "2021-03-26T23:06:38.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Salary workers",
                "menu_title" => "Увольнения",
                "parent" => 0,
                "alias" => "salary-workers",
                "uri" => "/salary-workers",
                "template" => "livewire.salary-workers",
                "icon" => "fas fa-coins",
                "sort" => 99,
                "published" => true,
                "published_at" => "2021-04-12T23:48:38.000000Z",
                "unpublished_at" => null
            ],
            [
                "page_title" => "Settings",
                "menu_title" => "Настройка",
                "parent" => 0,
                "alias" => "settings",
                "uri" => "/settings",
                "template" => "livewire.settings",
                "icon" => "fas fa-tachometer-alt",
                "sort" => 100,
                "published" => true,
                "published_at" => "2021-03-26T12:29:53.000000Z",
                "unpublished_at" => null
            ],
        ]
        ;

        foreach ($pages as $page) {
            DB::table('pages')->insert($page);
        }

    }
}
