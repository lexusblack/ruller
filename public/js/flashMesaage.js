const Site = {
    sel: {
        to_top_btn: '.to_top',
        to_top: 'html, body'
    },
    classes: {},
    init: function () {
        this.load();
    },
    load: function () {
        $(document).on('click', this.sel.to_top_btn, function (e) {
            e.preventDefault();
            Site.toTop();
        });
    },
    toTop: function () {
        $(Site.sel.to_top).animate({scrollTop: $(Site.sel.to_top).position().top}, 500);
    }
};

const FlashMessage = {
    setup: function () {
        this.instance = null;
    },
    init: function () {
        this.setup();
        this.load();
    },
    load: function () {
        FlashMessage.instance = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });
    },
    success: function (data) {
        if (!FlashMessage.isInitialize()) {
            FlashMessage.init();
        }

        FlashMessage.instance.fire({
            icon: 'success',
            title: FlashMessage.getMessage(data)
        });
    },

    error: function (data) {
        if (!FlashMessage.isInitialize()) {
            FlashMessage.init();
        }

        FlashMessage.instance.fire({
            icon: 'error',
            title: FlashMessage.getMessage(data)
        });
    },

    info: function (data) {
        if (!FlashMessage.isInitialize()) {
            FlashMessage.init();
        }

        FlashMessage.instance.fire({
            icon: 'info',
            title: FlashMessage.getMessage(data)
        });
    },

    warning: function (data) {
        if (!FlashMessage.isInitialize()) {
            FlashMessage.init();
        }

        FlashMessage.instance.fire({
            icon: 'warning',
            title: FlashMessage.getMessage(data)
        });
    },

    question: function (data) {
        if (!FlashMessage.isInitialize()) {
            FlashMessage.init();
        }

        FlashMessage.instance.fire({
            icon: 'question',
            title: FlashMessage.getMessage(data)
        });
    },
    isInitialize: function () {
        return !(FlashMessage.instance === null || typeof FlashMessage.instance === 'undefined');
    },
    getMessage: function (data, default_message) {
        let message = typeof default_message === 'undefined' ? 'Youp!' : default_message;

        if (typeof data.detail === 'object') {
            for (let item in data.detail) {
                if (!data.detail.hasOwnProperty(item)) {continue;}
                if (item === 'message') {
                    message = data.detail[item];
                }
            }
        }

        return message;
    }
};

(function($) {
    $(function () {
        Site.init();
        FlashMessage.init();
    });
})(jQuery);
