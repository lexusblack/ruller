var SettlementJS = {
    sel: {
        start: '.setlemmetn_date_start',
        stop: '.setlemmetn_date_stop',
    },
    data: {
        dateFormat: "yy-mm-dd"
    },
    init: function () {
        this.load();
    },
    load: function () {
        this.initDatePickStart();
        this.initDatePickStop();
    },
    initDatePickStart: function() {
        var start_min_date = $(SettlementJS.sel.start).data('minDate');
        var start_max_date = $(SettlementJS.sel.start).data('maxDate');
        var startConfig = {
            dateFormat: "yy-mm-dd",
            disabled: false,
            onSelect: function (value, inst) {
                window.livewire.emit('set-stop-min-date', value);
            }
        };
        if (typeof start_min_date !== 'undefined') {
            startConfig.minDate = start_min_date;
        }
        if (typeof start_max_date !== 'undefined') {
            startConfig.maxDate = start_max_date;
        }
        $(SettlementJS.sel.start).datepicker(startConfig);
    },
    initDatePickStop: function () {
        var stop_min_date = $(SettlementJS.sel.stop).data('minDate');
        var stop_max_date = $(SettlementJS.sel.stop).data('maxDate');
        var config = {
            dateFormat: "yy-mm-dd",
            disabled: false,
            onSelect: function (value, inst) {
                window.livewire.emit('set-stop-date', value);
            }
        };
        if (typeof stop_min_date !== 'undefined' && stop_min_date.length > 0) {
            config.disabled = false;
            config.minDate = stop_min_date;
        }
        if (typeof stop_max_date !== 'undefined') {
            config.maxDate = stop_max_date;
        }
        $(SettlementJS.sel.stop).datepicker(config);
    },
    refreshDatePickStop: function (min, max) {
        if (typeof min !== 'undefined' && min.length > 0) {
            $(SettlementJS.sel.stop).datepicker('option', 'disabled', false);
            $(SettlementJS.sel.stop).datepicker('option', 'minDate', min);
        }

        if (typeof max !== 'undefined' && max.length > 0) {
            $(SettlementJS.sel.stop).datepicker('option', 'maxDate', max);
        }
        $(SettlementJS.sel.stop).datepicker('refresh');
    }
    /*refreshDatePickStop: function () {
        var stop_min_date = $(SettlementJS.sel.stop).data('minDate');
        var stop_max_date = $(SettlementJS.sel.stop).data('maxDate');
        console.log(stop_min_date, stop_max_date);
        if (typeof stop_min_date !== 'undefined') {
            $(SettlementJS.sel.stop).datepicker('option', 'disabled', false);
            $(SettlementJS.sel.stop).datepicker('option', 'minDate', stop_min_date);
        }
        if (typeof stop_max_date !== 'undefined') {
            $(SettlementJS.sel.stop).datepicker('option', 'maxDate', stop_max_date);
        }
        $(SettlementJS.sel.stop).datepicker('refresh');
    }*/

};


(function($) {
    $(function() {
        SettlementJS.init();
    });
})(jQuery);
