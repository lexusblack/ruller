<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PageCreating
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($page)
    {
        if (empty($page->uri)) {
            $page->uri = '/' . strtolower($page->alias);
        }

        if (empty($page->parent)){
            $page->parent = '0';
        }

        if (empty($page->icon)){
            $page->icon = 'fas fa-tachometer-alt';
        }

        if (empty($page->template)) {
            $page->template = 'livewire.'.strtolower($page->alias);
        }



        /*if ($page->published) {
            $page->unpublished_at = null;
            $page->published_at = Carbon::parse('now');
        }
        else {
            $page->unpublished_at = Carbon::parse('now');
        }*/
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
