<?php


namespace App\Traits;

trait DataToTemplate
{
    public $css = [];
    public $js = [];
    public $bodyClass = '';
    public $title = '';
    public $fonts = [];

    /**
     * @param $tpl
     * @param array $data
     * @param string $base_tpl
     * @param string $slot
     * @return mixed
     */
    public function baseView($tpl, $base_tpl = 'layouts.login', $slot = 'body')
    {
        return view($tpl)
            ->extends($base_tpl, [
                'bodyClass' => $this->bodyClass,
                'title' => $this->title,
                'css' => $this->css,
                'js' => $this->js,
                'fonts' => $this->fonts
            ])
            ->slot($slot);
    }
}
