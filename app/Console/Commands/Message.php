<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserMessages;

class Message extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Harvest info for message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $message = new UserMessages('cron');
        $message->createVisaMessages();
        $message->createHostelMessage();
        return 0;
    }
}
