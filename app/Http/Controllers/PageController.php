<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function published($id)
    {
        /** @var Page $page */
        $page = Page::find($id);
        $page->published = true;
        $page->save();
    }

    public function unpublished($id)
    {
        /** @var Page $page */
        $page = Page::find($id);
        $page->published = false;
        $page->save();
    }

    public function index()
    {
        echo 'Test';
    }
}
