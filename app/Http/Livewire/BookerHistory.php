<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Worker;
use App\Models\Job;
use App\Models\Employment;
use Livewire\WithPagination;

class BookerHistory extends Component
{
    use WithPagination;

    public $currentPage = 1;
    public $search;

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function render()
    {
        $worker_table = (new Worker())->getTable();
        $job_table = (new Job())->getTable();
        $emplyments_table = (new Employment())->getTable();

        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

//        return view('livewire.booker-history');


        return view('livewire.booker-history', [
            'histories' => Employment::selectRaw($emplyments_table . '.*,' .
                $worker_table . '.first_name,' .
                $worker_table . '.second_name,' .
                $worker_table . '.passport,' .
                $job_table . '.description')
                ->leftJoin($worker_table, $worker_table . '.id', '=', $emplyments_table . '.worker_id')
                ->leftJoin($job_table, $job_table . '.id', '=', $emplyments_table . '.job_id')
                ->where($worker_table . '.first_name', 'like', $search)
                ->orWhere($worker_table . '.second_name', 'like', $search)
                ->orWhere($worker_table . '.passport', 'like', $search)
//                ->orderBy($emplyments_table . '.stop', 'ASC')
                ->having($emplyments_table . '.status', '=', Employment::STATUS_FINISH)
                ->paginate(10)
        ]);
    }
}
