<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Sponsor extends Base
{
    public function mount()
    {
        $this->bodyClass = 'text-sm control-sidebar-slide-open layout-navbar-fixed';
        $this->title = trans('words.sponsor');
        $this->css = [
            '/plugins/fontawesome-free/css/all.min.css',
            '/css/adminlte.min.css',
            '/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css',
            '/plugins/datatables-responsive/css/responsive.bootstrap4.min.css',
            '/plugins/datatables-buttons/css/buttons.bootstrap4.min.css',
            '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
            '/plugins/jquery-ui/jquery-ui.min.css',
            '/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css',
            '/plugins/toastr/toastr.min.css',
        ];
        $this->js = [
            '/plugins/jquery/jquery.min.js',
            '/plugins/bootstrap/js/bootstrap.bundle.min.js',
            '/plugins/datatables/jquery.dataTables.min.js',
            '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js',
            '/plugins/datatables-responsive/js/dataTables.responsive.min.js',
            '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js',
            '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
            '/plugins/datatables-buttons/js/dataTables.buttons.min.js',
            '/plugins/datatables-buttons/js/buttons.bootstrap4.min.js',
            '/plugins/jquery-ui/jquery-ui.min.js',
            '/plugins/sweetalert2/sweetalert2.min.js',
            '/js/adminlte.js',
            '/js/demo.js',
            '/js/flashMesaage.js',
        ];
        $this->fonts = [
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback',
            'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'
        ];
    }

    public function render()
    {
        return $this->baseView('livewire.sponsor');
    }
}
