<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Http\Request;


class ProfilForm extends Component
{
    use WithFileUploads;

    //todo сломан

    public $photo;
    public $name = '';
    public $email = '';
    public $street = '';
    public $town = '';
    public $profession = '';
    public $phone = '';
    public $role = '';
    public $role_id;
    public $user = 5;
    public $prev = null;


    public function updatedName()
    {
        $this->validate(['name' => 'required|min:3']);
        $this->emit('changeProfileData', '', $this->name);
    }

    public function updatedEmail()
    {
        $this->validate(['email' => 'required|email']);
    }

    public function updatedPhoto()
    {

        $this->validate([
            'photo' => 'image|max:2048', // 1MB Max
        ]);

        $info = explode('.', $this->photo->hashName());
        $ext = $info[count($info) - 1];

        $this->prev = 'data:image/' . $ext . ';base64,' . base64_encode($this->photo->get());




    }

    public function mount()
    {
        $current_user = $this->user = auth()->user();
        $this->name = $current_user->name;
        $this->email = $current_user->email;
        $this->phone = $current_user->phone;
        $this->street = $current_user->street;
        $this->town = $current_user->town;
        $this->role = $current_user->role;
        $this->profession = $current_user->profession;
        $this->photo = $current_user->photo;
    }

    public function save()
    {

        $profileData = $this->validate([
            'name' => 'min:3',
            'email' => 'required|email',
            'phone' => 'min:9',
            'street' => '',
            'role' => '',
//            'role_id' => '',
            'town' => '',
            'profession' => '',
            'photo' => 'image|max:2048',
        ]);

        if ($this->photo) {
            $avatar = $this->photo->store('/', 'avatars');
            $profileData = array_merge($profileData, ['photo' => $avatar]);
        }

        auth()->user()->update($profileData);

        $this->emit('changeProfileData', $avatar, $this->name);

        $this->cleanFields();

    }

    /*public function update(Request $request)
    {

    }*/

    protected function cleanFields()
    {
        //$this->prev = null;
        /*$this->name = '';
        $this->email = '';
        $this->phone = '';
        $this->street = '';
        $this->town = '';
        $this->profession = '';*/
    }

//    protected $messages = [
//        'name.min' => 'Минимальная длинна 3 символа',
//        'name.required' => 'Имя необходимо',
//        'photo.image' => 'Нет картинки',
//        'photo.max' => 'Превышен максимальный размер',
//        'email.required' => 'email необходим',
//        'email.email' => 'Это не email'
//    ];



    public function render()
    {
        return view('livewire.profil-form');
    }
}
