<?php

namespace App\Http\Livewire;

use App\Models\Employment;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Worker;
use App\Models\Penalty;
use App\Models\Fine;
use phpDocumentor\Reflection\Types\This;
use Livewire\WithPagination;
use function Symfony\Component\Translation\t;

class FineForm extends Component
{
    public $show_form = false;
    public $show_info = false;

    public $worker_id, $penalty_id, $date_penalty, $comment, $fine_id, $price = 0, $finePrice = 0, $search, $currentPage;

    public $workers = [];
    public $penaltys = [];
    public $worker = [];
    public $penalty = [];
    public $up = false;

    use WithPagination;

    public function updated()
    {
        $this->validate([
            'worker_id' => 'required',
            'penalty_id' => 'required',
            'date_penalty' => 'required'
        ]);
    }

    public function mount()
    {
        $this->workers = $this->getWorkers();
        $this->penaltys = $this->getPenaltys();
        $this->fines = $this->getFines();
    }

    public function getWorkers()
    {
//        return Worker::orderBy('second_name', 'ASC')->get();
        return Worker::whereHas('employments', function ($query) {
            $query->where('status', '=', Employment::STATUS_ACTIVE);
        })->get();
    }

    public function getPenaltys()
    {
        return Penalty::orderBy('name', 'ASC')->get();
    }

    public function getFines()
    {
        return Fine::orderBy('date_penalty', 'DESC')->get();
    }

    public function editFine($id)
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->show_form = true;
        $this->show_info = false;
        $fine = Fine::find($id);
        $this->fine_id = $fine->id;
        $this->worker_id = $fine->worker_id;
        $this->penalty_id = $fine->penalty_id;
        $this->date_penalty = $fine->date_penalty;
        $this->comment = $fine->comment;
        $this->price = $fine->price;
        $this->finePrice = $fine->penalty->price;
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function save()
    {
        $fineData = $this->validate([
            'worker_id' => 'required',
            'penalty_id' => 'required',
            'date_penalty' => 'required',
            'comment' => '',
            'price' => '',
        ]);

        if (empty($this->fine_id)) {
            $fine = new Fine();
            $penalty = Penalty::find($this->penalty_id);
            $fineData = array_merge($fineData, ['price' => $penalty->price]);
            $message = trans('words.add');
        } else {
            $fine = Fine::find($this->fine_id);
            if ($this->price == 0 || $this->up != false) {
                $fineData = array_merge($fineData, ['price' => $fine->penalty->price]);
            }
            $message = trans('words.updated');
        }

        $fine->fill($fineData);
        $fine->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $fine->name . ' ' . $fine->worker->first_name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->reset(['worker_id', 'penalty_id', 'date_penalty', 'comment', 'fine_id', 'finePrice', 'price', 'up']);
        $this->fines = $this->getFines();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteFine($id)
    {
        Fine::destroy($id);
        $this->fines = $this->getFines();
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $fine = Fine::find($id);
        $this->fine_id = $fine->id;
        $this->worker_id = $fine->worker_id;
        $this->penalty_id = $fine->penalty_id;
        $this->date_penalty = $fine->date_penalty;
        $this->comment = $fine->comment;
        $this->price = $fine->price;
        $this->worker = $fine->worker->toArray();
        $this->penalty = $fine->penalty->toArray();
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->reset(['worker_id', 'penalty_id', 'date_penalty', 'comment', 'fine_id', 'finePrice', 'price', 'up']);
        $this->show_info = false;
        $this->show_form = false;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function showForm()
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->reset(['worker_id', 'penalty_id', 'date_penalty', 'comment', 'fine_id', 'finePrice', 'price', 'up']);
        $this->show_form = true;
        $this->show_info = false;
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->reset(['worker_id', 'penalty_id', 'date_penalty', 'comment', 'fine_id', 'finePrice', 'price', 'up']);
        $this->show_form = false;
        $this->show_info = false;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        $worker_table = (new Worker())->getTable();
        $penalty_table = (new Penalty())->getTable();
        $fines_table = (new Fine())->getTable();

        return view('livewire.fine-form',[
            'fines' => Fine::selectRaw($fines_table . '.*,' .
            $worker_table . '.first_name,' .
            $worker_table . '.second_name,' .
            $penalty_table . '.name')
            ->leftJoin($worker_table, $worker_table . '.id', '=', $fines_table . '.worker_id')
            ->leftJoin($penalty_table, $penalty_table . '.id', '=', $fines_table . '.penalty_id')
            ->where($worker_table . '.first_name', 'like', $search)
            ->orWhere($worker_table . '.second_name', 'like', $search)
            ->orWhere($penalty_table . '.name', 'like', $search)
            ->paginate(10)
        ]);
    }
}
