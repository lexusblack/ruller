<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class Register extends Base
{

    public $name = '';
    public $email = '';
    public $password = '';
    public $passwordConfirmation = '';

    public  function updatedName()
    {
        $this->validate(['name' => 'min:6']);
    }

    public function updatedEmail()
    {
        $this->validate(['email' => 'email|unique:users']);
//        $this->validate(['password' => 'min:6|same:passwordConfirmation']);
    }

    public function mount()
    {
        $this->bodyClass = 'login-page';
        $this->title = 'Login';
        $this->css = [
            '/plugins/fontawesome-free/css/all.min.css',
            '/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
            '/css/adminlte.min.css'
        ];
        $this->js = [
            '/plugins/jquery/jquery.min.js',
            '/plugins/bootstrap/js/bootstrap.bundle.min.js',
            '/js/adminlte.min.js'
        ];
        $this->fonts = [
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback'
        ];
    }

    public function  updatedPassword()
    {
        $this->validate(['password' => 'min:6|same:passwordConfirmation']);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register()
    {
        $data = $this->validate([
            'name' => 'required|min:6',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|same:passwordConfirmation',
        ]);
        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect()->route('login');
    }

    protected $messages = [
        'name.min' => 'Minimal leight 6',
        ];

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->baseView('livewire.register');
    }
}
