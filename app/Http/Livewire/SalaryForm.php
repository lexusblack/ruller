<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Job;
use App\Models\Worker;
use App\Models\Salary;
use App\Models\Employment;
use Livewire\WithPagination;

class SalaryForm extends Component
{
    public
        $salary_id,
        $worker_id,
        $employment_id,
        $date,
        $hours,
        $status_paid,
        $salary,
        $show_form = false,
        $show_info = false,
        $search,
        $currentPage = 1;

    public $active_job;

//    public $salaries = [];
    public $workers = [];
    public $status_list = [];
    public $salarys = [];
    public $worker = [];


    use WithPagination;



    public function updated()
    {
        $this->validate([
            'worker_id' => 'required',
            'hours' => 'required',
            'date' => 'required',
            'status_paid' => 'required'
        ]);
    }

    public function mount()
    {
        $this->status_list = Salary::SALARY_STATUS;
        $this->workers = $this->getWorkers();
        $this->salaries = $this->getSalaries();
    }

    public function getWorkers()
    {
        return Worker::whereHas('employments', function ($query) {
            $query->where('status', '=', Employment::STATUS_ACTIVE);
        })->get();
    }

    public function getJobs()
    {
        return Job::orderBy('description', 'ASC')->get();
    }

    public function getEmployment()
    {
        return Employment::orderBy('start', 'DESC')->get();
    }

    public function getSalaries()
    {
        return Salary::orderBy('date', 'ASC')->get();
    }

    public function resetFields()
    {
        $this->reset(['worker_id', 'date', 'hours', 'status_paid']);
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function editSalary($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $salary = Salary::find($id);
        $this->worker_id = $salary->worker_id;
        $this->salary_id = $salary->id;
        $this->date = $salary->date;
        $this->hours = $salary->hours;
        $this->status_paid = $salary->status_paid;
        $this->getActiveJob();
        $this->dispatchBrowserEvent('toTopPage');
        $this->dispatchBrowserEvent('init-date-field');
    }

    public function getActiveJob()
    {
        //$worker = Worker::find($this->worker_id);
        $this->active_job = Employment::where([
            ['worker_id', '=', $this->worker_id],
            ['status', '=', Employment::STATUS_ACTIVE]
        ])->first();
    }

    public function getSumSalary()
    {
        $dop = 0;
        if ($this->hours > Salary::STANDART_HOURS) {
            $dop = ($this->hours - Salary::STANDART_HOURS) * $this->active_job->job->money_plus;
        }
        return $this->hours * $this->active_job->job->money + $dop;
    }

    public function save()
    {
        $salaryData = $this->validate([
            'worker_id' => 'required',
            'date' => 'required',
            'hours' => 'required',
            'status_paid' => 'required'
        ]);

        $salaryData = array_merge($salaryData, [
            'employment_id' => $this->active_job->id,
            'salary' => $this->getSumSalary()
        ]);

        if (empty($this->salary_id)) {
            $salary = new Salary();
            $message = trans('words.add');
        } else {
            $salary = Salary::find($this->salary_id);
            $message = trans('words.updated');
        }

        $salary->fill($salaryData);
        $salary->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $salary->worker->first_name . ' salary ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->resetFields();
        $this->salaries = $this->getSalaries();
        $this->show_form = false;
    }

    public function deleteSalary($id)
    {
        Salary::destroy($id);
        $this->salaries = $this->getSalaries();
    }

    public function showForm()
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->resetFields();
        $this->show_form = true;
        $this->show_info = false;
    }

    public function hideForm()
    {

        $this->show_form = false;
        $this->show_info = false;
        $this->resetFields();
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $salary = Salary::find($id);
        $this->worker_id = $salary->worker_id;
        $this->salary_id = $salary->id;
        $this->date = $salary->date;
        $this->hours = $salary->hours;
        $this->status_paid = $salary->status_paid;
        $this->worker = $salary->worker->toArray();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->resetFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        $workers_table = (new Worker())->getTable();
        $job_table = (new Job())->getTable();
        $salaries_table = (new Salary())->getTable();

        return view('livewire.salary-form',
        [
            'salaries' => Salary::selectRaw($salaries_table . '.*,' .
                $workers_table . '.first_name,' .
                $workers_table . '.second_name')
            ->leftJoin($workers_table, $workers_table . '.id', '=', $salaries_table . '.worker_id')
            ->where($workers_table . '.first_name' , 'like', $search)
            ->orWhere($workers_table . '.second_name', 'like', $search)
            ->paginate(10)
        ]);
    }
}
