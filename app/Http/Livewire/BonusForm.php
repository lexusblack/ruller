<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Bonus;
use Livewire\WithPagination;

class BonusForm extends Component
{
    public
        $name,
        $price,
        $comment,
        $bonus_id,
        $currentPage,
        $search;

//    public $bonuses = [];

    public $show_form = false;
    public $show_info = false;

    use WithPagination;

    public function updated()
    {
        $this->price = str_replace(',', '.', $this->price);
        $this->validate([
            'name' => 'required|min:3|unique:bonuses',
            'price' => 'required|numeric',
        ]);
    }

    public function mount()
    {
        $this->bonuses = $this->getBonuses();
    }

    public function getBonuses()
    {
        return Bonus::orderBy('name', 'ASC')->get();
    }

    public function save()
    {

        if (empty($this->bonus_id)) {
            $this->validate([
                'name' => 'required|min:3|unique:bonuses',
                'price' => 'required|numeric',
            ]);
            $message = 'add';
            $bonus = new Bonus();
        } else {
            $bonus = Bonus::find($this->bonus_id);
            $message = 'updated';
        }

        $bonusData = [
            'name' => $this->name,
            'price' => $this->price,
            'comment' => $this->comment,
        ];

        $bonus->fill($bonusData);
        $bonus->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $bonus->name . ' ' . $message . ' success!'
        ]);
        $this->bonuses = $this->getBonuses();
        $this->cleanFields();
        $this->show_form = false;

    }

    public function cleanFields()
    {
        $this->reset(['name', 'price', 'comment']);
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function editBonus($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $bonus = Bonus::find($id);
        $this->name = $bonus->name;
        $this->bonus_id = $bonus->id;
        $this->price = $bonus->price;
        $this->comment = $bonus->comment;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteBonus($id)
    {
        Bonus::destroy($id);
        $this->bonuses = $this->getBonuses();
    }

    public function showForm()
    {
        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $bonus = Bonus::find($id);
        $this->name = $bonus->name;
        $this->bonus_id = $bonus->id;
        $this->price = $bonus->price;
        $this->comment = $bonus->comment;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        return view('livewire.bonus-form',
        [
            'bonuses' => Bonus::where('name', 'like', $search)
            ->paginate(10)
        ]);
    }
}
