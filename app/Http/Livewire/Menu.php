<?php

namespace App\Http\Livewire;

use App\Models\Page as Model;
use Livewire\Component;

class Menu extends Component
{

    public $items;

    protected $listeners = ['changeProfileData', 'changeMenuItems'];
    public $avatar;
    public $name;

    public function changeMenuItems()
    {
        $this->items = $this->getMenu();
    }

    public function mount()
    {
        $user = auth()->user();
        $this->items = $this->getMenu();
        $this->avatar = $user->photo;
        $this->name = $user->name;
    }

    public function render()
    {
        return view('livewire.menu');
    }


    public function changeProfileData($avatar, $name)
    {
        $this->avatar = !empty($avatar) ? $avatar : $this->avatar;
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getMenu()
    {
        $pages = Model::orderBY('sort', 'ASC')->get();
        //$pages = $pages1->sortBy('sort');
        $result = [];
        foreach ($pages as $page) {
            $result[] = array_merge($page->toArray(), ['children' => []]);
        }

        return $result;

    }
}
