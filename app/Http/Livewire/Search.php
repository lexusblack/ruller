<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Worker;

class Search extends Component
{
    public $search;

    use WithPagination;

    public function render()
    {
        return view('livewire.search',
            [
                'workers' => Worker::where(function ($sub_query)
                {
                    $sub_query->where('first_name', 'like', '%'.$this->search.'%')
                        ->orWhere('second_name', 'like', '%'.$this->search.'%');
                })->paginate(10)
            ]
        );
//        return view('livewire.search');
    }
}
