<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SettingsForm extends Component
{
    public function render()
    {
        return view('livewire.settings-form');
    }
}
