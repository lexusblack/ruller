<?php

namespace App\Http\Livewire;

use App\Models\Salary;
use App\Models\Settlement;
use App\Models\Bonus;
use App\Models\Job;
use App\Models\Sponsor;
use App\Models\Fine;
use App\Models\Service;
use App\Models\Status;
use App\Models\Penalty;
use App\Models\Worker;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Livewire\Component;
use App\Models\Employment;
use Livewire\WithPagination;


class BookerWorker extends Component
{
    use WithPagination;

    public $search;
    public $selcted_year_salary = 0;
    public $selcted_month_salary = 0;

    public $workers_salary = [];
    public $workers_sponsor = [];
    public $workers_service = [];
    public $workers_penalty = [];
    public $workers_settlement = [];

    public $first_name;
    public $second_name;

    public $months_option_salary = [];
    public $years_option_salary = [];


    protected function formatOption($data, $value = 'value', $text = 'text')
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = [
                'value' => $item[$value],
                'text' => $item[$text]
            ];
        }
        return $result;
    }

    function makeYearsSalary()
    {
        $years = Salary::selectRaw('YEAR(date) as year')->groupBy('year')->orderBy('year', 'DESC')->get();
        $this->years_option_salary = $this->formatOption($years, 'year', 'year');
    }

    public function makeMonthSalary()
    {
        Carbon::setLocale('ru');

        $month = Salary::selectRaw('MONTH(date) as month')
            ->whereYear('date', $this->selcted_year_salary)
            ->groupBy('month')
            ->orderBy('month', 'ASC')
            ->get();

        $month->transform(function ($item) {
            return [
                'value' => $item->month,
                'text' => mb_convert_case(Carbon::parse('01-' . $item->month . '-' . $this->selcted_year_salary)->monthName, MB_CASE_TITLE, "UTF-8")
            ];
        });
        $this->months_option_salary = $this->formatOption($month);

    }

    public function mount()
    {
        $this->makeYearsSalary();
        $this->selcted_year_salary = Carbon::now()->format('Y');

        $this->makeMonthSalary();
        $this->selectedMonthSalary(Carbon::now()->format('n'));
//        $this->selcted_month_salary = Carbon::now()->format('n');

    }

    public function selectedYearSalary($value)
    {
        $this->selcted_year_salary = $value;
        $this->reset(['selcted_month_salary']);
        $this->makeMonthSalary();
        $this->reset(['first_name','second_name','workers_salary','workers_sponsor','workers_service','workers_penalty','workers_settlement']);

    }

    public function selectedMonthSalary($value)
    {
        $this->selcted_month_salary = $value;
        $this->reset(['first_name','second_name','workers_salary','workers_sponsor','workers_service','workers_penalty','workers_settlement']);
    }

    public function setWorkerActiveView($worker_id)
    {
        $settlement_table = (new Settlement())->getTable();
        $sponsor_table = (new Sponsor())->getTable();
        $fine_table = (new Fine())->getTable();
        $status_table = (new Status())->getTable();
        $salary_table = (new Salary())->getTable();
        $job_table = (new Job())->getTable();
        $employment_table = (new Employment())->getTable();

        $this->workers_salary = Salary::selectRaw($salary_table . '.*,' .
            $employment_table . '.job_id,' .
            $job_table . '.*')
            ->leftJoin($employment_table, $employment_table . '.id', '=', $salary_table . '.employment_id')
            ->leftJoin($job_table, $job_table . '.id', '=', $employment_table . '.job_id')
            ->whereMonth($salary_table . '.date', $this->selcted_month_salary)
            ->whereYear($salary_table . '.date', $this->selcted_year_salary)
            ->where($salary_table . '.worker_id', '=', $worker_id)
            ->get();

        $worker = Worker::find($worker_id);
        $this->first_name = $worker->first_name;
        $this->second_name = $worker->second_name;

        $this->workers_sponsor = Sponsor::selectRaw($sponsor_table . '.*')
            ->whereMonth($sponsor_table . '.date', $this->selcted_month_salary)
            ->whereYear($sponsor_table . '.date', $this->selcted_year_salary)
            ->where($sponsor_table . '.worker_id', $worker_id)
            ->get();

        $this->workers_penalty = Fine::selectRaw($fine_table . '.*')
            ->whereMonth($fine_table . '.date_penalty', $this->selcted_month_salary)
            ->whereYear($fine_table . '.date_penalty', $this->selcted_year_salary)
            ->where($fine_table . '.worker_id', $worker_id)
            ->get();

        $this->workers_service = Status::selectRaw($status_table . '.*')
            ->whereMonth($status_table . '.date_start', $this->selcted_month_salary)
            ->whereYear($status_table . '.date_start', $this->selcted_year_salary)
            ->where($status_table . '.worker_id', $worker_id)
            ->get();

        $this->workers_settlement = Settlement::selectRaw($settlement_table . '.*')
            ->whereMonth($settlement_table . '.start_hostel', $this->selcted_month_salary)
            ->whereYear($settlement_table . '.start_hostel', $this->selcted_year_salary)
            ->where($settlement_table . '.worker_id', $worker_id)
            ->get();
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function render()
    {

        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        $settlementQuery = Settlement::selectRaw('SUM(price)')
            ->whereColumn('settlements.worker_id', 'salaries.worker_id')
            ->whereMonth('settlements.start_hostel', $this->selcted_month_salary)
            ->whereYear('settlements.start_hostel', $this->selcted_year_salary)
            ->groupBy('settlements.worker_id')
            ->getQuery();

        $bonusQuery = Sponsor::selectRaw('SUM(price)')
            ->whereColumn('sponsors.worker_id', 'salaries.worker_id')
            ->whereMonth('sponsors.date', $this->selcted_month_salary)
            ->whereYear('sponsors.date', $this->selcted_year_salary)
            ->groupBy('sponsors.worker_id')
            ->getQuery();

        $fineQuery = Fine::selectRaw('SUM(price)')
            ->whereColumn('fines.worker_id', 'salaries.worker_id')
            ->whereMonth('fines.date_penalty', $this->selcted_month_salary)
            ->whereYear('fines.date_penalty', $this->selcted_year_salary)
            ->groupBy('fines.worker_id')
            ->getQuery();

        $serviceQuery = Status::selectRaw('SUM(price)')
            ->whereColumn('statuses.worker_id', 'salaries.worker_id')
            ->whereMonth('statuses.date_start', $this->selcted_month_salary)
            ->whereYear('statuses.date_start', $this->selcted_year_salary)
            ->groupBy('statuses.worker_id')
            ->getQuery();

        return view('livewire.booker-worker',[
            'salary_workers' => Salary::selectRaw('
            SUM(salaries.salary) as sum,
            salaries.worker_id as worker_id,
            workers.first_name as first_name,
            workers.second_name as second_name,
            workers.pesel,
            salaries.status_paid as status',)
                ->selectSub($settlementQuery, 'sum_settlement')
                ->selectSub($bonusQuery, 'sum_bonus')
                ->selectSub($fineQuery, 'sum_penalty')
                ->selectSub($serviceQuery, 'sum_service')
                ->leftJoin('workers', 'workers.id', '=', 'salaries.worker_id')
                ->whereMonth('salaries.date', $this->selcted_month_salary)
                ->whereYear('salaries.date', $this->selcted_year_salary)
                ->when(!empty($search), function ($query) use ($search) {
                    $query->where(function($q) use ($search) {
                        $q->where('workers.first_name', 'like', $search)
                            ->orWhere('workers.second_name', 'like', $search)
                            ->orWhere('workers.pesel', 'like', $search);
                    });
                })
                ->orderBy('salaries.status_paid', 'DESC')
                ->groupBy('workers.id')
                ->paginate(10)
        ]);
    }
}
