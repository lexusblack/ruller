<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Firm;
use App\Models\Town;
use App\Models\Hostel;
use Livewire\WithPagination;

class HostelForm extends Component
{
    public $name,
        $town_id,
        $address,
        $postal_code,
        $firm_id,
        $start_work,
        $stop_work,
        $owner_name,
        $owner_phone,
        $admin_name,
        $admin_phone,
        $search,
        $hostel_id;

    public $currentPage = 1;

    public $show_form = false;
    public $show_info = false;

    public $firms = [];
    public $towns = [];
//    public $hostels = [];

    public $firm = [];
    public $town = [];

    use WithPagination;
    public function updated()
    {
        $this->validate([
            'name' => 'unique:hostels|required|min:3',
            'admin_name' => 'required|min:3',
            'admin_phone' => 'required'
        ]);
    }

    public function mount()
    {
        $this->hostels = $this->getHostels();
        $this->firms = $this->getFirms();
        $this->towns = $this->getTowns();
    }

    public function getHostels()
    {
        return Hostel::orderBy('name', 'ASC')->get();
    }

    public function getFirms()
    {
        return Firm::orderBy('name', 'ASC')->get();
    }

    public function getTowns()
    {
        return Town::orderBy('name', 'ASC')->get();
    }

    public function cleanFields()
    {
        $this->name = null;
        $this->town_id = null;
        $this->address = null;
        $this->postal_code = null;
        $this->firm_id = null;
        $this->start_work = null;
        $this->stop_work = null;
        $this->owner_name = null;
        $this->owner_phone = null;
        $this->admin_name = null;
        $this->admin_phone = null;
        $this->hostel_id = null;
    }

    public function save()
    {
        if (empty($this->hostel_id)){
            $this->validate([
                'name' => 'unique:hostels|required|min:3',
                'admin_name' => 'required|min:3',
                'admin_phone' => 'required',
            ]);
            $hostel = new Hostel();
            $message = trans('words.add');
        }
        else {
            $hostel = Hostel::find($this->hostel_id);
            $message = trans('words.updated');
        }

        $hostelData = [
            'name' => $this->name,
            'town_id' => $this->town_id,
            'address' => $this->address,
            'postal_code' => $this->postal_code,
            'firm_id' => $this->firm_id,
            'start_work' => $this->start_work,
            'stop_work' => $this->stop_work,
            'owner_name' => $this->owner_name,
            'owner_phone' => $this->owner_phone,
            'admin_name' => $this->admin_name,
            'admin_phone' => $this->admin_phone,
        ];

        $hostel->fill($hostelData);
        $hostel->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => 'Hostel: ' . $hostel->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->hostels = $this->getHostels();
        $this->cleanFields();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function editHostel($id)
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->show_form = true;
        $this->show_info = false;
        $hostel = Hostel::find($id);
        $this->hostel_id = $hostel->id;
        $this->name = $hostel->name;
        $this->town_id = $hostel->town_id;
        $this->address = $hostel->address;
        $this->postal_code = $hostel->postal_code;
        $this->firm_id = $hostel->firm_id;
        $this->start_work = $hostel->start_work;
        $this->stop_work = $hostel->stop_work;
        $this->owner_name = $hostel->owner_name ;
        $this->owner_phone = $hostel->owner_phone ;
        $this->admin_name = $hostel->admin_name ;
        $this->admin_phone = $hostel->admin_phone ;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function deleteHostel($id)
    {
        Hostel::destroy($id);
        $this->hostels = $this->getHostels();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showForm()
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->show_form = true;
        $this->show_info = false;
        $this->cleanFields();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->show_form = false;
        $this->cleanFields();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showInfo($id)
    {
        $this->show_info = true;
        $this->show_form = false;
        $hostel = Hostel::find($id);
        $this->name = $hostel->name;
        $this->town_id = $hostel->town_id;
        $this->address = $hostel->address;
        $this->postal_code = $hostel->postal_code;
        $this->firm_id = $hostel->firm_id;
        $this->start_work = $hostel->start_work;
        $this->stop_work = $hostel->stop_work;
        $this->owner_name = $hostel->owner_name ;
        $this->owner_phone = $hostel->owner_phone ;
        $this->admin_name = $hostel->admin_name ;
        $this->admin_phone = $hostel->admin_phone ;
        $this->town = $hostel->town->toArray();
        $this->firm = $hostel->firm->toArray();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        return view('livewire.hostel-form', [
            'hostels' => Hostel::where('name', 'like', $search)
            ->paginate(10)
        ]);
    }
}
