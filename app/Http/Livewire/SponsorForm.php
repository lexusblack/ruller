<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Worker;
use App\Models\Bonus;
use App\Models\Sponsor;
use App\Models\Employment;
use Livewire\WithPagination;

class SponsorForm extends Component
{
    public
        $sponsor_id,
        $worker_id,
        $bonus_id,
        $date,
        $comment,
        $status_list,
        $price,
        $bonusPrice,
        $currentPage,
        $search;

//    public $sponsors = [];

    public $workers = [];

    public $bonuses = [];
    public $worker = [];
    public $bonus = [];

    public $show_form = false;
    public $show_info = false;
    public $up = false;

    use WithPagination;

    public function getSponsors()
    {
        return Sponsor::orderBy('date', 'ASC')->get();
    }

    public function getBonuses()
    {
        return Bonus::orderBy('name', 'ASC')->get();
    }

    public function updated()
    {
        $this->validate([
            'worker_id' => 'required',
            'bonus_id' => 'required',
            'date' => 'required',
        ]);
    }

    public function getWorkers()
    {
        return Worker::whereHas('employments', function ($query) {
            $query->where('status', '=', Employment::STATUS_ACTIVE);
        })->get();
    }

    public function getActiveJob()
    {
        $this->active_job = Employment::where([
            ['worker_id', '=', $this->worker_id],
            ['status', '=', Employment::STATUS_ACTIVE]
        ])->first();
    }

    public function mount()
    {
        $this->workers = $this->getWorkers();
        $this->bonuses = $this->getBonuses();
        $this->sponsors = $this->getSponsors();
    }

    public function cleanFields()
    {
        $this->reset(['worker_id', 'bonus_id', 'date', 'comment', 'price', 'bonusPrice', 'up']);
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function save()
    {
        $this->validate([
            'bonus_id' => 'required',
            'worker_id' => 'required',
            'date' => 'required',
        ]);

        $sponsorData = [
            'worker_id' => $this->worker_id,
            'bonus_id' => $this->bonus_id,
            'date' => $this->date,
            'comment' => $this->comment,
            'price' => $this->price,
        ];

        if (empty($this->sponsor_id)) {
            $sponsor = new Sponsor();
            $message = trans('words.add');

            $bonus = Bonus::find($this->bonus_id);
            $sponsorData = array_merge($sponsorData, ['price' => $bonus->price]);
        } else {
            $sponsor = Sponsor::find($this->sponsor_id);
            if ($this->price == 0 || $this->up != false) {
                $sponsorData = array_merge($sponsorData, ['price' => $sponsor->bonus->price]);
            }
            $message = trans('words.updated');
        }

        $sponsor->fill($sponsorData);
        $sponsor->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $sponsor->bonus->name . ' for worker ' . $sponsor->worker->first_name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->sponsors = $this->getSponsors();
        $this->cleanFields();
        $this->show_form = false;
    }

    public function editSponsor($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('init-date-field');
        $sponsor = Sponsor::find($id);
        $this->sponsor_id = $sponsor->id;
        $this->worker_id = $sponsor->worker_id;
        $this->bonus_id = $sponsor->bonus_id;
        $this->date = $sponsor->date;
        $this->comment = $sponsor->comment;
        $this->bonusPrice = $sponsor->bonus->price;
        $this->price = $sponsor->price;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteSponsor($id)
    {
        Sponsor::destroy($id);
        $this->sponsors = $this->getSponsors();
        $this->cleanFields();
    }

    public function showForm()
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $sponsor = Sponsor::find($id);
        $this->sponsor_id = $sponsor->id;
        $this->worker_id = $sponsor->worker_id;
        $this->bonus_id = $sponsor->bonus_id;
        $this->date = $sponsor->date;
        $this->comment = $sponsor->comment;
        $this->worker = $sponsor->worker->toArray();
        $this->bonus = $sponsor->bonus->toArray();
        $this->bonusPrice = $sponsor->bonus->price;
        $this->price = $sponsor->price;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        $worker_table = (new Worker())->getTable();
        $bonus_table = (new Bonus())->getTable();
        $sponsor_table = (new Sponsor())->getTable();

        return view('livewire.sponsor-form',
            [
                'sponsors' => Sponsor::selectRaw($sponsor_table . '.*,' .
                    $worker_table . '.first_name,' .
                    $worker_table . '.second_name,' .
                    $bonus_table . '.name')
                    ->leftJoin($worker_table, $worker_table . '.id', '=', $sponsor_table . '.worker_id')
                ->leftJoin($bonus_table, $bonus_table . '.id', '=', $sponsor_table . '.bonus_id')
                ->where($worker_table . '.first_name', 'like', $search)
                ->orWhere($worker_table . '.second_name', 'like', $search)
                ->orWhere($bonus_table . '.name', 'like', $search)
                ->paginate(10)
            ]);
    }
}
