<?php

namespace App\Http\Livewire;

use App\Models\DocTemplate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Pagination\Paginator;
use App\Models\Town;
use App\Models\Hostel;
use App\Models\Profession;
use App\Models\Job;
use App\Models\Firm;
use App\Models\Worker;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use \PhpOffice\PhpWord\Settings;
use App\Services\UserMessages;
use App\Models\Message;

class Workerlist extends Component
{
    use WithPagination;

    public
        $first_name,
        $second_name,
        $passport,
        $phone,
        $email,
        $national,
        $pesel,
        $birthday,
        $start_passport,
        $stop_passport,
        $date_start_working,
        $date_stop_working,
        $date_start_visa,
        $date_stop_visa,
        $street,
        $sex,
        $show_form = false,
        $show_info = false,
        $conto,
        $work_id,
        $bank,
        $search,
        $currentPage = 1;
    public $documents;
    public $document;


    public $images = [];
    public $professions = [];
//    public $workers = [];
    public $sex_list = [];

    use WithFileUploads;



    public function updated()
    {
        $this->validate([
            'images.*' => 'image|max:2048',
            'first_name' => 'required|min:2',
            'second_name' => 'required|min:2',
            'passport' => 'required',
            'national' => 'required',
            'birthday' => 'required|date',
            'start_passport' => 'required|date',
            'stop_passport' => 'required|date',
            'date_start_working' => 'required|date',
            'date_start_visa' => 'required|date',
            'date_stop_visa' => 'required|date',
            'sex' => 'required',
            'phone' => 'numeric'

        ]);
    }

    public function mount()
    {

        $this->sex_list = Worker::SEX_LIST;

        $user = Worker::find(request()->get('user'));
        if (!empty($user)) {
            $this->search = $user->passport;
        }

        $this->workers = $this->getWorkers();


    }

    public function getWorkers()
    {
        return Worker::orderBy('first_name', 'ASC')->get();
    }

    public function cleanFields()
    {
        $this->first_name = null;
        $this->second_name = null;
        $this->passport = null;
        $this->phone = null;
        $this->email = null;
        $this->national = null;
        $this->pesel = null;
        $this->birthday = null;
        $this->start_passport = null;
        $this->stop_passport = null;
        $this->date_start_working = null;
        $this->date_stop_working = null;
        $this->date_start_visa = null;
        $this->date_stop_visa = null;
        $this->conto = null;
        $this->sex = null;
        $this->bank = null;
        $this->messages = null;
        $this->documents = null;
        $this->resetErrorBag();
        $this->resetValidation();

    }

    public function editWorker($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('init-date-field');
        $worker = Worker::find($id);
        $this->work_id = $worker->id;
        $this->first_name = $worker->first_name;
        $this->second_name = $worker->second_name;
        $this->passport = $worker->passport;
        $this->phone = $worker->phone;
        $this->email = $worker->email;
        $this->national = $worker->national;
        $this->pesel = $worker->pesel;
        $this->birthday = $worker->birthday;
        $this->start_passport = $worker->start_passport;
        $this->stop_passport = $worker->stop_passport;
        $this->date_start_working = $worker->date_start_working;
        $this->date_stop_working = $worker->date_stop_working;
        $this->date_start_visa = $worker->date_start_visa;
        $this->date_stop_visa = $worker->date_stop_visa;
        $this->sex = $worker->sex;
        $this->conto = $worker->conto;
        $this->bank = $worker->bank;
        $this->images = $worker->images;
        $this->dispatchBrowserEvent('toTopPage');
    }


    public function save()
    {

        if (empty($this->work_id)) {
            $worker = new Worker();
            $message = trans('words.add');
        } else {
            $worker = Worker::find($this->work_id);
            $message = trans('words.updated');
        }

        Log::warning(is_array($this->images));

        $images = [];
        foreach ($this->images as $image) {
            if (!is_object($image)) {continue;}
            $images[] = $image->store('image');
        }

        $validator_rules = [
            'first_name' => 'required|min:3',
            'second_name' => 'required|min:2',
            'passport' => 'required',
            'phone' => 'numeric',
            'email' => '',
            'national' => 'required',
            'pesel' => '',
            'birthday' => 'required|date',
            'start_passport' => 'required|date',
            'stop_passport' => 'required|date',
            'date_start_working' => 'required|date',
            'date_stop_working' => '',
            'date_start_visa' => 'required|date',
            'date_stop_visa' => 'required|date',
            'conto' => '',
            'bank' => '',
            'sex' => 'required',
        ];

        if (!empty($images)) {
            $validator_rules['images.*'] = 'image';
        }

        $workerData = $this->validate($validator_rules);

        if (!empty($images)) {
            $workerData = array_merge($workerData, ['images' => $images]);
        }

        $worker->fill($workerData);
        $worker->save();



        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $worker->first_name . ' ' . $worker->second_name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);

        $this->workers = $this->getWorkers();
        $this->cleanFields();
        $this->show_form = false;

        $message = Message::create([
            'message' => trans('words.message_add_new_worker') . ' ' . $worker->first_name . ' ' . $worker->second_name,
            'user_id' => $worker->id,
            'type' => 'danger',
            'branch' => Worker::MESSAGE_BRANCH
        ]);

        $service_message = new UserMessages();

        $users_send_to = $service_message->getBookers();


        foreach ($users_send_to as $users) {
            $message->usersMessages()->create([
                'user_id' => $users->id,
                'ttl' => Carbon::now()->addHours(8)
            ]);
        }

    }


        public function showForm()
    {
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('init-date-field');
        $this->cleanFields();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->show_form = false;
        $this->show_info = false;
        $this->cleanFields();
    }

    public function showInfo($id)
    {
        $this->show_info = true;
        $this->show_form = false;
        $this->cleanFields();
        $worker = Worker::find($id);
        $this->work_id = $worker->id;
        $this->first_name = $worker->first_name;
        $this->second_name = $worker->second_name;
        $this->passport = $worker->passport;
        $this->phone = $worker->phone;
        $this->email = $worker->email;
        $this->national = $worker->national;
        $this->pesel = $worker->pesel;
        $this->birthday = $worker->birthday;
        $this->start_passport = $worker->start_passport;
        $this->stop_passport = $worker->stop_passport;
        $this->date_start_working = $worker->date_start_working;
        $this->date_stop_working = $worker->date_stop_working;
        $this->date_start_visa = $worker->date_start_visa;
        $this->date_stop_visa = $worker->date_stop_visa;
        $this->sex = $worker->sex;
        $this->conto = $worker->conto;
        $this->bank = $worker->bank;
        $this->images = $worker->images;
        $this->documents = $this->getDocuments();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function getDocuments()
    {
        return DocTemplate::where('category', '=', 'workers')->get();
    }

    public function getReadyDocument()
    {
        if(empty($this->document)) {
            return;
        }

        $template = DocTemplate::find($this->document);
        $template_path = Storage::disk('document_templates')->path($template->path);
        $new_name = Carbon::now()->format('d-m-Y') . '_' . $template->name . '_' . $this->first_name . '_' . $this->second_name . '.pdf';

        $processor = new TemplateProcessor($template_path);
        $processor->setValue('numer', $this->first_name);

        $processor->saveAs(Storage::disk('documents')->path($template->path));

        $phpWord = IOFactory::load(Storage::disk('documents')->path($template->path));


        Settings::setDefaultFontName('dejavu sans');
        Settings::setPdfRendererPath('.');
        Settings::setPdfRendererName(Settings::PDF_RENDERER_DOMPDF);


        $xmlWriter = IOFactory::createWriter($phpWord , 'PDF');

        ob_start();
        $xmlWriter->save("php://output");
        $response = ob_get_contents();
        ob_end_clean();
        return response()->streamDownload(function () use ($response, $new_name) {
            echo $response;
        }, $new_name);

    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.workerlist',
            [
                'workers' => Worker::where('first_name', 'like', $search)
                    ->orWhere('second_name', 'like', $search)
                    ->orWhere('passport', 'like', $search)
                    ->paginate(10)
            ]);
    }



}
