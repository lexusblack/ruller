<?php

namespace App\Http\Livewire;

use App\Models\UsersMessages;
use Livewire\Component;
use App\Services\UserMessages;

class CountMessages extends Component
{

    public function mount()
    {
    }


    public function render()
    {
        return view('livewire.count-messages');
    }
}
