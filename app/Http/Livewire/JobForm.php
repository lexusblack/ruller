<?php

namespace App\Http\Livewire;

use App\Models\Firm;
use App\Models\Town;
use App\Models\Job;
use App\Models\Profession;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class JobForm extends Component
{
    public $firms = [];
    public $towns = [];
    public $professions = [];
    public $profession = [];
    public $firm = [];
    public $town = [];
//    public $jobs = [];
    public $show_form = false;
    public $show_info = false;
    public $currentPage = 1;

    public $firm_id,
        $town_id,
        $profession_id,
        $address,
        $place,
        $description,
        $money,
        $job_id,
        $money_plus,
        $duties,
        $time_work,
        $living,
        $transport,
        $demand,
        $search;

    use WithPagination;

    public function updated()
    {
        $this->validate([
            'address' => 'min:5',
            'place' => 'required',
            'description' => 'min:10',
            'town_id' => 'required',
            'firm_id' => 'required',
            'profession_id' => 'required'
        ]);
    }

    public function updatedMoney()
    {
        $this->money = str_replace(',', '.', $this->money);
        $this->validate(['money' => 'numeric']);
    }

    public function mount()
    {
        $this->firms = $this->getFirms();
        $this->towns = $this->getTowns();
        $this->professions = $this->getProfessions();
        $this->jobs = $this->getJobs();

        $job = Job::find(request()->get('work'));

        if (!empty($job)) {
            $this->search = $job->description;
        }
    }

    public function getJobs()
    {
        return Job::all();
    }

    public function getFirms()
    {
        return Firm::orderBy('name', 'ASC')->get();
    }

    public function getTowns()
    {
        return Town::orderBy('name', 'ASC')->get();
    }

    public function getProfessions()
    {
        return Profession::orderBy('name', 'ASC')->get();
    }

    public function editJob($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $job = Job::find($id);
        $this->firm_id = $job->firm_id;
        $this->job_id = $job->id;
        $this->town_id = $job->town_id;
        $this->profession_id = $job->profession_id;
        $this->address = $job->address;
        $this->place = $job->place;
        $this->money = $job->money;
        $this->money_plus = $job->money_plus;
        $this->description = $job->description;
        $this->duties = $job->duties;
        $this->time_work = $job->time_work;
        $this->living = $job->living;
        $this->transport = $job->transport;
        $this->demand = $job->demand;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function cleanFields()
    {
        $this->firm_id = null;
        $this->town_id = null;
        $this->profession_id = null;
        $this->address = null;
        $this->place = null;
        $this->money = null;
        $this->money_plus = null;
        $this->description = null;
        $this->duties = null;
        $this->time_work = null;
        $this->living = null;
        $this->transport = null;
        $this->demand = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function save()
    {
        $jobData = $this->validate([
            'address' => 'min:5',
            'place' => 'required',
            'description' => 'min:10',
            'money' => 'numeric',
            'money_plus' => 'numeric',
            'town_id' => 'required',
            'firm_id' => 'required',
            'profession_id' => 'required',
            'duties' => '',
            'time_work' => '',
            'living' => '',
            'transport' => '',
            'demand' => '',
        ]);
        if (empty($this->job_id)) {
            $job = new Job();
            $message = trans('words.add');
        } else {
            $job = Job::find($this->job_id);
            $message = trans('words.updated');
        }

        $job->fill($jobData);
        $job->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $job->profession->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->jobs = $this->getJobs();
        $this->cleanFields();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showForm()
    {
        $this->show_form = true;
        $this->show_info = false;
        $this->cleanFields();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->show_form = false;
        $this->cleanFields();
    }

    public function showInfo($id)
    {
        $this->show_info = true;
        $this->show_form = false;
//        $this->editJob($id);
        $job = Job::find($id);
        $this->firm_id = $job->firm_id;
        $this->job_id = $job->id;
        $this->town_id = $job->town_id;
        $this->profession_id = $job->profession_id;
        $this->address = $job->address;
        $this->place = $job->place;
        $this->money = $job->money;
        $this->money_plus = $job->money_plus;
        $this->description = $job->description;
        $this->duties = $job->duties;
        $this->time_work = $job->time_work;
        $this->living = $job->living;
        $this->transport = $job->transport;
        $this->demand = $job->demand;
        $this->profession = $job->profession->toArray();
        $this->firm = $job->firm->toArray();
        $this->town = $job->town->toArray();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function render()
    {
        $job_table = (new Job())->getTable();
        $firm_table = (new Firm())->getTable();
        $town_table = (new Town())->getTable();
        $profession_table = (new Profession())->getTable();

        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        return view('livewire.job-form', [
            'jobs' => Job::selectRaw($job_table . '.*,' .
                $firm_table . '.name as firm_name,' .
                $town_table . '.name as town_name')
                ->leftJoin($firm_table, $firm_table . '.id', '=', $job_table . '.firm_id')
                ->leftJoin($town_table, $town_table . '.id', '=', $job_table . '.town_id')
                ->leftJoin($profession_table, $profession_table . '.id', '=', $job_table . '.profession_id')
                ->where($job_table . '.description', 'like', $search)
                ->orWhere($job_table . '.duties', 'like', $search)
                ->orWhere($job_table . '.demand', 'like', $search)
                ->orWhere($firm_table . '.name', 'like', $search)
                ->orWhere($town_table . '.name', 'like', $search)
                ->orWhere($profession_table . '.name', 'like', $search)
                ->paginate(10)
        ]);
    }
}
