<?php

namespace App\Http\Livewire;

use Livewire\Component;


class Bread extends Component
{
    public function render()
    {
        return view('livewire.bread');
    }
}
