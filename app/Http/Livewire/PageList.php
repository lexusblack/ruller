<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Page as Model;
use App\Models\Page;
use Livewire\WithPagination;

class PageList extends Component
{
//    public $pages;
    public $page_title, $menu_title, $parent, $alias, $uri, $template, $icon, $sort, $published = 0, $page_id;
    public $show_info = false;
    public $show_form = false;

    public $currentPage;
    public $search;

    use WithPagination;

    protected $rules = [
        'icon' => 'required|unique:pages|min:3',
    ];

    public function updated()
    {
        if (empty($this->page_id)) {
            $this->validate([
                'alias' => 'required|unique:pages|min:3',
                'page_title' => 'required',
            ]);
        }
    }

    /**
     *
     */
    public function mount()
    {
        $this->pages = $this->getPages();
    }

    /**
     * @return Model[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getPages()
    {
        return Model::orderBy('sort','ASC')->get();
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

//        $page_table = (new Page())->getTable();
        return view('livewire.page-list',
            [
                'pages' => Page::where('page_title', 'like', $search)
                ->orWhere('menu_title', 'like', $search)
                ->orWhere('alias', 'like', $search)
                ->orWhere('uri', 'like', $search)
                ->paginate(10)

            ]);
    }

    /**
     * @param $id
     */
    public function publishePage($id)
    {
        $page = Model::find($id);
        //if (empty($page)) {return false;}
        $page->published = true;
        $page->save();
        $this->pages = $this->getPages();

    }

    /**
     * @param $id
     */
    public function unpublishedPage($id)
    {
        $page = Model::find($id);
        //if (empty($page)) {return false;}
        $page->published = false;
        $page->save();
        $this->pages = $this->getPages();
    }

    /**
     * @param $id
     */
    public function editPage($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $page = Model::find($id);
        $this->page_title = $page->page_title;
        $this->menu_title = $page->menu_title;
        $this->parent = $page->parent;
        $this->alias = $page->alias;
        $this->uri = $page->uri;
        $this->template = $page->template;
        $this->icon = $page->icon;
        $this->sort = $page->sort;
        $this->published = $page->published;
        $this->page_id = $page->id;
        $this->dispatchBrowserEvent('toTopPage');
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Model::destroy($id); // Удаляет без связей
        $this->emit('changeMenuItems');
        $this->pages = $this->getPages();
    }

    public function save()
    {
        // TODO Добавить валидацию
//        $this->validate();

        if (empty($this->page_id)) {
            $page = new Model();
            $message = trans('words.add');
        }
        else {
            $page = Model::find($this->page_id);
            $message = trans('words.updated');
        }

        $page->fill([
            'page_title' => $this->page_title,
            'menu_title' => $this->menu_title,
            'parent' => $this->parent,
            'alias' => $this->alias,
            'uri' => $this->uri,
            'template' => $this->template,
            'icon' => $this->icon,
            'sort' => $this->sort,
            'published' => $this->published
        ]);
//        dd($page);exit();
        $page->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $page->alias . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->pages = $this->getPages();

        //
        $this->emit('changeMenuItems');

        $this->cleanFields();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    /**
     *
     */
    protected function cleanFields()
    {
        $this->page_title = '';
        $this->menu_title = '';
        $this->parent = '';
        $this->alias = '';
        $this->uri = '';
        $this->template = '';
        $this->icon = '';
        $this->sort = '';
        $this->published = '';
        $this->page_id = '';
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $page = Model::find($id);
        $this->page_title = $page->page_title;
        $this->menu_title = $page->menu_title;
        $this->parent = $page->parent;
        $this->alias = $page->alias;
        $this->uri = $page->uri;
        $this->template = $page->template;
        $this->icon = $page->icon;
        $this->sort = $page->sort;
        $this->published = $page->published;
        $this->page_id = $page->id;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showForm()
    {
        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }
}
