<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Worker;
use App\Models\Job;
use App\Models\Employment;

class DashboardForm extends Component
{

    public $count_workers_active;
    public $count_worker_inactive;
    public $worker_place;
    public $all_workers;


    public function mount()
    {
        $this->count_workers_active = $this->getActiveWorker();
        $this->count_worker_inactive = $this->getInactiveWorker();
        $this->worker_place = $this->countWorkerPlace();
        $this->all_workers = $this->countAllWorkers();
    }

    public function getActiveWorker()
    {
        return Employment::select('worker_id')
            ->where('status', Employment::STATUS_ACTIVE)
            ->count();
    }

    public function getInactiveWorker()
    {
        return Employment::select('worker_id)')
            ->where('status', '=', Employment::STATUS_FINISH)
            ->count();
    }

    public function countWorkerPlace()
    {
        $sum = 0;
        $places = Job::selectRaw('place')
            ->where('place', '>', 0)
            ->get();
        foreach ( $places as $place)
        {
            $sum = $sum + $place->place;
        }



        return $sum;
    }

    public function countAllWorkers()
    {
        return Employment::select('worker_id')
            ->count();
    }

    public function render()
    {
        return view('livewire.dashboard-form');
    }
}
