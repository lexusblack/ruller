<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Service;
use Livewire\WithPagination;

class ServiceForm extends Component
{
    public $service_id, $name, $price, $show_form = false, $show_info = false;
//    public $services = [];

    public $currentPage;
    public $search;

    use WithPagination;

    public function updated()
    {
        $this->price = str_replace(',', '.', $this->price);
        $this->validate(
            [
                'name' => 'required|min:3|unique:services',
                'price' => 'required|numeric',
            ]);
    }

    public function mount()
    {
        $this->services = $this->getService();
    }

    public function getService()
    {
        return Service::orderBy('name', 'ASC')->get();
    }

    public function save()
    {
        $this->validate([
            'name' => 'required|min:3',
        ]);
        if (empty($this->service_id)) {
            $this->validate([
                'name' => 'required|min:3|unique:services',
                'price' => 'required|numeric',
            ]);
            $service = new Service();
            $message = trans('words.add');
        } else {
            $service = Service::find($this->service_id);
            $message = trans('words.updated');
        }

        $serviceData = [
            'name' => $this->name,
            'price' => $this->price,
            'id' => $this->id,
        ];

        $service->fill($serviceData);
        $service->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $service->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->services = $this->getService();
        $this->cleanField();
        $this->show_form = false;

    }

    public function cleanField()
    {
        $this->name = null;
        $this->price = null;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function editService($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $service = Service::find($id);
        $this->name = $service->name;
        $this->price = $service->price;
        $this->service_id = $service->id;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteService($id)
    {
        Service::destroy($id);
        $this->services = $this->getService();

    }

    public function showForm()
    {
        $this->cleanField();
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->cleanField();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $service = Service::find($id);
        $this->name = $service->name;
        $this->price = $service->price;
        $this->service_id = $service->id;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function hideInfo()
    {
        $this->cleanField();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.service-form',
            [
                'services' => Service::where('name', 'like', $search)
                ->paginate(10)
            ]);
    }
}
