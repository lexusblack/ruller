<?php

namespace App\Http\Livewire;

use App\Models\Message;
use App\Models\UsersMessages;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Pagination\Paginator;
use App\Models\Worker;

class MessageList extends Component
{
    public $currentPage = 1;
    public $search;

    use WithPagination;

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function read($users_messages_id)
    {
        $users_message = UsersMessages::find($users_messages_id);
        $users_message->status = 1;
        $users_message->save();
    }

    public function render()

    {

        $users_messages_table = (new UsersMessages())->getTable();
        $message_tables = (new Message())->getTable();
        $worker_table = (new Worker())->getTable();

        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        return view('livewire.message-list', [
            'messages' => UsersMessages::selectRaw($users_messages_table . '.*,' .
                $worker_table . '.first_name as first_name,' .
                $worker_table . '.second_name as second_name,' .
                $message_tables . '.message as message,' .
                $users_messages_table . '.status as status,' .
                $worker_table . '.id as worker_id')
                ->leftJoin($message_tables, $message_tables . '.id', '=', $users_messages_table . '.message_id')
                ->leftJoin($worker_table, $worker_table . '.id', '=', $message_tables . '.user_id')

                ->when(!empty($search), function($query) use ($search, $worker_table) {
                    $query->where(function($q) use ($search, $worker_table) {
                        $q
                            ->where($worker_table . '.first_name', 'like', $search)
                            ->orWhere($worker_table . '.second_name', 'like', $search);
                    });
                })
                ->where($users_messages_table . '.user_id', '=', Auth::id())


                ->orderBy($users_messages_table . '.ttl', 'ASC')
                ->orderBy($users_messages_table . '.status', 'ASC')
                ->orderBy($users_messages_table . '.created_at')
                ->paginate(10)
//        ->toSql()
        ]);


    }
    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }
}
