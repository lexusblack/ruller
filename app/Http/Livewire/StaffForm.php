<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Models\User;
use App\Models\Role;

class StaffForm extends Component
{
    public $roles = [];
    public $staffs = [];
    public $photo,
        $staff_role_name,
        $avatar;
    public $name = '';
    public $email = '';
    public $street = '';
    public $town = '';
    public $profession = '';
    public $phone = '';
    public $role_id = '';
    public $user;
    public $prev = null;
    public $password = '';
    public $show_form = false;
    public $staff_id = null;
    public $staf = [];

    public function updated()
    {
        $this->validate([
            'name' => 'required|min:3',
            'email' => 'required|email'
        ]);
    }

    public function getUsers()
    {
        return User::orderBy('name')
            ->get();
    }

    protected function cleanFields()
    {
        $this->name = '';
        $this->email = '';
        $this->street = '';
        $this->town = '';
        $this->profession = '';
        $this->phone = '';
        $this->role_id = '';
        $this->password = null;
        $this->staff_id = null;
    }

    public function mount()
    {
        $user = auth()->user();
        $this->avatar = $user->photo;


        $this->roles = Role::all();
//        $this->staffs = User::orderBy('name')->get();
        $this->staffs = $this->getUsers();
//        $this->staf = $this->staffs->id;
    }


    public function showInfo($id)
    {
        $this->show_form = true;
        $this->editStaff($id);
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_form = false;
    }

    public function editStaff($id)
    {
        $user = User::find($id);
        $this->name = $user->name;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->role_id = $user->role_id;
        $this->staff_id = $user->id;
        //$this->staf = $user->role->toArryay();
    }

    public function save()
    {

        $this->validate([
            'name' => 'required|min:3',
        ]);

        $password = null;

        if (empty($this->staff_id)) {
            $this->validate([
                'password' => 'required|min:6',
                'email' => 'required|unique:users|email'
            ]);

            $Staff = new User();

            $password = Hash::make($this->password);
        } else {
            $Staff = User::find($this->staff_id);
        }

        $userData = [
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'role_id' => $this->role_id
        ];

        if (!empty($password)) {
            $userData['password'] = $password;
        }

        $Staff->fill($userData);
        $Staff->save();
//        $this->staffs = User::orderBy('name')->get();
        $this->staffs = $this->getUsers();
        $this->cleanFields();


    }

    public function deleteStaff($id)
    {
        if ($id == auth()->user()->id) {
            return;
        }
        User::destroy($id);
        $this->staffs = $this->getUsers();
    }

    public function render()
    {
        return view('livewire.staff-form');
    }
}
