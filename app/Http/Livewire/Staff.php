<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Staff extends Base
{
    public function mount()
    {
        $this->bodyClass = 'text-sm control-sidebar-slide-open layout-navbar-fixed';
        $this->title = trans('words.staff');
        $this->css = [
            '/plugins/fontawesome-free/css/all.min.css',
            '/css/adminlte.min.css',
            '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
        ];
        $this->js = [
            '/plugins/jquery/jquery.min.js',
            '/plugins/bootstrap/js/bootstrap.bundle.min.js',
            '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
            '/js/adminlte.js',
            '/js/demo.js',
        ];
        $this->fonts = [
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback',
            'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'
        ];
    }

    public function render()
    {
        return $this->baseView('livewire.staff');
    }
}
