<?php

namespace App\Http\Livewire;

use App\Models\Profession;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class ProfessionForm extends Component
{
    public $sex_list, $name, $search, $sex, $profession_id;

    public $show_form = false;
    public $show_info = false;

    public function updated()
    {
        $this->validate([
            'name' => 'required|unique:professions|min:3'
        ]);
    }

    use WithPagination;

    public function editProfession($id)
    {
        $profession = Profession::find($id);
        $this->sex = $profession->sex;
        $this->name = $profession->name;
        $this->profession_id = $profession->id;
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function mount()
    {
        $this->sex_list = Profession::SEX_LIST;
        $this->professions = $this->getProfession();
    }

    public function getProfession()
    {
        return Profession::orderBy('name', 'ASC')->get();
    }

    public function cleanFields()
    {
        $this->name = null;
        $this->sex = null;
        $this->profession_id = null;
    }

    public function save()
    {
        $professionData = $this->validate([
            'name' => 'required|min:3' . (empty($this->profession_id) ? '|unique:professions' : ''),
            'sex' => 'required'
        ]);

        if (empty($this->profession_id)) {
            $profession = new Profession();
            $message = trans('words.add');
        } else {
            $profession = Profession::find($this->profession_id);
            $message = trans('words.updated');
        }

        $profession->fill($professionData);
        $profession->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $profession->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->professions = $this->getProfession();
        $this->cleanFields();
        $this->show_form = false;
    }

    public function showForm()
    {
        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function resetSearch()
    {
        $this->search = '';
        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.profession-form', [
            'professions' => Profession::selectRaw('*')
            ->where('name', 'like', $search)
            ->paginate(10)
        ]);
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }
}
