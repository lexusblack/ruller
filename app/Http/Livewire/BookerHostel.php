<?php

namespace App\Http\Livewire;

use App\Models\Settlement;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class BookerHostel extends Component

{
    use WithPagination;

    public $count_worker;
    public $active_worker;
    public $finish_worker;
    public $day_for_view_next_month;
    public $current_month;
    public $all_records_hostel;
    public $hostels_name;
    public $first_name;
    public $second_name;
    public $description;
    public $search;

    public $selcted_year = 0;
    public $selcted_month = 0;
    public $salary_workers = [];

    public $workers = [];
    public $months_option = [];
    public $years_option = [];
    public $workers_salary = [];
    public $workers_sponsor = [];
    public $workers_service = [];
    public $workers_penalty = [];
    public $workers_settlement = [];
    public $statuses = [];
    public $hostel_id;

    public function mount()
    {
        $this->makeYears();
        $this->selcted_year = Carbon::now()->format('Y');

        $this->makeMonth();
        $this->selectedMonth(Carbon::now()->format('n'));

    }


    public function makeYears()
    {
        $years = Settlement::selectRaw('YEAR(start_hostel) as year')->groupBy('year')->orderBy('year', 'DESC')->get();
        $this->years_option = $this->formatOption($years, 'year', 'year');
    }

    public function makeMonth()
    {
        Carbon::setLocale('ru');

        $month = Settlement::selectRaw('MONTH(start_hostel) as month')
            ->whereYear('start_hostel', $this->selcted_year)
            ->groupBy('month')
            ->orderBy('month', 'ASC')
            ->get();

        $month->transform(function ($item) {
            return [
                'value' => $item->month,
                'text' => mb_convert_case(Carbon::parse('01-' . $item->month . '-' . $this->selcted_year)->monthName, MB_CASE_TITLE, "UTF-8")
            ];
        });
        $this->months_option = $this->formatOption($month);
    }

    protected function formatOption($data, $value = 'value', $text = 'text')
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = [
                'value' => $item[$value],
                'text' => $item[$text]
            ];
        }
        return $result;
    }

    public function selectedYear($value)
    {
        $this->selcted_year = $value;
        $this->reset(['selcted_month', 'hostels']);
        $this->makeMonth();
        $this->reset(['hostel_id']);

    }
    public function selectedMonth($value)
    {
        $this->selcted_month = $value;

        $this->reset(['hostel_id']);
    }

    public function setHostelActiveView($hostel_id)
    {
        $this->hostel_id = $hostel_id;
        $this->workers = Settlement::selectRaw('SUM(price) as sum, worker_id')
            ->whereMonth('start_hostel', $this->selcted_month)
            ->whereYear('start_hostel', $this->selcted_year)
            ->where('hostel_id', '=', $hostel_id)
            ->groupBy('worker_id')
            ->get();
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
        $this->reset('hostel_id');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }


    public function render()
    {

        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        return view('livewire.booker-hostel',[
            'hostels' => Settlement::selectRaw('SUM(price) as sum, hostel_id, COUNT(DISTINCT worker_id) as count_worker')
                ->leftJoin('hostels', 'hostels.id', 'settlements.hostel_id')
                ->whereMonth('settlements.start_hostel', $this->selcted_month)
                ->whereYear('settlements.start_hostel', $this->selcted_year)
                ->where('hostels.name', 'like', $search)
                ->groupBy('settlements.hostel_id')
                ->orderBy('sum', 'DESC')
                ->paginate(10)
        ]);
    }
}
