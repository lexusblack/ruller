<?php

namespace App\Http\Livewire;

use App\Models\Employment;
use App\Models\Status;
use App\Models\Worker;
use App\Models\Service;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class StatusForm extends Component
{
    public $worker_id,
        $service_id,
        $date_start,
        $date_end,
        $status,
        $status_list,
        $service,
        $status_id,
        $price,
        $servicePrice,
        $show_form = false,
        $show_info = false,
        $show_update = false,
        $updatePage = 0,
        $up = false;

    public $workers = [];
    public $services = [];
//    public $statuss = [];
    public $worker = [];
    public $servic = [];
    public $search;
    public $currentPage;

    use WithPagination;

    public function updated()
    {
        $this->validate([
            'service_id' => 'required',
            'worker_id' => 'required',
            'date_start' => 'required',
            'status' => 'required'
        ]);
    }

    public function mount()
    {
        $this->status_list = Status::STATUS_LIST;
        $this->workers = $this->getWorkers();
        $this->services = $this->getServices();
        $this->statuss = $this->getStatus();
    }

    public function getWorkers()
    {
        return Worker::whereHas('employments', function ($query) {
            $query->where('status', '=', Employment::STATUS_ACTIVE);
        })->get();
    }

    public function getServices()
    {
        return Service::orderBy('name', 'ASC')->get();
    }

    public function getStatus()
    {
        return Status::orderBy('status', 'ASC')->get();
    }

    public function save()
    {
        $this->validate([
            'service_id' => 'required',
            'worker_id' => 'required',
            'date_start' => 'required',
            'status' => 'required',
        ]);

        $statusdat = [
            'worker_id' => $this->worker_id,
            'service_id' => $this->service_id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'status' => $this->status,
            'price' => $this->price,
        ];

        if (empty($this->status_id)) {
            $status = new Status();
            $service = Service::find($this->service_id);
            $statusdat = array_merge($statusdat, ['price' => $service->price]);
            $message = trans('words.add');
        } else {
            $status = Status::find($this->status_id);
            if ($this->price == 0 || $this->up != false) {
                $statusdat = array_merge($statusdat, ['price' => $status->service->price]);
                $message = trans('words.updated');
            }
        }

        $status->fill($statusdat);
        $status->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $status->service->name . ' for ' . $status->worker->first_name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->reset(['date_start', 'date_end', 'status', 'worker_id', 'service_id','up','price','servicePrice']);
        $this->resetErrorBag();
        $this->resetValidation();
        $this->statuss = $this->getStatus();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $status = Status::find($id);
        $this->service_id = $status->service_id;
        $this->worker_id = $status->worker_id;
        $this->date_start = $status->date_start;
        $this->date_end = $status->date_end;
        $this->status = $status->status;
        $this->status_id = $status->id;
        $this->price = $status->price;
        $this->worker = $status->worker->toArray();
        $this->servic = $status->service->toArray();
        $this->servicePrice = $status->service->price;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->reset(['date_start', 'date_end', 'status', 'worker_id', 'service_id','up','price','servicePrice']);
        $this->show_info = false;
        $this->show_form = false;
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function showForm()
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->reset(['date_start', 'date_end', 'status', 'worker_id', 'service_id','up','price','servicePrice']);
        $this->show_form = true;
        $this->show_info = false;
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function hideForm()
    {
        $this->reset(['date_start', 'date_end', 'status', 'worker_id', 'service_id','up','price','servicePrice']);
        $this->show_form = false;
        $this->show_info = false;
        $this->resetErrorBag();
        $this->resetValidation();
    }


    public function editStatus($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $status = Status::find($id);
        $this->service_id = $status->service_id;
        $this->worker_id = $status->worker_id;
        $this->date_start = $status->date_start;
        $this->date_end = $status->date_end;
        $this->status = $status->status;
        $this->status_id = $status->id;
        $this->price = $status->price;
        $this->servicePrice = $status->service->price;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteStatus($id)
    {
        Status::destroy($id);
        $this->statuss = $this->getStatus();
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        $status_table = (new Status())->getTable();
        $worker_table = (new Worker())->getTable();
        $service_table = (new Service())->getTable();

        return view('livewire.status-form',
            [
                'statuss' => Status::selectRaw($status_table . '.*,' .
                $worker_table . '.first_name,' .
                $worker_table . '.second_name,' .
                $service_table . '.name')
                ->leftJoin($worker_table, $worker_table . '.id', '=', $status_table . '.worker_id')
                ->leftJoin($service_table, $service_table . '.id', '=', $status_table . '.service_id')
                ->where($worker_table . '.first_name', 'like', $search)
                ->orWhere($worker_table . '.second_name', 'like', $search)
                ->orWhere($service_table . '.name', 'like', $search)
                ->paginate(10)
            ]);
    }
}
