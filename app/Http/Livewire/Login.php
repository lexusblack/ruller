<?php

namespace App\Http\Livewire;


use Illuminate\Support\Facades\Auth;

class Login extends Base
{

    public $email;
    public $name;
    public $password;
    public $remember = false;

    protected $rules = [
        'email' => 'email',
        'password' => 'required',
    ];

    public function mount()
    {

        $this->bodyClass = 'login-page';
//        $this->title = 'Login';
        $this->title = trans('words.login_title');
        $this->css = [
            '/plugins/fontawesome-free/css/all.min.css',
            '/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
            '/css/adminlte.min.css'
        ];
        $this->js = [
            '/plugins/jquery/jquery.min.js',
            '/plugins/bootstrap/js/bootstrap.bundle.min.js',
            '/js/adminlte.min.js'
        ];
        $this->fonts = [
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback'
        ];
    }

    public function updatedEmail()
    {
        $this->validate(['email' => 'email']);
    }


    /**
     * @return Login|\Illuminate\Http\RedirectResponse
     */
    public function login()
    {
        $this->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        return auth()->attempt(['email' => $this->email, 'password' => $this->password], $this->remember)
            ? redirect()->intended('/dashboard')
            : $this->addError('email', trans('auth.failed'));
    }


    public function render()
    {
        return $this->baseView('livewire.login');
    }
}
