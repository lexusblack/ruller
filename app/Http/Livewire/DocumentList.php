<?php

namespace App\Http\Livewire;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;
use PhpOffice\PhpWord\TemplateProcessor;
use Livewire\WithFileUploads;
use App\Models\DocTemplate;

class DocumentList extends Component
{
    use WithFileUploads;


    public $show_form = false;
    public $show_info = false;
    public $docx;
    public $name;
    public $path;
    public $category;
//    public $templates;
    public $template_id;
    public $search;
    public $currentPage = 1;

    use WithPagination;

    public function updated(): void
    {
        $this->validate([
            'docx' => 'mimes:docx,xlsx|max:2048',
            'category' => 'required'
        ]);
    }

    public function getTemplates(): Collection
    {
        return DocTemplate::orderBy('name')->get();
    }

    public function save(): void
    {



        $template_file = '';
        if (empty($this->template_id)) {
            $template = new DocTemplate();
            $template_file = $this->docx->store('/', 'document_templates');
            $message = trans('words.add');

            $this->validate([
                'docx' =>  'mimes:docx,xlsx|max:2048', // 1MB Max
                'category' => 'required'
            ]);

        } else {
            $template = DocTemplate::find($this->template_id);
            $message = trans('words.updated');

            $this->validate([
//                'docx' =>  'mimes:docx,xlsx|max:2048', // 1MB Max
                'category' => 'required'
            ]);
        }

        $template_data = [
            'name' => $this->name,
            'category' => $this->category
        ];

        if (!empty($template_file)) {
            $template_data['path'] = $template_file;
        }

        $template->fill($template_data);

        $template->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $template->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->cleanFields();
        $this->templates = $this->getTemplates();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function mount(): void
    {
        $this->templates = $this->getTemplates();

    }

    public function editTemplate($id): void
    {
        $this->cleanFields();
        $template = DocTemplate::find($id);
        $this->name = $template->name;
        $this->template_id = $template->id;
        $this->category = $template->category;
        $this->show_form = true;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteTemplate($id)
    {
        DocTemplate::destroy($id);
        $this->templates = $this->getTemplates();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showForm(): void
    {
        $this->show_form = true;
        $this->show_info = false;
//        $this->dispatchBrowserEvent('init-date-field');
        $this->cleanFields();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm(): void
    {
        $this->show_form = false;
        $this->show_info = false;
        $this->cleanFields();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function cleanFields(): void
    {
        $this->reset(['name', 'category', 'template_id']);
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.document-list', [
            'templates' => DocTemplate::where('name', 'like', $search)
            ->paginate(10)
        ]);
    }
}
