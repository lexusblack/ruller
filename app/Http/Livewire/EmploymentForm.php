<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Worker;
use App\Models\Job;
use App\Models\Employment;
use App\Models\Profession;


class EmploymentForm extends Component
{
    public
        $worker_id = null,
        $job_id,
        $start,
        $stop,
        $comment,
        $employment_id,
        $status,
        $search,
        $dismissal,
        $show_form = false;
    public $role_id;
    public $show_info = false;

    public $workers = [];
    public $worker = [];
    public $job = [];
    public $jobs;

    public $statuses = [];

    use WithPagination;

    public function setEmptyJobs()
    {
        $test = ['id' => 0, 'description' => 'Choise Worker'];
        $this->jobs = collect();
        $this->jobs->push($test);
    }

    public function resetFields()
    {
        $this->reset(['worker_id', 'job_id', 'start', 'stop', 'comment', 'dismissal','employment_id']);
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function updated()
    {
        $this->validate([
            'worker_id' => 'required',
            'job_id' => 'required',
            'start' => 'required'
        ]);
    }

    public function mount()
    {
        $this->setEmptyJobs();
        $this->workers = $this->getWorkers();
        $this->employments = $this->getEmployments();
        $this->statuses = [
            Employment::STATUS_ACTIVE => 'Active',
            Employment::STATUS_FINISH => 'Finished',
        ];
    }

    public function getWorkers()
    {
        return Worker::orderBy('first_name', 'ASC')->get();
    }

    public function getJobs()
    {
        $query = Job::orderBy('description', 'ASC');
        if (!empty($this->worker_id)) {
            $this->reset(['job_id']);
            $worker_sex = Worker::find($this->worker_id)->sex;

            $professions = Profession::whereIN('sex', [$worker_sex, 'unisex'])->pluck('id');
            $query->whereIN('profession_id', $professions)->where('place', '>', 0);
        }

        if ($query->count() == 0) {
            $this->setEmptyJobs();
            return;
        }

        $this->jobs = $query->get();
    }

    public function getEmployments()
    {
        return Employment::all();
    }

    public function save()
    {
        $employmentData = $this->validate([
            'worker_id' => 'required',
            'job_id' => 'required',
            'start' => 'required',
            'stop' => '',
            'status' => '',
            'comment' => '',
            'dismissal' => ''
        ]);

        if (empty($this->employment_id)) {
            $employment = new Employment();
            $message = trans('words.add');
            $this->new_job = true;
        } else {
            $employment = Employment::find($this->employment_id);
            $message = trans('words.updated');

        }

        if (empty($employmentData['status'])) {
            $employmentData['status'] = Employment::STATUS_ACTIVE;
        }

//        if (empty($employmentData['stop'])) {
//            $employmentData['stop'] = null;
//        }

        if ($employmentData['status'] == Employment::STATUS_FINISH AND $employmentData['stop'] == null){
            $employmentData['stop'] = Carbon::now();
        }

        if ($employmentData['status'] == Employment::STATUS_ACTIVE AND $employmentData['stop'] <= Carbon::now()){
            $employmentData['stop'] = null;
        }

        $employment->fill($employmentData);
        $employment->save();
        $place_count = $employment->job->place;
        $job = $employment->job;
        $job->place = $place_count - 1;
        $job->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $employment->job->description . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->resetFields();

        $this->employments = $this->getEmployments();
        $this->setEmptyJobs();
        $this->show_form = false;
    }

    public function editEmployment($id)
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->show_form = true;
        $this->show_info = false;
        $employment = Employment::find($id);
        $this->employment_id = $employment->id;
        $this->worker_id = $employment->worker_id;
//        $this->getJobs();
        $this->job_id = $employment->job_id;
        $this->start = $employment->start;
        $this->stop = $employment->stop;
        $this->comment = $employment->comment;
        $this->status = $employment->status;
        $this->dismissal = $employment->dismissal;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deleteEmployment($id)
    {
        Employment::destroy($id);
        $this->setEmptyJobs();
        $this->employments = $this->getEmployments();
    }

    public function showForm()
    {
        $this->dispatchBrowserEvent('init-date-field');
        $this->resetFields();
        $this->show_form = true;
        $this->show_info = false;
    }

    public function hideForm()
    {
        $this->resetFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $employment = Employment::find($id);
        $this->employment_id = $employment->id;
        $this->worker_id = $employment->worker_id;
        $this->job_id = $employment->job_id;
        $this->start = $employment->start;
        $this->stop = $employment->stop;
        $this->comment = $employment->comment;
        $this->dismissal = $employment->dismissal;
        $this->status = $employment->status;
        $this->worker = $employment->worker->toArray();
        $this->job = $employment->job->toArray();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->resetFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function render()
    {
        $workers_table = (new Worker())->getTable();
        $employment_table = (new Employment())->getTable();

        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        return view('livewire.employment-form', [

            'employments' => Employment::select($workers_table . '.*', $employment_table . '.*', $employment_table . '.id as e_id')
                ->join($workers_table, $employment_table . '.worker_id', '=', $workers_table . '.id')
                ->whereRaw($employment_table . '.worker_id = ' . $workers_table . '.id')
                ->where($workers_table . '.first_name', 'like', $search)
                ->orWhere($workers_table . '.second_name', 'like', $search)
                ->orWhere($workers_table . '.phone','like',$search)
                ->orWhere($workers_table . '.pesel','like',$search)
                ->paginate(10)
        ]);
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }
}
