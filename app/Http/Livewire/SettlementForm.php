<?php

namespace App\Http\Livewire;

use App\Models\Employment;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Hostel;
use App\Models\Worker;
use App\Models\Settlement;
use Illuminate\Support\Carbon;
use Livewire\WithPagination;

class SettlementForm extends Component
{
    public $settlement_id,
        $worker_id,
        $start_hostel,
        $stop_hostel,
        $hostel_id,
        $price;

    public $search;
    public $currentPage;

    public $start_min_date;
    public $start_max_date;
    public $stop_min_date;
    public $stop_max_date;

    public $hostels = [];
    public $workers = [];
//    public $settlements = [];
    public $hostel = [];
    public $worker = [];

    public $show_form = false;
    public $show_info = false;

    use WithPagination;

    public function mount()
    {
        $this->hostels = Hostel::orderBy('name', 'ASC')->get();
//        $this->workers = Worker::orderBy('first_name', 'ASC')->get();

        $this->workers = Worker::whereHas('employments', function ($query) {
            $query->where('status', '=', Employment::STATUS_ACTIVE);
        })->get();

        $this->settlements = $this->getSettlements();
        $this->start_min_date = Carbon::parse('now')->format('Y-m-d');
        $this->stop_min_date = Carbon::parse('now')->addDay('Y-m-d');
    }

    protected $listeners = [
        'set-stop-min-date' => 'setStopMinDate',
        'set-stop-date' => 'setStopDate'
    ];

    public function setStopMinDate($value, $stop = null)
    {
        $this->start_hostel = $value;
        $this->stop_min_date = Carbon::parse($value)->addDay()->format('Y-m-d');
        $this->stop_max_date = Carbon::parse($value)->endOfMonth()->format('Y-m-d');
        $this->dispatchBrowserEvent('say-goodbye', ['min' => $this->stop_min_date, 'max' => $this->stop_max_date]);
        if (empty($stop)) {
            $this->stop_hostel = $this->stop_max_date;
        } else {
            $this->stop_hostel = $stop;
        }
    }

    public function setStopDate($value)
    {
        $this->stop_hostel = $value;
    }

    public function updated()
    {
        $this->validate([
            'start_hostel' => '',
            'stop_hostel' => '',
            'price' => 'required',
            'worker_id' => 'required',
            'hostel_id' => 'required'
        ]);
    }

    public function editSettlement($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $settlement = Settlement::find($id);
        $this->settlement_id = $settlement->id;
        $this->worker_id = $settlement->worker_id;
        $this->start_hostel = $settlement->start_hostel;
        $this->stop_hostel = $settlement->stop_hostel;
        $this->hostel_id = $settlement->hostel_id;
        $this->price = $settlement->price;
        $this->setStopMinDate($this->start_hostel, $this->stop_hostel);
        $this->dispatchBrowserEvent('toTopPage');
        $this->dispatchBrowserEvent('init-data-fields');
    }

    public function getSettlements()
    {
        return Settlement::all();
    }

    public function save()
    {
        $this->validate([
            'start_hostel' => 'required',
            'stop_hostel' => 'required',
            'price' => 'required',
            'worker_id' => 'required',
            'hostel_id' => 'required',
        ]);

        if (empty($this->settlement_id)) {
            $settlement = new Settlement();
            $message = trans('words.add');
        } else {
            $settlement = Settlement::find($this->settlement_id);
            $message = trans('words.updated');
        }

        $settlementData = [
            'worker_id' => $this->worker_id,
            'start_hostel' => Carbon::parse($this->start_hostel),
            'stop_hostel' => Carbon::parse($this->stop_hostel),
            'hostel_id' => $this->hostel_id,
            'price' => $this->price,
        ];

        $settlement->fill($settlementData);
        $settlement->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => 'Setlement for worker ' . $settlement->worker->first_name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->reset(array_keys($settlementData));
        $this->resetErrorBag();
        $this->resetValidation();
        $this->settlements = $this->getSettlements();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');


    }

    public function deleteSettlement($id)
    {
        Settlement::destroy($id);
        $this->settlements = $this->getSettlements();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $settlement = Settlement::find($id);
        $this->settlement_id = $settlement->id;
        $this->worker_id = $settlement->worker_id;
        $this->start_hostel = $settlement->start_hostel;
        $this->stop_hostel = $settlement->stop_hostel;
        $this->hostel_id = $settlement->hostel_id;
        $this->price = $settlement->price;
        $this->worker = $settlement->worker->toArray();
        $this->hostel = $settlement->hostel->toArray();
        $this->dispatchBrowserEvent('toTopPage');
    }


    public function hideInfo()
    {
        $this->reset(['worker_id', 'start_hostel', 'stop_hostel', 'hostel_id', 'price']);
        $this->show_info = false;
        $this->show_form = false;
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showForm()
    {
        $this->reset(['worker_id', 'start_hostel', 'stop_hostel', 'hostel_id', 'price']);
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('init-data-fields');
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');
        // init-data-fields
    }

    public function hideForm()
    {
        $this->reset(['worker_id', 'start_hostel', 'stop_hostel', 'hostel_id', 'price']);
        $this->show_form = false;
        $this->show_info = false;
        $this->resetErrorBag();
        $this->resetValidation();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function () {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if (!empty($this->search)) {
            Paginator::currentPageResolver(function () {
                return 0;
            });
        }

        $settlements_table = (new Settlement())->getTable();
        $worker_table = (new Worker())->getTable();
        $hostel_table = (new Hostel())->getTable();

        return view('livewire.settlement-form', [
            'settlements' => Settlement::selectRaw($settlements_table . '.*,' .
                $worker_table . '.first_name,' .
                $worker_table . '.second_name,' .
                $hostel_table . '.name')
                ->leftJoin($worker_table, $worker_table . '.id', '=', $settlements_table . '.worker_id')
                ->leftJoin($hostel_table, $hostel_table . '.id', '=', $settlements_table . '.hostel_id')
                ->where($worker_table . '.first_name', 'like', $search)
                ->orWhere($worker_table . '.second_name', 'like', $search)
                ->orWhere($hostel_table . '.name', 'like', $search)
                ->paginate(10)
        ]);
    }
}
