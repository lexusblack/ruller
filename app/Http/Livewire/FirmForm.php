<?php

namespace App\Http\Livewire;

use App\Models\Firm;
use App\Models\Town;
use Illuminate\Pagination\Paginator;
use Livewire\Component;


class FirmForm extends Component
{
//    public $firms = [];
    public $towns = [];
    public $town = [];
    public $firm_id, $name, $adress, $nip, $town_id, $postal_code, $contact_name, $phone, $start_work, $stop_work, $search, $currentPage;

    public $show_form = false, $show_info = false;

    public function updated()
    {
        $this->validate([
            'name' => 'required|unique:firms|min:3',
            'adress' => 'required',
            'nip' => 'required|min:5',
            'town_id' => 'numeric|min:0|not_in:0|required',
            'postal_code' => 'required|min:6|max:6',
            'contact_name' => 'required',
            'phone' => 'required'
        ]);
    }

    public function mount()
    {
        $this->firms = $this->getFirms();
        $this->towns = $this->getTowns();
    }

    public function getFirms()
    {
        return Firm::orderBy('name', 'ASC')->get();
    }

    public function getTowns()
    {
        return Town::orderBy('name', 'ASC')->get();
    }

    public function editFirm($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $firm = Firm::find($id);
        $this->firm_id = $firm->id;
        $this->name = $firm->name;
        $this->adress = $firm->adress;
        $this->nip = $firm->nip;
        $this->town_id = $firm->town_id;
        $this->postal_code = $firm->postal_code;
        $this->contact_name = $firm->contact_name;
        $this->phone = $firm->phone;
        $this->start_work = $firm->start_work;
        $this->stop_work = $firm->stop_work;
        $this->dispatchBrowserEvent('toTopPage');
        $this->dispatchBrowserEvent('init-date-field');

    }

    public function save()
    {
        $firmData = $this->validate([
            'name' => 'required|min:3' . (empty($this->firm_id) ? '|unique:firms' : ''),
            'adress' => 'required',
            'nip' => 'required|min:5',
            'town_id' => 'numeric|min:0|not_in:0|required',
            'postal_code' => 'required|min:6|max:6',
            'contact_name' => 'required',
            'phone' => 'required',
            'start_work' => '',
            'stop_work' => '',
        ]);

        if (empty($this->firm_id)) {
            $firm = new Firm();
            $message = trans('words.add');
        } else {
            $firm = Firm::find($this->firm_id);
            $message = trans('words.updated');
        }

        $firm->fill($firmData);
        $firm->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $firm->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->firms = $this->getFirms();
        $this->cleanFields();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function cleanFields()
    {
        $this->firm_id = '';
        $this->name = '';
        $this->adress = '';
        $this->nip = '';
        $this->town_id = '';
        $this->postal_code = '';
        $this->contact_name = null;
        $this->phone = null;
        $this->start_work = null;
        $this->stop_work = null;
        $this->resetErrorBag();
        $this->resetValidation();

    }

    public function showForm()
    {

        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('init-date-field');
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $firm = Firm::find($id);
        $this->firm_id = $firm->id;
        $this->name = $firm->name;
        $this->adress = $firm->adress;
        $this->nip = $firm->nip;
        $this->town_id = $firm->town_id;
        $this->postal_code = $firm->postal_code;
        $this->contact_name = $firm->contact_name;
        $this->phone = $firm->phone;
        $this->start_work = $firm->start_work;
        $this->stop_work = $firm->stop_work;
        $this->town = $firm->town->toArray();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.firm-form',
            [
                'firms' => Firm::where('name', 'like', $search)
                ->orWhere('adress', 'like', $search)
                ->orWhere('nip', 'like', $search)
                ->orWhere('postal_code', 'like', $search)
                ->paginate(10)
            ]);
    }
}
