<?php

namespace App\Http\Livewire;

use App\Models\Town;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;

class TownForm extends Component
{
    public $name = null;
    public $town_id = null;
    public $search;
    public $currentPage;

    public $show_info = false;
    public $show_form = false;

    use WithPagination;

    public function updated()
    {
        $this->validate([
            'name' => 'required|min:3' . (empty($this->town_id) ? '|unique:towns' : ''),
        ]);
    }

    public function mount ()
    {
        $this->getTownList();
    }

    public function editTown($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $town = Town::find($id);
        $this->name = $town->name;
        $this->town_id = $town->id;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function save()
    {
        $townData = $this->validate([
            'name' => 'required|min:3' . (empty($this->town_id) ? '|unique:towns' : ''),
        ]);

        if (empty($this->town_id)) {
            $town = new Town();
            $message = trans('words.add');
        }
        else {
            $town = Town::find($this->town_id);
            $message = trans('words.updated');
        }

        $town->fill($townData);
        $town->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $town->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);

        $this->getTownList();
        $this->cleanFields();
        $this->show_form = false;
    }

    public function getTownList()
    {
        $this->towns = Town::orderBy('name', 'ASC')->get();
    }

    public function cleanFields()
    {
        $this->name = null;
        $this->town_id = null;
        $this->resetErrorBag();
        $this->resetValidation();

    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function showForm()
    {
        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }

    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.town-form',[
            'towns' => Town::where('name', 'like', $search)
            ->paginate(10)
        ]);
    }
}
