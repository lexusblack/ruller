<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Traits\DataToTemplate;

class Base extends Component
{
    use DataToTemplate;

    public function mount()
    {
        $this->bodyClass = '';
        $this->title = '';
        $this->css = [];
        $this->js = [];
        $this->fonts = [];
    }

    /**
     * @return mixed
     */
    public function render()
    {
        return $this->baseView('livewire.login');
    }

}
