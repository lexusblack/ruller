<?php

namespace App\Http\Livewire;

use Livewire\Component;


class BookerForm extends Component
{

    public $show_form_hostel;
    public $show_form_salary;
    public $show_form_any;

    public function mount()
    {
        $this->show_form_any = true;
        $this->show_form_salary = false;
        $this->show_form_hostel = false;
    }

    public function showInfoSalary()
    {
        $this->show_form_hostel = false;
        $this->show_form_salary = true;
        $this->show_form_any = false;
    }

    public function showInfoHostel()
    {
        $this->show_form_hostel = true;
        $this->show_form_salary = false;
        $this->show_form_any = false;
    }

    public function showInfoAny()
    {
        $this->show_form_hostel = false;
        $this->show_form_salary = false;
        $this->show_form_any = true;
    }

    public function render()
    {
        return view('livewire.booker-form');
    }
}
