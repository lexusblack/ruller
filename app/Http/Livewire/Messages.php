<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UsersMessages;
use Livewire\Component;
use App\Services\UserMessages;

class Messages extends Component
{

    protected $listeners = ['setReaded' => 'setReadiInMessage'];

    public function setReadiInMessage($user_message_id)
    {
        $user_message_obj = UsersMessages::find($user_message_id);
        $user_message_obj->status = 1;
        $user_message_obj->save();

        $worker_id = $user_message_obj->message->user_id;
        $url = route('worker') . '?user=' . $worker_id;
        redirect($url);
    }


    public function render()
    {
        return view('livewire.messages');
    }
}
