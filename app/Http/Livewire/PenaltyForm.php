<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\Paginator;
use Livewire\Component;
use App\Models\Penalty;
use Livewire\WithPagination;

class PenaltyForm extends Component
{
//    public $penaltys = [];
    public $show_form = false;
    public $show_info = false;

    public $penalty_id, $name, $price, $description, $search, $currentPage;

    use WithPagination;
    /**
     *
     */
    public function mount()
    {
        $this->penaltys = $this->getPenaltys();
    }

    public function updated()
    {
        $this->price = str_replace(',', '.', $this->price);
        $this->validate([
            'name' => 'required|min:3',
            'price' => 'required|numeric',
            'description' => 'required|min:3'
        ]);
    }
    /**
     * @return mixed
     */
    public function getPenaltys()
    {
        return Penalty::orderBy('name', 'ASC')->get();
    }

    public function cleanFields()
    {
        $this->reset(['name','price','description']);
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function save()
    {
        $this->validate([
            'name' => 'required|min:3',
            'price' => 'required:numeric',
            'description' => 'required|min:3',
        ]);

        if (empty($this->penalty_id)) {
            $penalty = new Penalty();
            $message = trans('words.add');
        } else {
            $penalty = Penalty::find($this->penalty_id);
            $message = trans('words.updated');
        }

        $penaltyData = [
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
        ];

        $penalty->fill($penaltyData);
        $penalty->save();
        $this->dispatchBrowserEvent('messageSuccess', [
            'message' => $penalty->name . ' ' . $message . ' ' . trans('words.success') . '!'
        ]);
        $this->reset(array_keys($penaltyData));
        $this->penaltys = $this->getPenaltys();
        $this->show_form = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function editPenalty($id)
    {
        $this->show_form = true;
        $this->show_info = false;
        $penalty = Penalty::find($id);
        $this->penalty_id = $penalty->id;
        $this->name = $penalty->name;
        $this->price = $penalty->price;
        $this->description = $penalty->description;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function deletePenalty($id)
    {
        Penalty::destroy($id);
        $this->penaltys = $this->getPenaltys();
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function showInfo($id)
    {
        $this->show_form = false;
        $this->show_info = true;
        $penalty = Penalty::find($id);
        $this->penalty_id = $penalty->id;
        $this->name = $penalty->name;
        $this->price = $penalty->price;
        $this->description = $penalty->description;
        $this->dispatchBrowserEvent('toTopPage');

    }

    public function hideInfo()
    {
        $this->cleanFields();
        $this->show_info = false;
        $this->show_form = false;
    }

    public function showForm()
    {
        $this->cleanFields();
        $this->show_form = true;
        $this->show_info = false;
        $this->dispatchBrowserEvent('toTopPage');
    }

    public function hideForm()
    {
        $this->cleanFields();
        $this->show_form = false;
        $this->show_info = false;
    }
    public function setPage($page)
    {
        $this->currentPage = $page;
        Paginator::currentPageResolver(function ()
        {
            return $this->currentPage;
        });
    }

    public function resetSearch()
    {
        $this->search = '';
//        $this->emit('urlChange', '/worker');
    }

    public function render()
    {
        $search = '%' . $this->search . '%';
        if(!empty($this->search)) {
            Paginator::currentPageResolver(function () {return 0;});
        }

        return view('livewire.penalty-form',[
            'penaltys' => Penalty::where('name', 'like', $search)
            ->orwhere('description', 'like', $search)
            ->paginate(10)
        ]);
    }
}
