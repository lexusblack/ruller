<?php


namespace App\Services;

use App\Models\Message;
use App\Models\Settlement;
use App\Models\UsersMessages;
use App\Models\Worker;
use App\Models\Employment;
use App\Models\Hostel;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class UserMessages
{

    protected $messages = null;
    protected $count = null;
    protected $type_run = null;
    protected $message_table;
    protected $worker_table;
    protected $user_message_table;
    protected $employment_table;
    protected $settlement_table;
    protected $hostels_table;
    protected $roles_table;
    protected $users_table;

    protected $buh = 'buh';
    protected $admin = 'admin';
    protected $owner = 'owner';
    protected $kadr = 'kadr';
    protected $rekrut = 'rekrut';

    public $send_to;
    public $send_to_booker = [];
    public $send_to_admin = [];


    public function __construct($type = 'site')
    {
        $this->type_run = $type;
        $this->message_table = (new Message())->getTable();
        $this->worker_table = (new Worker())->getTable();
        $this->user_message_table = (new UsersMessages())->getTable();
        $this->employment_table = (new Employment())->getTable();
        $this->hostels_table = (new Hostel())->getTable();
        $this->users_table = (new User())->getTable();
        $this->roles_table = (new Role())->getTable();
        $this->settlement_table = (new Settlement())->getTable();

    }

    public function getAdmins()
    {
        return User::selectRaw($this->users_table . '.*')
            ->leftJoin($this->roles_table, $this->roles_table . '.id', '=', $this->users_table . '.role_id')
            ->where($this->roles_table . '.name', '=', $this->admin)
            ->get();
    }

    public function getBookers()
    {
        return User::selectRaw($this->users_table . '.*')
            ->leftJoin($this->roles_table, $this->roles_table . '.id', '=', $this->users_table . '.role_id')
            ->where($this->roles_table . '.name', '=', $this->rekrut)
            ->orWhere($this->roles_table . '.name', '=', $this->admin)
            ->get();
    }


    public function messages($user_id)
    {

        if ($this->messages === null) {
            $this->getMessage($user_id);
        }

        return [
            'items' => $this->messages->toArray(),
            'total' => $this->count
        ];

    }

    public function storage()
    {
        return Storage::disk('avatars');
    }

    public function count($user_id)
    {
        if ($this->count === null) {
            $this->getMessage($user_id);
        }

        return $this->count;
    }

    protected function getMessage($user_id)
    {

        $query = UsersMessages::selectRaw(
            $this->worker_table . '.first_name as first_name,' .
            $this->worker_table . '.second_name as second_name, ' .
            $this->worker_table . '.id as worker_id,' .
            $this->message_table . '.type as type,' .
            $this->message_table . '.message as message,' .
            $this->message_table . '.created_at as created_at, 
            users_messages.id as id'
        )
            ->leftJoin($this->message_table, $this->message_table . '.id', 'users_messages.message_id')
            ->leftJoin($this->worker_table, $this->worker_table . '.id', $this->message_table . '.user_id')
            ->where([
                ['users_messages.user_id', '=', $user_id],
                ['users_messages.status', '=', 0]
            ])
            ->where(function ($sub_query) {
                return $sub_query->where('users_messages.ttl', '>', Carbon::now())->orWhereNull('ttl');
            });

        $this->count = $query->count();
        $this->messages = $query->limit(5)->get();
    }

    public function createVisaMessages()
    {
        if ($this->type_run != 'cron') {
            return;
        }

        $now = Carbon::now();
        $date_for_message = $now->clone()->addDays(60)->startOfDay();

        $workers = Worker::selectRaw($this->worker_table . '.*')
            ->leftJoin($this->employment_table, $this->employment_table . '.worker_id', $this->worker_table . '.id')
            ->where([
                [$this->worker_table . '.date_stop_visa', '<=', $date_for_message],
                [$this->worker_table . '.date_stop_visa', '>=', $now],
                [$this->employment_table . '.status', '=', Employment::STATUS_ACTIVE]
            ])->get();

        foreach ($workers as $worker) {

            $date_stop_visa = Carbon::parse($worker->date_stop_visa);
            $secomd_message_start = $date_stop_visa->clone()->subDays(7)->startOfDay();
            $secomd_message_end = $secomd_message_start->clone()->endOfDay();

            $messages = UsersMessages::selectRaw($this->user_message_table . '.created_at as created_at')
                ->leftJoin($this->message_table, $this->message_table . '.id', $this->user_message_table . '.message_id')
                ->where([
                    [$this->message_table . '.user_id', '=', $worker->id],
                    [$this->message_table . '.branch', '=', Employment::MESSAGE_BRANCH]
                ])
                ->get();

            if ($messages->count() > 0) {
                $current_message = $messages->firstWhere('created_at', '>=', $date_for_message);
                if (!empty($current_message)) {
                    continue;
                }
                if (!$now->between($secomd_message_start, $secomd_message_end)) {
                    continue;
                }
            }

            $message = Message::create([
                'message' => 'У меня заканчивается виза ' . $date_stop_visa->format('d-m-Y') . '.',
                'user_id' => $worker->id,
                'type' => 'danger',
                'branch' => Employment::MESSAGE_BRANCH
            ]);

            $users_send_to = $this->getBookers();
//            var_dump($users_send_to);
            foreach ($users_send_to as $user) {
                $message->usersMessages()->create([
                    'user_id' => $user->id,
                    'ttl' => $date_stop_visa
                ]);
            }
        }
    }

    public function createHostelMessage()
    {

        if ($this->type_run != 'cron') {
            return;
        }

        $now = Carbon::now();
        $date_for_message = $now->clone()->addDays(3)->startOfDay();

        $workers = Worker::selectRaw($this->worker_table . '.*,' .
            $this->settlement_table . '.stop_hostel as stop_hostel')
            ->leftJoin($this->employment_table, $this->employment_table . '.worker_id', '=', $this->worker_table . '.id')
            ->leftJoin($this->settlement_table, $this->settlement_table . '.worker_id', '=', $this->worker_table . '.id')
            ->where([
                [$this->employment_table . '.status', '=', Employment::STATUS_ACTIVE],
                [$this->settlement_table . '.stop_hostel', '<=', $date_for_message],
                [$this->settlement_table . '.stop_hostel', '>=', $now]
            ])
            ->get();

        foreach ($workers as $worker) {

            $date_stop_hostel = Carbon::parse($worker->stop_hostel);

            /** @var Message $message */
            $message = Message::create([
                'message' => 'У меня заканчиваеться оплата хостела ' . $date_stop_hostel->format('d-m-Y') . '.',
                'user_id' => $worker->id,
                'type' => 'danger',
                'branch' => Settlement::MESSAGE_BRANCH
            ]);

            $users_send_to = $this->getBookers();

            foreach ($users_send_to as $users) {
                $message->usersMessages()->create([
                    'user_id' => $users->id,
                    'ttl' => $date_stop_hostel
                ]);
            }
        }

    }
}
