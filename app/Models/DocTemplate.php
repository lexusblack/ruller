<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class DocTemplate extends Model
{
    use HasFactory;
    protected $fillable = ['name','path','category'];

    const CATEGORIES = ['workers' => 'Работники', 'firms' => 'Фирмы'];

    protected $appends = ['link'];

    public function getLinkAttribute()
    {
        return Storage::disk('document_templates')->url($this->path);
    }
}
