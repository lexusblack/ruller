<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Message extends Model
{

    const TYPES = ['info','warning','danger'];

    protected $fillable = ['message','user_id','type', 'branch'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usersMessages()
    {
        return $this->hasMany(UsersMessages::class);
    }
}
