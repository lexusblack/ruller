<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{
    use HasFactory;
    protected $fillable = ['worker_id', 'start_hostel', 'stop_hostel', 'hostel_id', 'price'];

    const PAYMENT_PERIOD = 'endOfMonth';
    const COUNT_DAY_FOR_PAYMENT = '5';
    const MESSAGE_BRANCH = 'hostel';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hostel()
    {
        return $this->belongsTo('App\Models\Hostel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo('App\Models\Worker');
    }
}
