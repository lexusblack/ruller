<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UsersMessages extends Model
{

    protected $table = 'users_messages';
    protected $fillable = ['user_id','message_id','ttl','status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}

