<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Events\PageCreating;
use App\Events\PageUpdating;

class Page extends Model
{
    use HasFactory;

    protected $listeners = ['publishPost', 'unpublishPost'];

    protected $fillable = ['page_title', 'menu_title', 'parent', 'alias', 'uri', 'template', 'icon', 'sort', 'published', 'published_at', 'unpublished_at'];

    protected $casts = ['published' => 'boolean', 'published_at' => 'datetime', 'unpublished_at' => 'datetime'];

    protected $dispatchesEvents = [
        'creating' => PageCreating::class,
        'updating' => PageUpdating::class
    ];

}
