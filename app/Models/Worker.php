<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Worker extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'second_name',
        'passport',
        'phone',
        'email',
        'national',
        'pesel',
        'birthday',
        'start_passport',
        'stop_passport',
        'date_start_working',
        'date_stop_working',
        'date_start_visa',
        'date_stop_visa',
        'images',
        'sex',
        'conto',
        'bank'
    ];

    /**
     * @var array SEX_LIST
     */
    const SEX_LIST = ['man' => 'Man', 'woman' => 'Woman', 'unisex' => 'Unisex'];

    protected $casts = ['images' => 'array'];

    const MESSAGE_BRANCH = 'new_worker';

    /**
     * @return HasMany|null
     */
    public function activeJobs(): ?hasMany

    {
        return $this->hasMany(Employment::class)->where('status', '=', Employment::STATUS_ACTIVE);
    }

    /**
     * @return HasMany|null
     */
    public function finishedJobs(): ?hasMany
    {
        return $this->hasMany(Employment::class)->where('status', '=', Employment::STATUS_FINISH);
    }


    /**
     * @return BelongsTo|null
     */
    public function town(): ?belongsTo
    {
        return $this->belongsTo(Town::class);
    }


    /**
     * @return BelongsTo|null
     */
    public function profession(): ?belongsTo
    {
        return $this->belongsTo(Profession::class);
    }


    /**
     * @return HasMany|null
     */
    public function settlements(): ?hasMany
    {
        return $this->hasMany(Settlement::class);
    }

    /**
     * @return HasMany|null
     */
    public function statuss(): ?hasMany
    {
        return $this->hasMany(Status::class);
    }

    /**
     * @return HasMany|null
     */
    public function fines(): ?hasMany
    {
        return $this->hasMany(Fine::class);
    }

    /**
     * @return HasMany|null
     */
    public function employments(): ?hasMany
    {
        return $this->hasMany(Employment::class);
    }

    /**
     * @return HasMany|null
     */
    public function salaries(): ?hasMany
    {
        return $this->hasMany(Salary::class);
    }

    /**
     * @return HasMany|null
     */
    public function sponsors(): ?hasMany
    {
        return $this->hasMany(Sponsor::class);
    }

    /**
     * @return HasMany|null
     */
    public function documents(): ?hasMany
    {
        return $this->hasMany(Document::class);
    }
}
