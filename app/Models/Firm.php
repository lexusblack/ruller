<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Firm extends Model
{
    use HasFactory;
    protected $fillable = ['name','adress','nip','town_id','postal_code','contact_name','phone','start_work','stop_work'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function town () {
        return $this->belongsTo('App\Models\Town');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workers()
    {
        return $this->hasMany('App\Model\Worker');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hostels()
    {
        return $this->hasMany('App\Model\Hostel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()
    {
        return $this->hasMany('App\Model\Job');
    }

}
