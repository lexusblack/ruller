<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory;
    const SALARY_STATUS = ['add_hour' => 'Начислена', 'paid' => 'Перечисленна'];
    const STANDART_HOURS = 180;

    protected $fillable = ['worker_id', 'employment_id', 'date', 'hours', 'status_paid', 'salary'];

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//     */
//    public function jobs()
//    {
//        return $this->belongsTo('App\Models\Job');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo('App\Models\Worker');
    }

//    /**
//     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
//     */
//    public function job()
//    {
//        return $this->belongsTo('App\Models\Job');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employment()
    {
        return $this->belongsTo(Employment::class);
    }
}
