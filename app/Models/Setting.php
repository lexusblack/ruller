<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'nip', 'town_id', 'street', 'phone', 'postal_code', 'first_name', 'second_name', 'conto', 'conto23', 'date'];
}
