<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hostel extends Model
{
    use HasFactory;
    protected $fillable = ['name','town_id','address','firm_id','postal_code','start_work','owner_name','owner_phone',
        'admin_name','admin_phone','stop_work'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function town() {
        return $this->belongsTo('App\Models\Town');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firm() {
        return $this->belongsTo('App\Models\Firm');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workers()
    {
        return $this->hasMany('App\Models\Woker');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settlements()
    {
        return $this->hasMany('App\Models\Settlement');
    }
}
