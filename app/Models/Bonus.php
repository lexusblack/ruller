<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Bonus extends Model
{
    use HasFactory;
    protected $fillable = ['name','price','comment'];


    /**
     * @return HasMany|null
     */
    public function sponsors(): ?hasMany
    {
        return $this->hasMany(Sponsor::class);
    }
}
