<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    use HasFactory;
    protected $fillable = ['name','category_id','sex'];
    const SEX_LIST = ['man' => 'Man','woman' => 'Woman','unisex' => 'Unisex'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workers()
    {
        return $this->hasMany('App\Models\Worker');
    }

    public function jobs()
    {
        return $this->hasMany('App\Models\Job');
    }
}
