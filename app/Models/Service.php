<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    protected $fillable = ['name','price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuss()
    {
        return $this->hasMany('App\Models\Status');
    }
}
