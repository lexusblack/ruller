<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    protected $fillable = [
        'worker_id',
        'job_id',
        'start',
        'stop',
        'comment',
        'dismissal',
        'status'
    ];
    use HasFactory;

    const STATUS_ACTIVE = 'active';
    const STATUS_FINISH = 'finish';
    const MESSAGE_BRANCH = 'visa';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function worker()
    {
        return $this->belongsTo('App\Models\Worker');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo('App\Models\Job');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profession()
    {
        return $this->belongsTo('App\Models\Profession');
    }
}
