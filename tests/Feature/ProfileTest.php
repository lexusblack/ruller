<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    /**test
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/profile')
            ->assertSuccessful()
            ->assertSeeLiwevire('profile');


    }
}
