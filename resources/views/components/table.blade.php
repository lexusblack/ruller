<div class="card-body p-0">
    <table class="table table-striped projects">
        <thead>
        <tr>
            {{ $head }}
        </tr>
        </thead>

        <tbody>
        {{ $body }}
        </tbody>
    </table>
</div>
