<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title ?? 'Заголовок сайта' }}</title>
{{--    <title>{{ trans('words.login_title') ?? 'Заголовок сайта' }}</title>--}}

    <!-- Styles -->
    @foreach ($fonts as $font)
        <link href="{{ $font }}" rel="stylesheet">
    @endforeach
    @foreach ($css as $style)
        <link href="{{ asset($style) }}" rel="stylesheet">
    @endforeach

    @livewireStyles
</head>
<body class="hold-transition {{ $bodyClass ?? '' }}">

@yield('body')

<!-- Scripts -->
@foreach ($js as $script)
    <script src="{{ asset($script) }}"></script>
@endforeach
@livewireScripts
</body>
</html>


