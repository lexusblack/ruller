<div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-10">
                    <div class="card-title btn btn-flat margin">Workers list</div>
{{--                    @if(!$show_form)--}}
{{--                        <button wire:click="showForm()"--}}
{{--                                class="btn bg-olive btn-flat margin">Add new worker--}}
{{--                        </button>--}}
{{--                    @else--}}
{{--                        <button wire:click="hideForm()"--}}
{{--                                class="btn bg-olive btn-flat margin">Close form--}}
{{--                        </button>--}}
{{--                    @endif--}}
                </div>

                <div class="col-sm-2">
                    <div class="card-tools">

                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text"
                                   name="table_search"
                                   class="form-control float-right"
                                   placeholder="Search"
                                   wire:model="search"
                            >

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <x-table>
            <x-slot name="head">
                <tr>
                    <th>ID</th>
                    <th>First name</th>
                    <th>Second name</th>
                    <th>Start visa</th>
                    <th>Stop visa</th>
                    <th>Actions</th>
                </tr>
            </x-slot>

            <x-slot name="body">
                @foreach ($workers as $worker)
                    <tr wire:key="row-{{ $worker->id }}">
                        <td>{{ $worker->id }}</td>
                        <td>{{ $worker->first_name }}</td>
                        <td>{{ $worker->second_name }}</td>
                        <td>{{ $worker->date_start_visa }}</td>
                        <td>{{ $worker->date_stop_visa }}</td>
                        <td>
                            <button wire:click="editWorker({{ $worker->id }})"
                                    class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt">Edit</i>
                            </button>
{{--                            @if(!$show_info)--}}
{{--                                <button wire:click="showInfo({{ $worker->id }})"--}}
{{--                                        class="btn btn-info btn-sm"><i class="fas fa-folder">Info</i>--}}
{{--                                </button>--}}
{{--                            @else--}}
{{--                                <button wire:click="hideInfo()"--}}
{{--                                        class="btn btn-info btn-sm"><i class="fas fa-folder-minus">Hide--}}
{{--                                        Info</i>--}}
{{--                                </button>--}}
{{--                            @endif--}}

                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table>
    </div>

</div>


