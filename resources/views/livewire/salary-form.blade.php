<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.add_salary') }}</h3>
            </div>
            <div class="card-body">
                <div class="mb-2 row">
                    <div class="col-4">
                        <select wire:change="getActiveJob" wire:model.lazy="worker_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($workers as $worker)
                                <option value="{{ $worker->id }}"
                                        wire:key="worker_{{ $worker->id }}">{{ $worker->first_name }} {{ $worker->second_name }}</option>
                            @endforeach
                        </select>
                        @error('worker_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2">
                        <input wire:model.lazy="hours" type="number" class="form-control" placeholder="{{ trans('words.hours') }}">
                        @error('hours')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <input wire:model="date" type="text" class="form-control datepicker" autocomplete="off" readonly id="date"
                               placeholder="{{ trans('words.date') }}">
                        @error('date')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="col-3">
                        <select wire:model.lazy="status_paid" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($status_list as $value => $text)
                                <option value="{{ $value }}" wire:key="status_{{ $value }}">{{ $text }}</option>
                            @endforeach
                        </select>
                        @error('status_paid')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>

                </div>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" neme="id" wire:model="fine_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.first_name') }} {{ trans('words.second_name') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $worker['first_name'] }} {{ $worker['second_name'] }} {{ $worker['passport'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.date') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $date }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.hours') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $hours }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.status') }}:</dt>
                    <dd class="col-sm-6">
                        {{ \App\Models\Salary::SALARY_STATUS[$status_paid] }}
                    </dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_salary') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.date') }}</th>
                            <th>{{ trans('words.hours') }}</th>
                            <th>{{ trans('words.status') }}</th>
                            <th>{{ trans('words.sum') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($salaries as $salary)
                            <tr wire:key="row-{{ $salary->id }}">
                                <td>{{ $salary->id }}</td>
                                <td>{{ $salary->worker->first_name }} {{ $salary->worker->second_name }}</td>
                                <td>{{ $salary->date }}</td>
                                <td>{{ $salary->hours }}</td>
                                <td>
                                    <span class="@if (\App\Models\Salary::SALARY_STATUS[$salary->status_paid] != \App\Models\Salary::SALARY_STATUS['paid'])
                                            badge bg-warning
                                        @else
                                            badge bg-success
                                        @endif
                                        ">
                                        {{ \App\Models\Salary::SALARY_STATUS[$salary->status_paid] }}
                                    </span>

                                </td>
                                <td>{{ $salary->salary }}</td>
                                <td>{{ $salary->comment }}</td>
                                <td>
                                    <button wire:click="editSalary({{ $salary->id }})"
                                            class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"> {{ trans('words.edit') }} </i>
                                    </button>
                                    <button wire:click="showInfo({{ $salary->id }})" class="btn btn-info btn-sm"><i
                                            class="fas fa-folder"> {{ trans('words.info') }}</i></button>
                                    <button wire:click="deleteSalary({{ $salary->id }})" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash">{{ trans('words.delete') }}</i></button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $salaries->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>
<script>
    document.addEventListener('livewire:load', function () {
        window.addEventListener('init-date-field', () => {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'clip',
                onSelect: function(dateText, inst) {
                    if ($(this).prop('readonly')) {
                        $(this).prop('readonly', false);
                    }
                    $(this).val(dateText);
                    this.dispatchEvent(new InputEvent('input'));
                },
                onClose: function(dateText, inst) {
                    if (!$(this).prop('readonly')) {
                        $(this).prop('readonly', true);
                    }
                }
            });
        });
        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>

