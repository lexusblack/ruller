<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">

            <div class="card-header">
                <h3 class="card-title">{{ trans('words.document_add') }}</h3>
            </div>
            <div class="card-body">

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.name') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="name" type="text" class="form-control"
                               id="name"
                               placeholder="{{ trans('words.name') }}">
                        @error('name')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="category" class="col-sm-2 col-form-label">{{ trans('words.document_category') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="category" class="form-control  @error('category')is-invalid @enderror">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach(\App\Models\DocTemplate::CATEGORIES as $key => $label)
                                <option value="{{ $key }}"
                                        wire:key="category_{{ $key }}">{{ $label }}</option>
                            @endforeach
                        </select>
                        @error('category')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                @if(empty($template_id))
                <div class="form-group row">

                    <label class="col-2 col-sm-2 col-form-label">{{ trans('words.template_document') }}</label>
                    <div class="custom-file col-2 col-sm-10">

                        <input wire:model="docx" type="file" class="custom-file-input"
                               id="docx">
                        @error('docx')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                        <label class="custom-file-label" for="image">{{ trans('words.choise_file') }}</label>
                    </div>
                    <div
                        class="progress-bar progress-bar-striped progress-bar-animated col-sm-12"
                        wire:loading wire:target="images">{{ trans('words.uploading') }}...
                    </div>

                </div>
                @endif

                <div class="card-footer">
                    <div class="row">
                        <div class="col-12">

                            <input type="hidden" name="id" wire:model="bonus_id">

                            <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>

                            @if(!$show_form)


                                <button wire:click="showForm()"

                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_template') }}
                                </button>
                            @else

                                <button wire:click="hideForm()"

                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.name') }}</th>
                            <th>{{ trans('words.link') }}</th>
                            <th>{{ trans('words.document_category') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">

                        @foreach ($templates as $template)


                            <tr wire:key="row-{{ $template->id }}">
                                <td>{{ $template->id }}</td>
                                <td>{{ $template->name }}</td>
                                <td><a class="btn btn-primary btn-sm" href="{{ $template->link }}">{{ trans('words.link') }}</a></td>
                                <td>{{ \App\Models\DocTemplate::CATEGORIES[$template->category] }}</td>
                                <td>
                                    <button wire:click="editTemplate({{ $template->id }})"
                                            class="btn btn-primary btn-sm"><i
                                            class="fas fa-pencil-alt">{{ trans('words.edit') }}</i></button>

                                    <button wire:click="deleteTemplate({{ $template->id }})"
                                            class="btn btn-danger btn-sm"><i
                                            class="fas fa-trash">{{ trans('words.delete') }}</i></button>
                                </td>
                            </tr>
                        @endforeach

                    </x-slot>
                </x-table>
            </div>

        </div>
    </div>
</div>


<script>
    document.addEventListener('livewire:load', function () {

        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>

