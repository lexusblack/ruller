<livewire:head/>


<livewire:menu/>


<div class="sidebar-custom">

    @auth
        <livewire:logout/>
    @endauth
</div>

</aside>


<div class="content-wrapper">

    @include('livewire.bread', [
    'title' => $title ?? 'test'
    ])


    <section class="content">

        <livewire:document-list />
    </section>

</div>


<livewire:footer/>
