@inject('messages', 'App\Services\UserMessages')
<div wire:poll:120s>
    @foreach($messages->messages(Auth::user()->id)['items'] as $item)
        <a wire:click.prevent="$emit('setReaded', {{ $item['id'] }})" href="{{ route('worker') }}?user={{ $item['worker_id'] }}" class="dropdown-item message__to-worker-link">
            <div class="media">
                <img src="{{ \Illuminate\Support\Facades\Storage::disk('avatars')->url('unnamed.png') }}" alt="{{ $item['first_name'] }}" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                    <h3 class="dropdown-item-title">
                        {{ $item['first_name'] }} {{ $item['second_name'] }}
                        <span class="float-right text-sm text-{{ $item['type'] }}">
                        <i class="fas fa-star"></i>
                    </span>
                    </h3>
                    <p class="text-sm">{{ $item['message'] ?? '' }}</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> {{ \Carbon\Carbon::parse($item['created_at'])->diffForHumans() }}</p>
                </div>
            </div>
        </a>
        <div class="dropdown-divider"></div>
    @endforeach

    @if ($messages->messages(Auth::user()->id)['total'] > 5)
        <a href="{{ route('message-page') }}" class="dropdown-item dropdown-footer">{{ trans('words.see_all_message') }}</a>
    @endif
</div>
