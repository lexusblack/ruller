<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.hostel_add') }}</h3>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.town') }}</label>
                    <div class="col-sm-10">
                        <select wire:model="town_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($towns as $town)
                                <option value="{{ $town->id }}"
                                        wire:key="town_{{ $town->id }}">{{ $town->name }}</option>
                            @endforeach
                        </select>
                        @error('town_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.name') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="name" type="text" class="form-control" placeholder="{{ trans('words.name') }}">
                        @error('name')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.address') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="address" type="text" class="form-control" placeholder="{{ trans('words.address') }}">
                        @error('address')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.postal_code') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="postal_code" type="text" class="form-control" placeholder="{{ trans('words.postal_code') }}">
                        @error('postal_code')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.firm') }}</label>
                    <div class="col-sm-10">
                        <select wire:model="firm_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($firms as $firm)
                                <option value="{{ $firm->id }}"
                                        wire:key="firm_{{ $firm->id }}">{{ $firm->name }}</option>
                            @endforeach
                        </select>
                        @error('firm_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.start_work') }}</label>
                    <div class="col-sm-10">
                        <input wire:model="start_work" type="text" class="form-control datepicker" autocomplete="off" readonly
                               placeholder="{{ trans('words.start_work') }}">
                        @error('start_work')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.owner_name') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="owner_name" type="text" class="form-control" placeholder="{{ trans('words.owner_name') }}">
                        @error('owner_name')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.phone') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="owner_phone" type="text" class="form-control" placeholder="{{ trans('words.phone') }}">
                        @error('owner_phone')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.admin_name') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="admin_name" type="text" class="form-control" placeholder="{{ trans('words.admin_name') }}">
                        @error('admin_name')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.phone') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="admin_phone" type="text" class="form-control" placeholder="{{ trans('words.phone') }}">
                        @error('admin_phone')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.stop_work') }}</label>
                    <div class="col-sm-10">
                        <input wire:model="stop_work" type="text" class="form-control datepicker" autocomplete="off" readonly
                               placeholder="{{ trans('words.stop_work') }}">
                        @error('stop_work')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="id" wire:model="hostel_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.town') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $town['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.name') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $name }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.address') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $address }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.postal_code') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $postal_code }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.firm') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $firm['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.date_start_working') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $start_work }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.date_stop_working') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $stop_work }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.owner_name') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $owner_name }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.phone') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $owner_phone }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.admin_name') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $admin_name }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.phone') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $admin_phone }}
                    </dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_hostel') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch"
                                    >
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.name_hostel') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($hostels as $hostel)
                            <tr wire:key="row-{{ $hostel->id }}">
                                <td>{{ $hostel->id }}</td>
                                <td>{{ $hostel->name }}</td>
                                <td>
                                    <button wire:click="editHostel({{ $hostel->id }})"
                                            class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"> {{ trans('words.edit') }} </i>
                                    </button>
                                    <button wire:click="showInfo({{ $hostel->id }})"
                                            class="btn btn-info btn-sm"><i class="fas fa-folder"> {{ trans('words.info') }}</i>
                                    </button>
                                    <button wire:click="deleteHostel({{ $hostel->id }})"
                                            class="btn btn-danger btn-sm"><i class="fas fa-trash">{{ trans('words.delete') }}</i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
            </div>

        </div>
    </div>
</div>


<script>
    document.addEventListener('livewire:load', function () {
        window.addEventListener('init-date-field', () => {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'clip',
                onSelect: function(dateText, inst) {
                    if ($(this).prop('readonly')) {
                        $(this).prop('readonly', false);
                    }
                    $(this).val(dateText);
                    this.dispatchEvent(new InputEvent('input'));
                    // $(this).prop('readonly', true);
                    //console.log('OnSelect', this, dateText, inst);
                },
                onClose: function(dateText, inst) {
                    if (!$(this).prop('readonly')) {
                        $(this).prop('readonly', true);
                    }

                    //console.log('OnClose', dateText, inst);
                }
            });
        });
        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>

