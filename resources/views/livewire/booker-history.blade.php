    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch"
                                    >
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.dismissal') }}</th>
                            <th>{{ trans('words.start_work') }}</th>
                            <th>{{ trans('words.stop_work') }}</th>
                            <th>{{ trans('words.job') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($histories as $history)
                            <tr wire:key="row-{{ $history->id }}"
                                class="table_tr_succes">
                                <td>{{ $history->id }}</td>
                                <td>
                                    <a href="{{ route('worker') . '?user=' . $history->worker_id }}"
                                       class="btn btn-primary btn-sm">
                                        {{ $history->first_name }} {{ $history->second_name }}
                                    </a>
                                </td>
                                <td>{{ $history->dismissal }}</td>
                                <td>{{ $history->start }}</td>
                                <td>{{ $history->stop }}</td>

                                <td>
                                    <a href="{{ route('job') . '?work=' . $history->job_id }}"
                                       class="btn btn-primary btn-sm">
                                        {{ $history->description }}
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
            </div>
            {{ $histories->links('livewire.pagination') }}
        </div>
    </div>
<style>
    .table_tr_dander {
        background-color: rgba(255, 0, 0, .2) !important;
    }

    .table_tr_succes {
        background-color: rgba(2, 182, 8, .2) !important;
        color: #000;
    }
</style>

