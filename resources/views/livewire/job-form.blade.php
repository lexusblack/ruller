<div>
    @if($show_form)

        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.job_add') }}</h3>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.profession') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="profession_id" class="form-control  @error('profession_id')is-invalid @enderror">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($professions as $profession)
                                <option value="{{ $profession->id }}"
                                        wire:key="profession_{{ $profession->id }}">{{ $profession->name }}</option>
                            @endforeach
                        </select>
                        @error('profession_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address" class="col-sm-2 col-form-label">{{ trans('words.address') }}</label>
                    <div class="col-sm-10">
                        <input wire:model="address" type="text" class="form-control  @error('address')is-invalid @enderror" placeholder="{{ trans('words.address') }}">
                        @error('address')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="town_id" class="col-sm-2 col-form-label">{{ trans('words.town') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="town_id" class="form-control @error('town_id')is-invalid @enderror">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($towns as $town)
                                <option value="{{ $town->id }}"
                                        wire:key="town_{{ $town->id }}">{{ $town->name }}</option>
                            @endforeach
                        </select>
                        @error('town_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="firm_id" class="col-sm-2 col-form-label">{{ trans('words.firm') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="firm_id" class="form-control @error('firm_id')is-invalid @enderror">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($firms as $firm)
                                <option value="{{ $firm->id }}"
                                        wire:key="firm_{{ $firm->id }}">{{ $firm->name }}</option>
                            @endforeach
                        </select>
                        @error('firm_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.count_place') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="place" type="text" class="form-control" placeholder="{{ trans('words.count_place') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.money_per_hour') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="money" type="number" min="0" step="0.01" class="form-control @error('money')is-invalid @enderror"
                               placeholder="{{ trans('words.money_per_hour') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.money_plus') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="money_plus" type="number" min="0" step="0.01" class="form-control"
                               placeholder="{{ trans('words.money_plus') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.description') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="description" type="text" class="form-control" placeholder="{{ trans('words.description') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.duties') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="duties" type="text" class="form-control" placeholder="{{ trans('words.duties') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.time_work') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="time_work" type="text" class="form-control" placeholder="{{ trans('words.time_work') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.transport') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="transport" type="text" class="form-control" placeholder="{{ trans('words.transport') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.living') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="living" type="text" class="form-control" placeholder="{{ trans('words.living') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.demand') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy="demand" type="text" class="form-control" placeholder="{{ trans('words.demand') }}">
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" neme="id" wire:model="job_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.profession') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $profession['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.address') }}:</dt>
                    <dd class="col-sm-6">{{ $address }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.town') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $town['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.firm') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $firm['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.count_place') }}:</dt>
                    <dd class="col-sm-6">{{ $place }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.money_per_hour') }}:</dt>
                    <dd class="col-sm-6">{{ $money }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.money_plus') }}:</dt>
                    <dd class="col-sm-6">{{ $money_plus }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.description') }}:</dt>
                    <dd class="col-sm-6">{{ $description }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.duties') }}:</dt>
                    <dd class="col-sm-6">{{ $duties }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.time_work') }}:</dt>
                    <dd class="col-sm-6">{{ $time_work }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.living') }}:</dt>
                    <dd class="col-sm-6">{{ $living }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.transport') }}:</dt>
                    <dd class="col-sm-6">{{ $transport }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.demand') }}:</dt>
                    <dd class="col-sm-6">{{ $demand }}</dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_job') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.profession') }}</th>
                            <th>{{ trans('words.address') }}</th>
                            <th>{{ trans('words.town') }}</th>
                            <th>{{ trans('words.firm') }}</th>
                            <th>{{ trans('words.count_place') }}</th>
                            <th>{{ trans('words.money_per_hour') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($jobs as $job)
                            <tr wire:key="row-{{ $job->id }}">
                                <td>{{ $job->id }}</td>
                                <td>{{ $job->profession->name }}</td>
                                <td>{{ $job->address }}</td>
                                <td>{{ $job->town->name }}</td>
                                <td>{{ $job->firm->name }}</td>
                                <td>{{ $job->place }}</td>
                                <td>{{ $job->money }}</td>
                                <td>
                                    <button wire:click="editJob({{ $job->id }})"
                                            class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"> {{ trans('words.edit') }} </i>
                                    </button>

                                    <button wire:click="showInfo({{ $job->id }})"
                                            class="btn btn-info btn-sm"><i class="fas fa-folder"> {{ trans('words.info') }}</i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $jobs->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>

<script>
    document.addEventListener('livewire:load', function () {

        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>
