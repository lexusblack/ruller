<section class="content">
    <div class="container-fluid">

        @if ($show_form)
            <div class="row">

                <div class="col-md-12">
                    <form wire:submit.prevent="save" class="form-horizontal">
                        <div class="card">

                            <div class="card-header">

                            </div>

                            <div class="card-header p-2">
                                <ul class="nav nav-pills">

                                    <button wire:click.prevent="hideForm()"
                                            class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                    </button>

                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">

                                    <div class="active tab-pane" id="passport">

                                        <div class="form-group row">
                                            <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.first_name') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="first_name" type="text" class="form-control"
                                                       id="first_name"
                                                       placeholder="{{ trans('words.first_name') }}">
                                                @error('first_name')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="second_name" class="col-sm-2 col-form-label">{{ trans('words.second_name') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="second_name" type="text" class="form-control"
                                                       id="second_name"
                                                       placeholder="{{ trans('words.second_name') }}">
                                                @error('second_name')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="passport" class="col-sm-2 col-form-label">{{ trans('words.passport') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="passport" type="text" class="form-control"
                                                       id="passport"
                                                       placeholder="{{ trans('words.passport') }}">
                                                @error('passport')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="national"
                                                   class="col-sm-2 col-form-label">{{ trans('words.national') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="national" type="text" class="form-control"
                                                       id="national"
                                                       placeholder="{{ trans('words.national') }}">
                                                @error('national')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="birthday" class="col-sm-2 col-form-label">{{ trans('words.birthday') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="birthday" type="text" class="form-control datepicker"
                                                       autocomplete="off" readonly
                                                       id="birthday" placeholder="{{ trans('words.birthday') }}">
                                                @error('birthday')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="start_passport" class="col-sm-2 col-form-label">{{ trans('words.start_passport') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="start_passport" type="text"
                                                       class="form-control datepicker" autocomplete="off" readonly
                                                       id="start_passport"
                                                       placeholder="{{ trans('words.start_passport') }}">
                                                @error('start_passport')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="stop_passport" class="col-sm-2 col-form-label">{{ trans('words.stop_passport') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="stop_passport" type="text"
                                                       class="form-control datepicker" autocomplete="off" readonly
                                                       id="stop_passport"
                                                       placeholder="{{ trans('words.stop_passport') }}">
                                                @error('stop_passport')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="sex" class="col-sm-2 col-form-label">{{ trans('words.sex') }}</label>
                                            <div class="col-sm-10">
                                                <select wire:model.lazy="sex" class="form-control">
                                                    <option value="">{{ trans('words.choise') }}</option>
                                                    @foreach($sex_list as $value => $text)
                                                        <option value="{{ $value }}"
                                                                wire:key="worker_{{ $value }}">{{ $text }}</option>
                                                    @endforeach
                                                </select>
                                                @error('sex')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="date_start_visa" class="col-sm-2 col-form-label">{{ trans('words.start_visa') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="date_start_visa" type="text"
                                                       class="form-control datepicker" autocomplete="off" readonly
                                                       id="date_start_visa"
                                                       placeholder="{{ trans('words.start_visa') }}">
                                                @error('date_start_visa')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="date_stop_visa" class="col-sm-2 col-form-label">{{ trans('words.stop_visa') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="date_stop_visa" type="text"
                                                       class="form-control datepicker" autocomplete="off" readonly
                                                       id="date_stop_visa"
                                                       placeholder="{{ trans('words.stop_visa') }}">
                                                @error('date_stop_visa')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="phone" class="col-sm-2 col-form-label">{{ trans('words.phone') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="phone" type="text" class="form-control"
                                                       id="phone"
                                                       placeholder="{{ trans('words.phone') }}">
                                                @error('phone')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-sm-2 col-form-label">{{ trans('words.email') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="email" type="email" class="form-control"
                                                       id="email"
                                                       placeholder="{{ trans('words.email') }}">
                                                @error('email')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="pesel" class="col-sm-2 col-form-label">{{ trans('words.pesel') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="pesel" type="text" class="form-control"
                                                       id="pesel"
                                                       placeholder="{{ trans('words.pesel') }}">
                                                @error('pesel')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="conto" class="col-sm-2 col-form-label">{{ trans('words.conto') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="conto" type="text" class="form-control"
                                                       id="conto"
                                                       placeholder="{{ trans('words.conto') }}">
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="bank" class="col-sm-2 col-form-label">{{ trans('words.bank') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model.lazy="bank" type="text" class="form-control"
                                                       id="bank"
                                                       placeholder="{{ trans('words.bank') }}">
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="date_start_working" class="col-sm-2 col-form-label">{{ trans('words.start_working') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="date_start_working" type="text"
                                                       class="form-control datepicker" autocomplete="off" readonly
                                                       id="date_start_working"
                                                       placeholder="{{ trans('words.start_working') }}">
                                                @error('date_start_working')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <label for="date_stop_working" class="col-sm-2 col-form-label">{{ trans('words.stop_working') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="date_stop_working" type="text"
                                                       class="form-control datepicker" autocomplete="off" readonly
                                                       id="date_stop_working"
                                                       placeholder="{{ trans('words.stop_working') }}">
                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <label class="col-2 col-sm-2 col-form-label">{{ trans('words.photo') }}</label>
                                            <div class="custom-file col-2 col-sm-10">

                                                <input wire:model="images" type="file" class="custom-file-input"
                                                       id="images" multiple>
                                                @error('images')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                                <label class="custom-file-label" for="image">{{ trans('words.choise') }}</label>
                                            </div>
                                            <div
                                                class="progress-bar progress-bar-striped progress-bar-animated col-sm-12"
                                                wire:loading wire:target="images">Uploading...
                                            </div>

                                        </div>

                                    </div>


                                    <input type="hidden" name="id" wire:model="work_id">
                                    <button type="submit" class="btn btn-danger">{{ trans('words.send') }}</button>

                                </div>

                            </div>
                        </div>

                    </form>
                </div>


            </div>
        @endif
        @if($show_info)
            <div class="card card-lg active">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">

                        <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                                data-toggle="tab">{{ trans('words.close') }}</a>
                        </li>
                    </ul>

                </div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-6 text-sm-right">{{ trans('words.first_name') }} {{ trans('words.second_name') }}:</dt>
                        <dd class="col-sm-6">{{ $first_name }}{{ $second_name }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.email') }}:</dt>
                        <dd class="col-sm-6">{{ $email }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.phone') }}:</dt>
                        <dd class="col-sm-6">{{ $phone }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.passport') }}:</dt>
                        <dd class="col-sm-6">{{ $passport }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.national') }}:</dt>
                        <dd class="col-sm-6">{{ $national }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.pesel') }}:</dt>
                        <dd class="col-sm-6">{{ $pesel }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.birthday') }}:</dt>
                        <dd class="col-sm-6">{{ $birthday }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.start_passport') }}:</dt>
                        <dd class="col-sm-6">{{ $start_passport }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.stop_passport') }}:</dt>
                        <dd class="col-sm-6">{{ $stop_passport }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.start_working') }}:</dt>
                        <dd class="col-sm-6">{{ $date_start_working }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.stop_working') }}:</dt>
                        <dd class="col-sm-6">{{ $date_stop_working }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.start_visa') }}:</dt>
                        <dd class="col-sm-6">{{ $date_start_visa }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.stop_visa') }}:</dt>
                        <dd class="col-sm-6">{{ $date_stop_visa }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.sex') }}:</dt>
                        <dd class="col-sm-6">{{ $sex }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.bank') }}:</dt>
                        <dd class="col-sm-6">{{ $bank }}</dd>

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.conto') }}:</dt>
                        <dd class="col-sm-6">{{ $conto }}</dd>

                        @foreach($images as $image)
                            <dt class="col-sm-6 text-sm-right">File:</dt>
                            <dd class="col-sm-6">
                                <a target="_blank"
                                   href="{{ \Illuminate\Support\Facades\Storage::disk('avatars')->url($image) }}"
                                   class="btn btn-primary btn-sm">Open</a>
                            </dd>
                        @endforeach

                        <dt class="col-sm-6 text-sm-right">{{ trans('words.photo') }}:</dt>
                        <dd class="col-sm-2">
                                <select wire:model.lazy="document" class="form-control  ">
                                    <option value="">{{ trans('words.choise') }}</option>
                                    @foreach($documents as $document)
                                        <option value="{{ $document->id }}"
                                                wire:key="document_{{ $document->id }}">{{ $document->name }}</option>
                                    @endforeach
                                </select>

                        </dd>
                        <dd class="col-sm-3"><a wire:click.prevent="getReadyDocument" href="#" class="btn btn-primary btn-sm">{{ trans('words.download') }}</a></dd>

                    </dl>

                </div>

            </div>

        @endif

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                                @if(!$show_form)
                                    <button wire:click="showForm()"
                                            class="btn bg-olive btn-flat margin">{{ trans('words.add_new_worker') }}
                                    </button>
                                @else
                                    <button wire:click="hideForm()"
                                            class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                    </button>
                                @endif
                            </div>

                            <div class="col-sm-2">
                                <div class="card-tools">

                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text"
                                               name="table_search"
                                               class="form-control float-right"
                                               placeholder="{{ trans('words.search') }}"
                                               wire:model="search"
                                               wire:keydown.escape="resetSearch"
                                               wire:keydown.tab="resetSearch"
                                        >

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <x-table>
                        <x-slot name="head">
                            <tr>
                                <th>ID</th>
                                <th>{{ trans('words.first_name') }}</th>
                                <th>{{ trans('words.second_name') }}</th>
                                <th>{{ trans('words.start_visa') }}</th>
                                <th>{{ trans('words.stop_visa') }}</th>
                                <th>{{ trans('words.action') }}</th>
                            </tr>
                        </x-slot>

                        <x-slot name="body">
                            @foreach ($workers as $worker)
                                <tr wire:key="row-{{ $worker->id }}">
                                    <td>{{ $worker->id }}</td>
                                    <td>{{ $worker->first_name }}</td>
                                    <td>{{ $worker->second_name }}</td>
                                    <td>{{ $worker->date_start_visa }}</td>
                                    <td>{{ $worker->date_stop_visa }}</td>
                                    <td>
                                        <button wire:click="editWorker({{ $worker->id }})"
                                                class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt">{{ trans('words.edit') }}</i>
                                        </button>
                                        <button wire:click="showInfo({{ $worker->id }})"
                                                class="btn btn-info btn-sm"><i class="fas fa-folder">{{ trans('words.info') }}</i>
                                        </button>

                                    </td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table>

                    {{ $workers->links('livewire.pagination') }}

                </div>

            </div>
        </div>
    </div>
</section>

<script>

    document.addEventListener('DOMContentLoaded', function () {
        window.livewire.on('urlChange', (url) => {
            history.pushState(null, null, url);
        });
    });

    document.addEventListener('load', function() {});

    document.addEventListener('livewire:load', function () {
        window.addEventListener('init-date-field', () => {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'clip',
                onSelect: function (dateText, inst) {
                    if ($(this).prop('readonly')) {
                        $(this).prop('readonly', false);
                    }
                    $(this).val(dateText);
                    this.dispatchEvent(new InputEvent('input'));
                },
                onClose: function (dateText, inst) {
                    if (!$(this).prop('readonly')) {
                        $(this).prop('readonly', true);
                    }

                }
            })

            ;
        });

        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });

</script>
