@inject('messages', 'App\Services\UserMessages')
<span wire:poll:120s class="badge badge-danger navbar-badge">{{ $messages->count(Auth::user()->id) }}</span>
