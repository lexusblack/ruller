<div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch"
                                    >
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.messages') }}</th>
                            <th>{{ trans('words.status') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($messages as $message)
                            <tr wire:key="row-{{ $message->id }}"
                                class="{{ $message->status == 0 ? 'table_tr_dander' : 'table_tr_succes' }}">
                                <td>{{ $message->id }}</td>
                                <td>
                                    <a href="{{ route('worker') . '?user=' . $message->worker_id }}"
                                       class="btn btn-primary btn-sm">
                                        {{ $message->first_name }} {{ $message->second_name }}
                                    </a></td>
                                <td>{{ $message->message }}</td>
                                <td>{{ $message->status == 0 ? trans('words.unread') : trans('words.read')}}</td>
                                <td>
                                    @if ($message->status == 0)
                                        <button wire:click="read({{ $message->id }})"
                                                class="btn btn-primary btn-sm"><i class="fas fa-check-circle"> {{ trans('words.see') }} </i>
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
            </div>
            {{ $messages->links('livewire.pagination') }}
        </div>
    </div>
</div>

<style>
    .table_tr_dander {
        background-color: rgba(255, 0, 0, .2) !important;
    }

    .table_tr_succes {
        background-color: rgba(2, 182, 8, .2) !important;
        color: #000;
    }
</style>
