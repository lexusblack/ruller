<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.sponsor_add') }}</h3>
            </div>
            <div class="card-body">
                <div class="mb-2 row">
                    <div class="col-2">
                        <select wire:change="getActiveJob" wire:model.lazy="worker_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($workers as $worker)
                                <option value="{{ $worker->id }}"
                                        wire:key="worker_{{ $worker->id }}">{{ $worker->first_name }} {{ $worker->second_name }}</option>
                            @endforeach
                        </select>
                        @error('worker_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2">
                        <select wire:model.lazy="bonus_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($bonuses as $bonus)
                                <option value="{{ $bonus->id }}"
                                        wire:key="bonus_{{ $bonus->id }}">{{ $bonus->name }}</option>
                            @endforeach
                        </select>
                        @error('bonus_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2">
                        <input wire:model="date" type="text" class="form-control datepicker" autocomplete="off" readonly
                               id="date"
                               placeholder="{{ trans('words.date') }}">
                        @error('date')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-6">
                        <input wire:model.lazy="comment" type="text" class="form-control" placeholder="{{ trans('words.description') }}">
                    </div>
                    @if($price != $bonusPrice)
                        <div class="col-2">
                            <div class="custom-control custom-switch">
                                <input wire:model="up" type="checkbox" value="{{ $up }}"
                                       class="custom-control-input" id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">{{ trans('words.update_sponsor_price') }}</label>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="id" wire:model="sponsor_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.worker') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $worker['first_name'] }} {{ $worker['second_name'] }} {{ $worker['passport'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.bonus') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $bonus['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.date') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $date }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.price') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $price }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.price_now') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $bonusPrice }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.description') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $comment }}
                    </dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_sponsor') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.bonus') }}</th>
                            <th>{{ trans('words.price') }}</th>
                            <th>{{ trans('words.date') }}</th>
                            <th>{{ trans('words.description') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($sponsors as $sponsor)
                            <tr wire:key="row-{{ $sponsor->id }}">
                                <td>{{ $sponsor->id }}</td>
                                <td>{{ $sponsor->worker->first_name }} {{ $sponsor->worker->second_name }}</td>
                                <td>{{ $sponsor->bonus->name }}</td>
                                <td>{{ $sponsor->price }}</td>
                                <td>{{ $sponsor->date }}</td>
                                <td>{{ $sponsor->comment }}</td>
                                <td>
                                    <button wire:click="editSponsor({{ $sponsor->id }})" class="btn btn-primary btn-sm">
                                        <i class="fas fa-pencil-alt">{{ trans('words.edit') }}</i></button>
                                    <button wire:click="showInfo({{ $sponsor->id }})" class="btn btn-info btn-sm"><i
                                            class="fas fa-folder">{{ trans('words.info') }}</i></button>
                                    <button wire:click="deleteSponsor({{ $sponsor->id }})"
                                            class="btn btn-danger btn-sm"><i class="fas fa-trash">{{ trans('words.delete') }}</i></button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $sponsors->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>
<script>
    document.addEventListener('livewire:load', function () {
        window.addEventListener('init-date-field', () => {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'clip',
                onSelect: function (dateText, inst) {
                    if ($(this).prop('readonly')) {
                        $(this).prop('readonly', false);
                    }
                    $(this).val(dateText);
                    this.dispatchEvent(new InputEvent('input'));
                },
                onClose: function (dateText, inst) {
                    if (!$(this).prop('readonly')) {
                        $(this).prop('readonly', true);
                    }

                }
            });
        });
        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>

