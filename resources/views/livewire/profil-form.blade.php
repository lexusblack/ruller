<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">


                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                 @if ($prev != null)
                                    src="{{ $prev }}"
                                 @else
                                    src="
{{--{{Storage::disk('avatars')->url( $photo) }}--}}
                                     {{ $photo == null ? Storage::disk('avatars')->url('unnamed.png') : Storage::disk('avatars')->url($photo) }}"
                                 @endif
                                 alt="User profile picture">
                        </div>
                        <h3 class="profile-username text-center">{{ $name }}</h3>


                        <p class="text-muted text-center">{{ $email }}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>{{ $phone }}</b>
                            </li>
                            <li class="list-group-item">
                                <b>{{ $street }}</b>
                            </li>
                            <li class="list-group-item">
                                <b>{{ $town }}</b>
                            </li>
                            <li class="list-group-item">
                                <b>{{ $profession }}</b>
                            </li>
                            <li class="list-group-item">
                                <b>{{ $role->alias }}</b>
                            </li>
                        </ul>


                    </div>

                </div>

            </div>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#settings"
                                                    data-toggle="tab">{{ trans('words.settings') }}</a>
                            </li>

                            <div wire:loading wire:target="photo">{{ trans('words.uploading') }}...</div>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane" id="activity">

                            </div>

                            <div class="tab-pane" id="timeline">

                            </div>


                            <div class="tab-pane active" id="settings">
                                <form wire:submit.prevent="save" class="form-horizontal">
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">{{ trans('words.name') }}</label>
                                        <div class="col-sm-10">
                                            <input wire:model="name" type="text" class="form-control" id="inputName"
                                                   placeholder="name">
                                            @error('name')
                                            <span class="bg-red">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-2 col-form-label">{{ trans('words.name') }}</label>
                                        <div class="col-sm-10">
                                            <input wire:model="email" type="email" class="form-control" id="inputEmail"
                                                   placeholder="{{ trans('words.name') }}">
                                            @error('email')
                                            <span class="bg-red">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-2 col-form-label">{{ trans('words.phone') }}</label>
                                        <div class="col-sm-10">
                                            <input wire:model="phone" type="text" class="form-control" id="phone"
                                                   placeholder="{{ trans('words.phone') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="street"
                                               class="col-sm-2 col-form-label">{{ trans('words.street') }}</label>
                                        <div class="col-sm-10">
                                            <input wire:model="street" type="text" class="form-control" id="street"
                                                   placeholder="{{ trans('words.street') }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="towm" class="col-sm-2 col-form-label">{{ trans('words.town') }}</label>
                                        <div class="col-sm-10">
                                            <input wire:model="town" type="text" class="form-control" id="Town"
                                                   placeholder="{{ trans('words.town') }}">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label for="profession" class="col-sm-2 col-form-label">{{ trans('words.profession') }}</label>
                                        <div class="col-sm-10">
                                            <input wire:model="profession" type="text" class="form-control"
                                                   id="Profession"
                                                   placeholder="{{ trans('words.profession') }}">
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-sm-2 col-form-label">{{ trans('words.photo') }}</label>
                                        <div class="custom-file col-2 col-sm-10">

                                            <input wire:model="photo" type="file" class="custom-file-input" id="photo">
                                            @error('photo')
                                            <span class="bg-red">{{ $message }}</span>
                                            @enderror
                                            <label class="custom-file-label" for="photo">{{ trans('words.choise_file') }}</label>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger">{{ trans('words.send') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
</section>
