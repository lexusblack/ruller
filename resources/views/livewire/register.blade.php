<div>
    <div class="register-box">
        <div class="card card-outline card-primary">

            <div class="card-header text-center">
                <p class="h1">{{ trans('words.logo_title') }} - {{ trans('words.desc_reg') }}</p>
            </div>
            <div class="card-body">
                <p class="login-box-msg">
                    @error('email')
                <p class="login-box-msg text-red"> {{ $message }}</p>@enderror
                @error('name')<p class="login-box-msg text-red"> {{ $message }}</p>@enderror
                @error('passwordConfirmation')<p class="login-box-msg text-red"> {{ $message }}</p>@enderror
                </p>

                <form wire:submit.prevent="register" action="#" method="post">
                    <div class="input-group mb-3">
                        <input wire:model.lazy="name" type="text" class="form-control" placeholder="{{ trans('words.name') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input wire:model.lazy="email" type="email" class="form-control" placeholder="{{ trans('words.email') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input wire:model.lazy="password" type="password" class="form-control" placeholder="{{ trans('words.password') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input wire:model.lazy="passwordConfirmation" type="password" class="form-control"
                               placeholder="{{ trans('words.retype_password') }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">


                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block">{{ trans('words.register') }}</button>
                        </div>

                    </div>
                </form>


                <a href="/login" class="text-center">{{ trans('words.already_register') }}</a>
            </div>

        </div>
    </div>
</div>
