<div class="sidebar">

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img class="img-circle elevation-2" src=" {{ $avatar == null ? Storage::disk('avatars')->url('unnamed.png') : Storage::disk('avatars')->url($avatar) }}
{{--{{Storage::disk('avatars')->url($avatar) }}--}}
                " alt="User Image">
        </div>
        <div class="info">
            <a href="{{ route('profile') }}" class="d-block">{{ $name }}</a>
        </div>
    </div>


    <div class="form-inline">
        {{--<div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>--}}
    </div>


    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @foreach ($items as $item)
                <li class="nav-item">

                    <a href="{{ $item['uri'] }}"
                       class="nav-link {{ Route::current()->getName() == $item['alias'] ? 'active' : ''}}">
                        <i class="nav-icon {{ $item['icon'] }}"></i>
                        <p>
                            {{ $item['menu_title'] ?? $item['page_title']}}
                            @if (count($item['children']) > 0 )
                                <i class="right fas fa-angle-left"></i>
                            @endif
                        </p>
                    </a>
                    @if (count($item['children']) > 0)
                        <ul class="nav nav-treeview">
                            @foreach($item['children'] as $child)
                                <li class="nav-item">
                                    <a href="{{ $child['uri'] }}"
                                       class="nav-link {{ Route::current()->getName() == $child['alias'] ? 'active' : ''}}">
                                        <i class="{{ $child['icon'] }} nav-icon"></i>
                                        <p>{{ $child['title'] }}</p>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>

</div>
