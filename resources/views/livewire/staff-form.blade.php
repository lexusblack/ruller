<section class="content">
    <div class="container-fluid">
        <div class="row d-flex align-items-stretch">
            @foreach ($staffs as $Staff)
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">

                    <div class="card bg-light">
                        <div class="card-header text-muted border-bottom-0">
                            {{ $Staff->role->alias }}
                        </div>
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="lead"><b>{{ $Staff->name }}</b></h2>
                                    <p class="text-muted text-sm"><b>{{ trans('words.profession') }}: </b> {{ $Staff->profession }} </p>
                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                        <li class="small"><span class="fa-li"><i
                                                    class="fas fa-lg fa-building"></i></span>
                                            {{ $Staff->email }}
                                        </li>
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span>
                                            {{ trans('words.phone') }}: {{ $Staff->phone }}
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-5 text-center">
                                    <img @if ($prev != null)
                                            src="{{ $prev }}"
                                         @else
                                            @if($Staff->photo == null)
                                                src="/dist/img/user2-160x160.jpg"
                                            @else
                                                src="{{Storage::disk('avatars')->url($Staff->photo) }}"
                                            @endif
                                         @endif
                                         alt="user-avatar"
                                         class="img-circle img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">

                            </div>
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
        @if($show_form)
            <div class="row">


                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">

                                <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                                        data-toggle="tab">{{ trans('words.close') }}</a>
                                </li>
                            </ul>

                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane" id="activity">

                                </div>

                                <div class="tab-pane" id="timeline">

                                </div>


                                <div class="tab-pane active" id="settings">
                                    <form wire:submit.prevent="save" class="form-horizontal">
                                        <div class="form-group row">
                                            <label for="inputName" class="col-sm-2 col-form-label">{{ trans('words.staff_name') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="name" type="text" class="form-control" id="inputName"
                                                       placeholder="{{ trans('words.staff_name') }}">
                                                @error('name')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-2 col-form-label">{{ trans('words.email') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="email" type="email" class="form-control"
                                                       id="inputEmail"
                                                       placeholder="{{ trans('words.email') }}">
                                                @error('email')
                                                <span class="bg-red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-sm-2 col-form-label">{{ trans('words.phone') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="phone" type="text" class="form-control" id="phone"
                                                       placeholder="{{ trans('words.phone') }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="role_id" class="col-sm-2 col-form-label">{{ trans('words.role') }}</label>
                                            <div class="col-sm-10">
                                                <select wire:model.lazy="role_id" class="form-control">
                                                    <option value="">{{ trans('words.choise') }}</option>
                                                    @foreach($roles as $item)
                                                        <option value="{{ $item->id }}"
                                                                wire:key="item_{{ $item->id }}">{{ $item->alias }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label for="password" class="col-sm-2 col-form-label">{{ trans('words.password') }}</label>
                                            <div class="col-sm-10">
                                                <input wire:model="password" type="password" class="form-control"
                                                       id="password"
                                                       placeholder="{{ trans('words.password') }}">
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <div class="offset-sm-2 col-sm-10">
                                                <input type="hidden" wire:model="staff_id">
                                                <button type="submit" class="btn btn-danger">{{ trans('words.send') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
        @else

        @endif
        <x-table>
            <x-slot name="head">
                <tr>
                    <th>ID</th>
                    <th>{{ trans('words.staff_name') }}</th>
                    <th>{{ trans('words.phone') }}</th>
                    <th>{{ trans('words.role') }}</th>
                    <th>{{ trans('words.action') }}</th>
                </tr>
            </x-slot>

            <x-slot name="body">
                @foreach ($staffs as $Staff)
                    <tr wire:key="staff-{{ $Staff->id }}">
                        <td>{{ $Staff->id }}</td>
                        <td>{{ $Staff->name }}</td>
                        <td>{{ $Staff->phone }}</td>
                        <td>{{ $Staff->role->alias }}</td>
                        <td>

                            <button wire:click="showInfo({{ $Staff->id }})"
                                    class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt">{{ trans('words.edit') }}</i>
                            </button>

                            <button wire:click="deleteStaff({{ $Staff->id }})"
                                    class="btn btn-danger btn-sm"><i class="fas fa-trash">{{ trans('words.delete') }}</i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </x-slot>
        </x-table>

    </div>
</section>
