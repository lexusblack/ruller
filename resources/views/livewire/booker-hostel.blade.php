<div wire:key="{{ \Illuminate\Support\Carbon::now()->timestamp }}">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('words.hostels') }}</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right"
                                   placeholder="{{ trans('words.search') }}"
                                   wire:model="search"
                                   wire:keydown.escape="resetSearch"
                                   wire:keydown.tab="resetSearch"
                            >


                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <select wire:change="selectedYear($event.target.value)"
                            wire:model="selcted_year">
                        <option value="0">{{ trans('words.select_year') }}</option>
                        @foreach ($years_option as $year)
                            <option value="{{$year['value']}}">{{ $year['text'] }}</option>
                        @endforeach
                    </select>
                    <select wire:change="selectedMonth($event.target.value)"
                            wire:model="selcted_month">
                        <option value="0">{{ trans('words.select_month') }}</option>
                        @foreach ($months_option as $month)
                            <option value="{{$month['value']}}">{{ $month['text'] }}</option>
                        @endforeach
                    </select>
                </div>

                @isset($month['value'])
                    <x-table>
                        <x-slot name="head">
                            <tr>
                                <th>ID</th>
                                <th>{{ trans('words.name_hostel') }}</th>
                                <th>{{ trans('words.price_per_month') }}</th>
                                <th>{{ trans('words.count_workers') }}</th>
                            </tr>
                        </x-slot>

                        <x-slot name="body">

                            @foreach ($hostels as $item)
                                <tr wire:key="hostel-{{ $item['hostel_id'] }}">
                                    <td>{{ $item['hostel_id'] }}</td>
                                    <td>
                                        <a href="#" class="btn btn-block btn-outline-primary btn-xs"
                                           wire:click.prevent="setHostelActiveView({{ $item['hostel_id'] }})">{{ $item['hostel']['name'] }}</a>
                                    </td>
                                    <td>{{ $item['sum'] }}</td>
                                    <td>{{ $item['count_worker'] }}</td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table>
                @endisset
            </div>
        </div>
    </div>
    {{--{{ print_r($workers) }}--}}
    @isset($hostel_id)

        <div class="card card-lg active">
            <div class="col-md-12">
                <div class="card-header">
                    <p class="text-sm-center"><b>{{ trans('words.lived') }}</b></p>
                </div>
            </div>
            <div class="card-body">
                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.sum') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach($workers as $item_worker)
                            <tr wire:key="worker-{{ $item_worker->worker_id }}">
                                <td>{{ $item_worker->worker_id }}</td>
                                <td>{{ $item_worker->worker->first_name}} {{ $item_worker->worker->second_name }}</td>
                                <td>{{ $item_worker->sum }}</td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
            </div>


        </div>
    @endisset
</div>
<style>

    /*.col-md-12 > .card-header {*/
    /*    background: #bff3d5;*/
    /*}*/

    .card > .col-md-12 {
        background: #bff3d5;
    }

    .bg-cyan {
        background: #6bee9f !important;
        color: black !important;
    }

    .bg-red {
        background-color: #fd8888a6 !important;
        color: black !important;
    }

</style>
