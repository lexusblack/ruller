<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.page_add') }}</h3>
            </div>
            <div class="card-body">
                <div class="mb-2 row">
                    <div class="col-3">
                        <input wire:model.lazy="page_title" type="text" class="form-control" placeholder="{{ trans('words.title') }}">
                        @error('page_title')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <input wire:model.lazy="menu_title" type="text" class="form-control" placeholder="{{ trans('words.menu') }}">
                        @error('menu_title')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <select wire:model.lazy="parent" class="form-control">
                            <option value="">{{ trans('words.null') }}</option>
                            @foreach($pages as $page)
                                <option value="{{ $page->id }}"
                                        wire:key="parent_{{ $page->id }}">{{ $page->page_title }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="col-3">
                        <input wire:model.lazy="icon" type="text" class="form-control" placeholder="{{ trans('words.icon') }}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <input wire:model="alias" type="text" class="form-control" placeholder="{{ trans('words.alias') }}">
                        @error('alias')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <input wire:model.lazy="uri" type="text" class="form-control" placeholder="{{ trans('words.uri') }}">
                    </div>

                    <div class="col-3">
                        <input wire:model.lazy="template" type="text" class="form-control" placeholder="{{ trans('words.template') }}">
                    </div>
                    <div class="col-1">
                        <input wire:model.lazy="sort" type="text" class="form-control" placeholder="{{ trans('words.sort') }}">
                    </div>
                    <div class="col-2">
                        <div class="custom-control custom-switch">
                            <input wire:model="published" type="checkbox" value="{{ $published }}"
                                   class="custom-control-input" id="customSwitch1">
                            <label class="custom-control-label" for="customSwitch1">{{ trans('words.publick') }}?</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" neme="id" wire:model="page_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.send') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.pages') }} {{ trans('words.title') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $page_title }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.menu') }} {{ trans('words.title') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $menu_title }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.parent') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $parent }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.alias') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $alias }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.uri') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $uri }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.template') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $template }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.icon') }}:</dt>
                    <dd class="col-sm-6">
                        <i class="nav-icon {{ $icon }}"></i>
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.sort') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $sort }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.publish') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $published }}
                    </dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_page') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.title') }}</th>
                            <th>{{ trans('words.sort') }}</th>
                            <th>{{ trans('words.uri') }}</th>
                            <th>{{ trans('words.status') }}</th>
                            <th>{{ trans('words.last_update') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($pages as $page)
                            <tr wire:key="row-{{ $page->id }}">
                                <td>{{ $page->id }}</td>
                                <td>{{ $page->menu_title }}</td>
                                <td>{{ $page->sort }}</td>
                                <td>{{ $page->uri }}</td>
                                <td>{{ $page->published }}</td>
                                <td>{{ $page->published_at }}</td>
                                <td>
                                    @if ($page->published == 1)
                                        <button wire:click="unpublishedPage({{ $page->id }})"
                                                class="btn btn-info btn-sm"><i class="fas fa-folder">{{ trans('words.unpublish') }}</i>
                                        </button>
                                    @else
                                        <button wire:click="publishePage({{ $page->id }})" class="btn btn-info btn-sm">
                                            <i class="fas fa-folder-minus">{{ trans('words.publish') }}</i></button>
                                    @endif

                                    <button wire:click="editPage({{ $page->id }})" class="btn btn-primary btn-sm"><i
                                            class="fas fa-pencil-alt">{{ trans('words.edit') }}</i></button>
                                        <button wire:click="showInfo({{ $page->id }})"
                                                class="btn btn-info btn-sm"><i class="fas fa-folder"> {{ trans('words.info') }}</i>
                                        </button>
                                    <button wire:click="delete({{ $page->id }})" class="btn btn-danger btn-sm"><i
                                            class="fas fa-trash">{{ trans('words.delete') }}</i></button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $pages->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>

<script>
    document.addEventListener('livewire:load', function () {

        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>
