<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 wire:click="sayGoodbye" class="card-title">{{ trans('words.settlement_add') }}</h3>
            </div>
            <div class="card-body">
                <div class="mb-2 row">
                    <div class="col-3">
                        <select wire:model.lazy="worker_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($workers as $worker)
                                <option value="{{ $worker->id }}"
                                        wire:key="worker_{{ $worker->id }}">{{ $worker->first_name }} {{ $worker->second_name }}</option>
                            @endforeach
                        </select>
                        @error('worker_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <select wire:model.lazy="hostel_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($hostels as $hostel)
                                <option value="{{ $hostel->id }}"
                                        wire:key="worker_{{ $hostel->id }}">{{ $hostel->name }}</option>
                            @endforeach
                        </select>
                        @error('hostel_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2">
                        <input wire:model.lazy="price" type="number" class="form-control" min="1" step="0.1" id="price"
                               placeholder="{{ trans('words.price') }}">
                        @error('price')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2">
                        <input
                            wire:model="start_hostel"
                            type="text"
                            data-min-date="{{ $start_min_date }}"
                            class="form-control setlemmetn_date_start"
                            id="start_hostel"
                            placeholder="{{ trans('words.start') }}"
                            autocomplete="off"
                            readonly
                        >
                        @error('start_hostel')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-sm-2">
                        <input
                            wire:model="stop_hostel"
                            type="text"
                            data-min-date="{{ $stop_min_date }}"
                            class="form-control setlemmetn_date_stop"
                            id="stop_hostel"
                            placeholder="{{ trans('words.stop') }}"
                            autocomplete="off"
                            readonly
                        >
                        @error('stop_hostel')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="settlement_id" wire:model="settlement_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.worker') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $worker['first_name'] }} {{ $worker['second_name'] }} {{ $worker['passport'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.hostel') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $hostel['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.address') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $hostel['address'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.price') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $price }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.start_work') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $start_hostel }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.stop_work') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $stop_hostel }}
                    </dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_settlement') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.hostel') }}</th>
                            <th>{{ trans('words.price') }}</th>
                            <th>{{ trans('words.start') }}</th>
                            <th>{{ trans('words.stop') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($settlements as $settlement)
                            <tr wire:key="row-{{ $settlement->id }}">
                                <td>{{ $settlement->id }}</td>
                                <td>{{ $settlement->worker->first_name }} {{ $settlement->worker->second_name }}</td>
                                <td>{{ $settlement->hostel->name }}</td>
                                <td>{{ $settlement->price }}</td>
                                <td>{{ $settlement->start_hostel }}</td>
                                <td>{{ $settlement->stop_hostel }}</td>
                                <td>
                                    <button wire:click="editSettlement({{ $settlement->id }})"
                                            class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"> {{ trans('words.edit') }} </i>
                                    </button>
                                    <button wire:click="showInfo({{ $settlement->id }})"
                                            class="btn btn-info btn-sm"><i class="fas fa-folder"> {{ trans('words.info') }}</i>
                                    </button>
                                    <button wire:click="deleteSettlement({{ $settlement->id }})"
                                            class="btn btn-danger btn-sm"><i class="fas fa-trash">{{ trans('words.delete') }}</i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $settlements->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>


<script>
    document.addEventListener('livewire:load', function () {
        window.addEventListener('say-goodbye', event => {
            SettlementJS.refreshDatePickStop(event.detail.min, event.detail.max);
        });
        window.addEventListener('init-data-fields', () => {
            SettlementJS.init();
        });
        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>
