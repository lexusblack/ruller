<div  wire:key="{{ \Illuminate\Support\Carbon::now()->timestamp }}">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ trans('words.salaries') }}</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right"
                                   placeholder="{{ trans('words.search') }}"
                                   wire:model="search"
                                   wire:keydown.escape="resetSearch"
                                   wire:keydown.tab="resetSearch"
                            >
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <select wire:change="selectedYearSalary($event.target.value)"
                            wire:model="selcted_year_salary">
                        <option value="">{{ trans('words.select_year') }}</option>
                        @foreach ($years_option_salary as $year_salary)
                            <option value="{{$year_salary['value']}}">{{ $year_salary['text'] }}</option>
                        @endforeach
                    </select>
                    <select wire:change="selectedMonthSalary($event.target.value)"
                            wire:model="selcted_month_salary">
                        <option value="">{{ trans('words.select_month') }}</option>
                        @foreach ($months_option_salary as $month_salary)
                            <option
                                value="{{$month_salary['value']}}">{{ $month_salary['text'] }}</option>
                        @endforeach
                    </select>
                </div>

                @isset($month_salary['value'])
                    <x-table>
                        <x-slot name="head">
                            <tr>
                                <th>ID</th>
                                <th>{{ trans('words.worker') }}</th>
                                <th>{{ trans('words.sum') }}</th>
                                <th>{{ trans('words.bonus') }}</th>
                                <th>{{ trans('words.service') }}</th>
                                <th>{{ trans('words.hostel') }}</th>
                                <th>{{ trans('words.penalty') }}</th>
                                <th>{{ trans('words.status') }}</th>
                            </tr>
                        </x-slot>

                        <x-slot name="body">
                            @foreach ($salary_workers as $item)
                                <tr wire:key="salaryWorkers-{{ $item['worker_id'] }}">
                                    <td>{{ $item['worker_id'] }}</td>
                                    <td>
                                        <a href="#"
                                           wire:click.prevent="setWorkerActiveView({{ $item['worker_id'] }})"
                                           class=" btn btn-block btn-outline-primary btn-xs">
                                            {{ $item['first_name'] }} {{ $item['second_name'] }}
                                        </a>

                                    </td>
                                    <td>{{ $item['sum'] + ($item['sum_bonus'] ?? 0) - ($item['sum_settlement'] ?? 0) - ($item['sum_penalty'] ?? 0) - ($item['sum_service'] ?? 0)}}</td>
                                    <td>{{ $item['sum_bonus'] ?? 0}}</td>
                                    <td>{{ $item['sum_service'] ?? 0}}</td>
                                    <td>{{ $item['sum_settlement'] ?? 0}}</td>
                                    <td>{{ $item['sum_penalty'] ?? 0}}</td>
                                    <td>{{ \App\Models\Salary::SALARY_STATUS[$item['status']] }}</td>
                                </tr>
                            @endforeach
                        </x-slot>
                    </x-table>
                    {{ $salary_workers->links('livewire.pagination') }}
                @endisset
            </div>
        </div>
    </div>
{{--        {{ print_r($workers_salary->job_id) }}--}}
    @isset($first_name)

        <div class="card card-lg active">
            <div class="col-md-12">
                <div class="card-header">
                    <p class="text-sm-center"> <b>{{ $first_name }} {{$second_name}} </b></p>
                </div>
            </div>
            <div class="card-body">
                <dl class="row">
                    @foreach($workers_salary as $salary)
                        <dt class="col-sm-3 text-sm-right">{{ trans('words.job') }}:</dt>
                        <dt class="col-sm-3"> {{ $salary->description }}</dt>
                        <dt class="col-sm-3 text-sm-right"> {{ trans('words.salaries') }}:</dt>
                        <dd class="col-sm-3 bg-cyan">{{ $salary->salary }}</dd>
                    @endforeach
                </dl>
                @isset($workers_sponsor)
                    <dl class="row">
                        @foreach($workers_sponsor as $bonus)
                            <dt class="col-sm-3 text-sm-right">{{ trans('words.bonus') }}:</dt>
                            <dt class="col-sm-3"> {{ $bonus->comment }}</dt>
                            <dt class="col-sm-3 text-sm-right"> {{ trans('words.price') }}:</dt>
                            <dd class="col-sm-3 bg-cyan">{{ $bonus->price }}</dd>
                        @endforeach
                    </dl>
                @endisset
                @isset($workers_penalty)
                    <dl class="row">
                        @foreach($workers_penalty as $penalty)
                            <dt class="col-sm-3 text-sm-right">{{ trans('words.penalty') }}:</dt>
                            <dt class="col-sm-3"> {{ $penalty->comment }}</dt>
                            <dt class="col-sm-3 text-sm-right"> {{ trans('words.price') }}:</dt>
                            <dd class="col-sm-3 bg-red">{{ $penalty->price }}</dd>
                        @endforeach
                    </dl>
                @endisset
                @isset($workers_service)
                    <dl class="row">
                        @foreach($workers_service as $service)
                            <dt class="col-sm-3 text-sm-right">{{ trans('words.service') }}:</dt>
                            <dt class="col-sm-3"> {{ $service->service->name }}</dt>
                            <dt class="col-sm-3 text-sm-right"> {{ trans('words.price') }}:</dt>
                            <dd class="col-sm-3 bg-red">{{ $service->price }}</dd>
                        @endforeach
                    </dl>
                @endisset
                @isset($workers_settlement)
                    <dl class="row">
                        @foreach($workers_settlement as $settlement)
                            <dt class="col-sm-3 text-sm-right">{{ trans('words.hostel') }}:</dt>
                            <dt class="col-sm-3"> {{ $settlement->hostel->name }}</dt>
                            <dt class="col-sm-3 text-sm-right"> {{ trans('words.price') }}:</dt>
                            <dd class="col-sm-3 bg-red">{{ $settlement->price }}</dd>
                        @endforeach
                    </dl>
                @endisset
            </div>

        </div>

    @endisset
</div>
<style>

    /*.col-md-12 > .card-header {*/
    /*    background: #bff3d5;*/
    /*}*/

    .card > .col-md-12 {
        background: #bff3d5;
    }

    .bg-cyan {
        background: #6bee9f !important;
        color: black !important;
    }

    .bg-red {
        background-color: #fd8888a6!important;
        color: black !important;
    }

</style>
