<livewire:head/>


<livewire:menu/>


<div class="sidebar-custom">

    @auth
        <livewire:logout/>
    @endauth
</div>

</aside>
<div class="content-wrapper">

    @include('livewire.bread', [
    'title' => $title
    ])


    <section class="content">

        <livewire:dashboard-form/>
    </section>

</div>


<livewire:footer/>
