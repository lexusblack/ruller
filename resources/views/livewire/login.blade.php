<div class="login-box">

    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <p class="h1">{{ trans('words.logo_title') }}</p>
        </div>
        <div class="card-body">

            <form wire:submit.prevent="login" action="#" method="post">

                <div class="input-group mb-3">
                    <input wire:model.lazy="email" type="email" class="form-control" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                    <span class="bg-red">{{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input wire:model.lazy="password" type="password" class="form-control" placeholder="{{ trans('words.password') }}">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                    <span class="bg-red">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input wire:model="remember" value="" type="checkbox" class="custom-control-input" id="customSwitch3">
                        <label class="custom-control-label" for="customSwitch3">{{ trans('words.remember_me') }}</label>
                    </div>
                </div>
                <div class="row">


                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">{{ trans('words.sign_up') }}</button>
                    </div>

                </div>
            </form>

            {{--  <p class="mb-1">
                  <a href="forgot-password.html">I forgot my password</a>
              </p>--}}
            <p class="mb-0">
                <a href="/register" class="text-center">{{ trans('words.register') }}</a>
            </p>
        </div>

    </div>

</div>

