<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.status_add') }}</h3>
            </div>

            <div class="card-body">
                <div class="form-group row">
                    <label for="worker_id" class="col-sm-2 col-form-label">{{ trans('words.worker') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="worker_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($workers as $worker)
                                <option value="{{ $worker->id }}"
                                        wire:key="worker_{{ $worker->id }}">{{ $worker->first_name }} {{ $worker->second_name }}</option>
                            @endforeach
                        </select>
                        @error('worker_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="service_id" class="col-sm-2 col-form-label">{{ trans('words.services') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="service_id" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($services as $service)
                                <option value="{{ $service->id }}"
                                        wire:key="worker_{{ $service->id }}">{{ $service->name }}</option>
                            @endforeach
                        </select>
                        @error('service_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">{{ trans('words.service_status') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="status" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($status_list as $value => $text)
                                <option value="{{ $value }}" wire:key="profession_{{ $value }}">{{ $text }}</option>
                            @endforeach
                        </select>
                        @error('status')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date_start" class="col-sm-2 col-form-label">{{ trans('words.service_start_day') }}</label>
                    <div class="col-sm-10">
                        <input wire:model="date_start" type="text" class="form-control datepicker" autocomplete="off" readonly id="date_start"
                               placeholder="{{ trans('words.service_start_day') }}">
                        @error('date_start')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.service_stop_day') }}</label>
                    <div class="col-sm-10">
                        <input wire:model="date_end" type="text" class="form-control datepicker" autocomplete="off" readonly id="date_end"
                               placeholder="{{ trans('words.service_stop_day') }}">
                    </div>
                </div>

                @if($price != $servicePrice)
                    <div class="col-2">
                        <div class="custom-control custom-switch">
                            <input wire:model="up" type="checkbox" value="{{ $up }}"
                                   class="custom-control-input" id="customSwitch1">
                            <label class="custom-control-label" for="customSwitch1">{{ trans('words.update_price_service') }}</label>
                        </div>
                    </div>
                @endif

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" name="status_id" wire:model="status_id">
                        <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>
                    </div>
                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.worker') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $worker['first_name'] }} {{ $worker['second_name'] }} {{ $worker['passport'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.name') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $servic['name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.status') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $status }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.service_start_day') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $date_start }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.service_stop_day') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $date_end }}
                    </dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_services') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.worker') }}</th>
                            <th>{{ trans('words.service') }}</th>
                            <th>{{ trans('words.status') }}</th>
                            <th>{{ trans('words.start') }}</th>
                            <th>{{ trans('words.stop') }}</th>
                            <th>{{ trans('words.price') }}</th>
                            <th>{{ trans('words.action') }}</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($statuss as $status)
                            <tr wire:key="row-{{ $status->id }}">
                                <td>{{ $status->id }}</td>
                                <td>{{ $status->worker->first_name }} {{ $status->worker->second_name }}</td>
                                <td>{{ $status->service->name }}</td>
                                <td>{{ $status->status }}</td>
                                <td>{{ $status->date_start }}</td>
                                <td>{{ $status->date_end }}</td>
                                <td>{{ $status->price }}</td>
                                <td>
                                    <button wire:click="editStatus({{ $status->id }})"
                                            class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"> {{ trans('words.edit') }} </i>
                                    </button>
                                    <button wire:click="showInfo({{ $status->id }})"
                                            class="btn btn-info btn-sm"><i class="fas fa-folder"> {{ trans('words.info') }}</i>
                                    </button>
                                    <button wire:click="deleteStatus({{ $status->id }})"
                                            class="btn btn-danger btn-sm"><i class="fas fa-trash">{{ trans('words.delete') }}</i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $statuss->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>

<script>
    document.addEventListener('livewire:load', function () {
        window.addEventListener('init-date-field', () => {
            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'clip',
                onSelect: function(dateText, inst) {
                    if ($(this).prop('readonly')) {
                        $(this).prop('readonly', false);
                    }
                    $(this).val(dateText);
                    this.dispatchEvent(new InputEvent('input'));
                },
                onClose: function(dateText, inst) {
                    if (!$(this).prop('readonly')) {
                        $(this).prop('readonly', true);
                    }
                }
            });
        });
        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>

