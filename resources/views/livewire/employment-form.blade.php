<div>
    @if($show_form)
        <form wire:submit.prevent="save" class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ trans('words.employment_add') }}</h3>
            </div>
            <div class="card-body">

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.worker') }}</label>
                    <div class="col-sm-10">
                        <select wire:change="getJobs" wire:model.lazy="worker_id" class="select2bs4 form-control"
                                id="workers">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($workers as $worker)
                                <option value="{{ $worker->id }}"
                                        wire:key="worker_{{ $worker->id }}">{{ $worker->first_name }} {{ $worker->second_name }}
                                    ({{ $worker->passport  }})
                                </option>
                            @endforeach
                        </select>
                        @error('worker_id')
                        <span class="bg-red">{{ $message }}</span>
                        @enderror

                    </div>
                </div>
                @if(empty($employment_id))
                    <div class="form-group row">
                        <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.job') }}</label>
                        <div class="col-sm-10">
                            <select wire:model.lazy="job_id" class="form-control">
                                <option value="">{{ trans('words.choise') }}</option>
                                @foreach($jobs as $job)
                                    <option value="{{ $job['id'] }}"
                                            {{ $job['id'] == 0 ? 'disabled' : '' }} wire:key="job_{{ $job['id'] }}">{{ $job['description'] }}</option>
                                @endforeach
                            </select>
                            @error('job_id')
                            <span class="bg-red">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.date_start_working') }}</label>
                        <div class="col-sm-10">
                            <input wire:model="start" type="text" class="form-control datepicker" autocomplete="off"
                                   readonly id="start"
                                   placeholder="{{ trans('words.date_start_working') }}">
                            @error('start')
                            <span class="bg-red">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                @endif
                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.date_stop_working') }}</label>
                    <div class="col-sm-10">
                        <input wire:model="stop" type="text" class="form-control datepicker" autocomplete="off" readonly
                               id="stop"
                               placeholder="{{ trans('words.date_stop_working') }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="first_name" class="col-sm-2 col-form-label">{{ trans('words.status') }}</label>
                    <div class="col-sm-10">
                        <select wire:model.lazy="status" class="form-control">
                            <option value="">{{ trans('words.choise') }}</option>
                            @foreach($statuses as $key => $value)
                                <option value="{{ $key }}" wire:key="status_{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="comment" class="col-sm-2 col-form-label">{{ trans('words.description') }}</label>
                    <div class="col-sm-10">
                        <input wire:model.lazy='comment' type="text" class="form-control" placeholder="{{ trans('words.description') }}">
                    </div>
                </div>
                @if($status == \App\Models\Employment::STATUS_FINISH)
                    <div class="form-group row">
                        <label for="dismissal" class="col-sm-2 col-form-label">{{ trans('words.dismissal') }}</label>
                        <div class="col-sm-10">
                            <input wire:model.lazy='dismissal' type="text" class="form-control" placeholder="{{ trans('words.dismissal') }}">
                        </div>
                    </div>
                @endif
            </div>

            <div class="card-footer">
                <div class="row">

                    <input type="hidden" name="employment_id" wire:model="employment_id">
                    <button type="submit" class="btn btn-primary">{{ trans('words.send') }}</button>

                </div>
            </div>
        </form>
    @endif
    @if($show_info)
        <div class="card card-lg active">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                    <li class="nav-item"><a class="nav-link active" href="#" wire:click="hideInfo()"
                                            data-toggle="tab">{{ trans('words.close') }}</a>
                    </li>
                </ul>

            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-6 text-sm-right">{{ trans('words.first_name') }} {{ trans('words.second_name') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $worker['first_name'] }} {{ $worker['second_name'] }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.job_description') }}:</dt>
                    <dd class="col-sm-6">{{ $job['description'] }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.date_start_working') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $start }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.date_stop_working') }}:</dt>
                    <dd class="col-sm-6">
                        {{ $stop }}
                    </dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.status_worker') }}:</dt>
                    <dd class="col-sm-6">{{ $status }}</dd>

                    <dt class="col-sm-6 text-sm-right">{{ trans('words.description') }}:</dt>
                    <dd class="col-sm-6">{{ $comment }}</dd>

                </dl>

            </div>

        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="card-title btn btn-flat margin">{{ trans('words.list') }}</div>
                            @if(!$show_form)
                                <button wire:click="showForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.add_new_employment') }}
                                </button>
                            @else
                                <button wire:click="hideForm()"
                                        class="btn bg-olive btn-flat margin">{{ trans('words.close') }}
                                </button>
                            @endif
                        </div>

                        <div class="col-sm-2">
                            <div class="card-tools">

                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text"
                                           name="table_search"
                                           class="form-control float-right"
                                           placeholder="{{ trans('words.search') }}"
                                           wire:model="search"
                                           wire:keydown.escape="resetSearch"
                                           wire:keydown.tab="resetSearch"
                                    >
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <x-table>
                    <x-slot name="head">
                        <tr>
                            <th>ID</th>
                            <th>{{ trans('words.date_start_working') }}</th>
                            <th>{{ trans('words.date_stop_working') }}</th>
                            <th>{{ trans('words.status') }}</th>
                            <th>{{ trans('words.name') }}</th>
                            <th>{{ trans('words.money_per_hour') }}</th>
                            <th>{{ trans('words.money_plus') }}</th>
                            <th>Actions</th>
                        </tr>
                    </x-slot>

                    <x-slot name="body">
                        @foreach ($employments as $employment)
                            <tr wire:key="row-{{ $employment->e_id }}">
                                <td>{{ $employment->e_id }}</td>
                                <td>{{ $employment->start }}</td>
                                <td>{{ $employment->stop }}</td>
                                <td><span class=" @if ( $employment->status == \App\Models\Employment::STATUS_FINISH )
                                            badge bg-danger
                                        @else
                                            badge bg-success
                                        @endif
                                        ">
                                    {{ $employment->status }}
                                    </span>
                                </td>
                                <td>{{ $employment->worker->first_name }} {{ $employment->worker->second_name }}</td>
                                <td>{{ $employment->job->money }}</td>
                                <td>{{ $employment->job->money_plus }}</td>
                                <td>
                                    <button wire:click="editEmployment({{ $employment->e_id }})"
                                            class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"> {{ trans('words.edit') }} </i>
                                    </button>
                                    <button wire:click="showInfo({{ $employment->e_id }})"
                                            class="btn btn-info btn-sm"><i class="fas fa-folder"> {{ trans('words.info') }}</i>
                                    </button>
                                    <button wire:click="deleteEmployment({{ $employment->e_id }})"
                                            class="btn btn-danger btn-sm"><i class="fas fa-trash">{{ trans('words.delete') }}</i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </x-slot>
                </x-table>
                {{ $employments->links('livewire.pagination') }}
            </div>

        </div>
    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", () => {


        Livewire.hook('message.processed', (message, component) => {
            $('.select2bs4').select2({})
                .on('select2:select', function (e) {
                    e.params.data.element.parentElement.dispatchEvent(new InputEvent('change'));
                });

        })

    });


    document.addEventListener('livewire:load', function () {
        window.addEventListener('init-date-field', () => {

            $('.datepicker').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showAnim: 'clip',
                onSelect: function (dateText, inst) {
                    if ($(this).prop('readonly')) {
                        $(this).prop('readonly', false);
                    }
                    $(this).val(dateText);
                    this.dispatchEvent(new InputEvent('input'));
                    // $(this).prop('readonly', true);
                    //console.log('OnSelect', this, dateText, inst);
                },
                onClose: function (dateText, inst) {
                    if (!$(this).prop('readonly')) {
                        $(this).prop('readonly', true);
                    }

                    //console.log('OnClose', dateText, inst);
                }
            });


        });
        window.addEventListener('messageSuccess', (message) => {
            FlashMessage.success(message);
        });
        window.addEventListener('messageError', (message) => {
            FlashMessage.error(message);
        });
        window.addEventListener('messageInfo', (message) => {
            FlashMessage.info(message);
        });
        window.addEventListener('messageWarning', (message) => {
            FlashMessage.warning(message);
        });
        window.addEventListener('messageQuestion', (message) => {
            FlashMessage.question(message);
        });
        window.addEventListener('toTopPage', () => {
            Site.toTop();
        });
    });
</script>
