<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':atrybut musi zostać zaakceptowany.',
    'active_url' => ':atrybut nie jest prawidłowym adresem URL.',
    'after' => ':atrybut musi być datą po :data.',
    'after_or_equal' => ':atrybut musi być datą późniejszą lub równą :data.',
    'alpha' => ':atrybut może zawierać tylko litery.',
    'alpha_dash' => ':attribute może zawierać tylko litery, cyfry, myślniki i podkreślenia.',
    'alpha_num' => ':attribute może zawierać tylko litery i cyfry.',
    'array' => ':attribute musi być tablicą.',
    'before' => ':attribute musi być datą wcześniejszą :date.',
    'before_or_equal' => ':attribute musi być datą wcześniejszą lub równą :date.',
    'between' => [
        'numeric' => ':attribute musi być pomiędzy :min i :max.',
        'file' => ':attribute musi być pomiędzy :min i :max kilobajty.',
        'string' => ':attribute musi być pomiędzy :min i :max postacie.',
        'array' => ':attribute musi mieć pomiędzy :min i :max przedmiotów.',
    ],
    'boolean' => ':attribute pole musi mieć wartość true lub false.',
    'confirmed' => ':attribute potwierdzenie nie pasuje.',
    'date' => ':attribute nie jest prawidłową datą.',
    'date_equals' => ':attribute musi być datą równą :date.',
    'date_format' => ':attribute nie pasuje do formatu :format.',
    'different' => ':attribute i :other musi być inny.',
    'digits' => ':attribute musi być :digits cyfry.',
    'digits_between' => ':attribute musi być pomiędzy :min i :max cyfry.',
    'dimensions' => ':attribute ma nieprawidłowe wymiary obrazu.',
    'distinct' => ':attribute pole ma zduplikowaną wartość.',
    'email' => ':attribute musi być prawidłowym adresem e-mail.',
    'ends_with' => ':attribute musi kończyć się jednym z poniższych: :values.',
    'exists' => 'Wybrany :attribute jest nieważny.',
    'file' => ':attribute musi być plikiem.',
    'filled' => ':attribute pole musi mieć wartość.',
    'gt' => [
        'numeric' => ':attribute musi być większe niż :value.',
        'file' => ':attribute musi być większe niż :value kilobajtom.',
        'string' => ':attribute musi być większe niż :value postacie.',
        'array' => ':attribute musi mieć więcej niż :value przedmioty.',
    ],
    'gte' => [
        'numeric' => ':attribute musi być większe lub równe :value.',
        'file' => ':attribute musi być większa lub równa :value kilobajtom.',
        'string' => ':attribute musi być większe lub równe :value postacie.',
        'array' => ':attribute muszę mieć :value przedmioty lub więcej.',
    ],
    'image' => ':attribute musi być obrazem.',
    'in' => 'Wybrany :attribute jest nieważny.',
    'in_array' => ':attribute pole nie istnieje w :other.',
    'integer' => ':attribute musi być liczbą całkowitą.',
    'ip' => ':attribute musi być prawidłowym adresem IP.',
    'ipv4' => ':attribute musi być prawidłowym adresem IPv4.',
    'ipv6' => ':attribute musi być prawidłowym adresem IPv6.',
    'json' => ':attribute musi być prawidłowym ciągiem JSON.',
    'lt' => [
        'numeric' => ':attribute musi być mniejsze niż :value.',
        'file' => ':attribute musi być mniejsze niż :value kilobajtom.',
        'string' => ':attribute musi być mniejsze niż :value postacie.',
        'array' => ':attribute musi mieć mniej niż :value przedmioty.',
    ],
    'lte' => [
        'numeric' => ':attribute musi być mniejsze lub równe :value.',
        'file' => ':attribute musi być mniejsze lub równe :value kilobajtom.',
        'string' => ':attribute musi być mniejsze lub równe :value postacie.',
        'array' => ':attribute nie może mieć więcej niż :value przedmioty.',
    ],
    'max' => [
        'numeric' => ':attribute nie może być większa niż :max.',
        'file' => ':attribute nie może być większa niż :max kilobajtom.',
        'string' => ':attribute nie może być większa niż :max postacie.',
        'array' => ':attribute nie może mieć więcej niż :max przedmioty.',
    ],
    'mimes' => ':attribute musi być plikiem typu: :values.',
    'mimetypes' => ':attribute mmusi być plikiem typu: :values.',
    'min' => [
        'numeric' => ':attribute musi być przynajmniej :min.',
        'file' => ':attribute musi być przynajmniej :min kilobajtom.',
        'string' => ':attribute musi być przynajmniej :min postacie.',
        'array' => ':attribute musi mieć co najmniej :min przedmioty.',
    ],
    'multiple_of' => ':attribute musi być wielokrotnością :value',
    'not_in' => 'Wybrany :attribute jest nieważny.',
    'not_regex' => ':attribute format jest nieprawidłowy.',
    'numeric' => ':attribute musi być liczbą.',
    'password' => 'Hasło jest błędne.',
    'present' => ':attribute pole musi być obecne.',
    'regex' => ':attribute format jest nieprawidłowy.',
    'required' => ':attribute to pole jest wymagane.',
    'required_if' => ':attribute pole jest wymagane, gdy :other jest :value.',
    'required_unless' => ':attribute pole jest wymagane, chyba że :other jest w :values.',
    'required_with' => ':attribute pole jest wymagane, gdy :values jest obecny.',
    'required_with_all' => ':attribute pole jest wymagane, gdy :values jest obecny.',
    'required_without' => ':attribute pole jest wymagane, gdy :values nie jest obecny.',
    'required_without_all' => ':attribute pole jest wymagane, gdy żadne z :values są obecni.',
    'same' => ':attribute i :other musi pasować.',
    'size' => [
        'numeric' => ':attribute musi być :size.',
        'file' => ':attribute musi być :size kilobajtom.',
        'string' => ':attribute musi być :size postacie.',
        'array' => ':attribute musi zawierać :size przedmioty.',
    ],
    'starts_with' => ':attribute musi zaczynać się od jednego z poniższych: :values.',
    'string' => ':attribute musi być ciągiem.',
    'timezone' => ':attribute musi być prawidłową strefą.',
    'unique' => ':attribute został już zajęty.',
    'uploaded' => ':attribute nie udało się przesłać.',
    'url' => ':attribute format jest nieprawidłowy.',
    'uuid' => ':attribute musi być prawidłowym identyfikatorem UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
