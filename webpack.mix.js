const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //require('postcss-css-variables')()
    ]);*/

/*
mix.combine([
    'resources/plugins/jquery/jquery.min.js',
    'resources/plugins/jquery-ui/jquery-ui.min.js',
], 'resources/js/base.js').js('resources/js/base.js', 'public/js').version(); 
mix.sass('resources/css/app.scss', 'public/css').version();
mix.copy('resources/plugins/fontawesome-free/webfonts/!*', 'public/webfonts');*/

// mix.sass('resources/css/app.scss', 'public/css').version();

/*
mix.combine([
    'resources/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'resources/plugins/chart.js/Chart.min.js',
    'resources/plugins/sparklines/sparkline.mjs',
    'resources/plugins/jqvmap/jquery.vmap.min.js',
    'resources/plugins/jqvmap/maps/jquery.vmap.usa.js',
    'resources/plugins/jquery-knob/jquery.knob.min.js',
    'resources/plugins/moment/moment.min.js',
    'resources/plugins/daterangepicker/daterangepicker.js',
    'resources/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
    'resources/plugins/summernote/summernote-bs4.min.js',
    'resources/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
], 'resources/js/first.js').js('resources/js/first.js', 'public/js').version();

mix.combine([
    'resources/js/adminlte.min.js',
    'resources/js/demo.js',
    'resources/js/pages/dashboard.js'
], 'resources/js/second.js').js('resources/js/second.js', 'public/js').version();
*/
/*

mix.scripts([
    'resources/plugins/jquery/jquery.min.js',
    'resources/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'resources/js/adminlte.min.js',
], 'resources/js/app.js').js('resources/js/app.js', 'public/js').version();
mix.sass('resources/css/app.scss', 'public/css').version();
mix.copy('resources/plugins/fontawesome-free/webfonts/!*', 'public/webfonts');*/
